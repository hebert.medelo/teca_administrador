/*
 * FachadaAppTeca.java
 *
 * Proyecto: Cotización de seguros
 * Cliente: Teca
 * Copyright 2017 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.AsignaAtributoStatement;
import co.mobiltech.teca.common.util.Formato;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.CotizacionUsuarioDTO;
import co.mobiltech.teca.mvc.dto.DetalleCotizacionDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Ing. Hebert Medelo
 */
public class DetalleCotizacionDAO {

    /**
     *
     * @param conexion
     * @param id
     * @return
     */
    public DetalleCotizacionDTO detalleCotizacion(Connection conexion, String id) {
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        DetalleCotizacionDTO datos = null;
        StringBuilder cadSQL = null;
        
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.coti_id, cl.tido_id, td.tido_descripcion, cl.clie_nombre, cl.clie_apellido, cl.clie_fechanacimiento, cl.clie_telefono, ");
            cadSQL.append(" cl.clie_email, v.vehi_id, m.muni_id, m.muni_nombre, d.depa_id, d.depa_nombre, u.ubve_id, u.ubve_descripcion, tv.tisv_id, ");
            cadSQL.append(" tv.tisv_descripcion, cf.cofa_linea, cf.cofa_marca, v.vehi_modelo, v.vehi_placa, dc.deco_id, ");
            cadSQL.append(" a.aseg_id, a.aseg_nombre, ts.tise_id, ts.tise_nombre, dc.deco_tiempoduracion, dc.deco_valortotalseguro, dc.deco_valorasegurado, ");
            cadSQL.append(" dc.deco_valorreferencia, dc.deco_antecedente, dc.deco_cobertura, dc.deco_informacionimportante, cl.clie_cedula, c.coti_observacionasesor ");
            cadSQL.append(" FROM cotizacion AS c ");
            cadSQL.append(" INNER JOIN cliente AS cl ON cl.clie_id = c.clie_id ");
            cadSQL.append(" INNER JOIN tipo_documento AS td ON td.tido_id = cl.tido_id ");
            cadSQL.append(" INNER JOIN vehiculo AS v ON v.vehi_id = c.vehi_id ");
            cadSQL.append(" INNER JOIN municipio AS m ON m.muni_id = v.muni_id ");
            cadSQL.append(" INNER JOIN departamento AS d ON m.depa_id = d.depa_id ");
            cadSQL.append(" INNER JOIN ubicacion_vehiculo AS u ON u.ubve_id = v.ubve_id ");
            cadSQL.append(" INNER JOIN tipo_serviciovehiculo AS tv ON tv.tisv_id = v.tisv_id ");
            cadSQL.append(" INNER JOIN configuracion_fasecolda AS cf ON cf.cofa_codigofasecolda = v.vehi_codigofasecolda ");
            cadSQL.append(" INNER JOIN detalle_cotizacion AS dc ON dc.coti_id = c.coti_id ");
            cadSQL.append(" INNER JOIN aseguradora AS a ON a.aseg_id = dc.aseg_id ");
            cadSQL.append(" INNER JOIN tipo_seguro AS ts ON ts.tise_id = c.tise_id ");
            cadSQL.append(" WHERE c.coti_id = ? ");
            
            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, id, ps);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                datos = new DetalleCotizacionDTO();
                datos.setIdCotizacion(rs.getString("coti_id"));
                datos.setIdTipoDocumento(rs.getString("tido_id"));
                datos.setTipoDocumento(rs.getString("tido_descripcion"));
                datos.setNombre(rs.getString("clie_nombre"));
                datos.setApellido(rs.getString("clie_apellido"));
                datos.setFechaNacimiento(rs.getString("clie_fechanacimiento"));
                datos.setTelefono(rs.getString("clie_telefono"));
                datos.setCorreo(rs.getString("clie_email"));
                datos.setIdVehiculo(rs.getString("vehi_id"));
                datos.setIdmunicipio(rs.getString("muni_id"));
                datos.setMunicipioNombre(rs.getString("muni_nombre"));
                datos.setIdDepartamento(rs.getString("depa_id"));
                datos.setDepartamentoNombre(rs.getString("depa_nombre"));
                datos.setIdUbicacionVehiculo(rs.getString("ubve_id"));
                datos.setUbicacionVehiculo(rs.getString("ubve_descripcion"));
                datos.setIdTipoServicio(rs.getString("tisv_id"));
                datos.setTipoServicio(rs.getString("tisv_descripcion"));
                datos.setLineaVehiculo(rs.getString("cofa_linea"));
                datos.setMarca(rs.getString("cofa_marca"));
                datos.setModelo(rs.getString("vehi_modelo"));
                datos.setPlaca(rs.getString("vehi_placa"));
                datos.setId(rs.getString("deco_id"));
                datos.setIdAseguradora(rs.getString("aseg_id"));
                datos.setAseguradora(rs.getString("aseg_nombre"));
                datos.setIdTipoSeguro(rs.getString("tise_id"));
                datos.setTipoSeguro(rs.getString("tise_nombre"));
                datos.setTiempoDuracion(rs.getString("deco_tiempoduracion"));
                datos.setValorTotalSeguro(rs.getString("deco_valortotalseguro"));
                datos.setValorAsegurado(rs.getString("deco_valorasegurado"));
                datos.setValorReferencia(rs.getString("deco_valorreferencia"));
                datos.setAntecedente(rs.getString("deco_antecedente"));
                datos.setCobertura(rs.getString("deco_cobertura"));
                datos.setInfomacionImportante(rs.getString("deco_informacionimportante"));
                datos.setDocumento(rs.getString("clie_cedula"));
                datos.setObservacionAsesor(rs.getString("coti_observacionasesor"));
            }
            ps.close();
            ps = null;
            
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        
        return datos;
    }

    /**
     * @param parmetros
     * @param conexion
     * @return
     */
    public ArrayList<DetalleCotizacionDTO> listarCotizacionesRealizadas(Connection conexion, DetalleCotizacionDTO parmetros) {
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<DetalleCotizacionDTO> listado = null;
        DetalleCotizacionDTO datos = null;
        StringBuilder cadSQL = null;
        
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.coti_id, td.tido_descripcion, cl.clie_nombre, cl.clie_apellido, cl.clie_fechanacimiento, cl.clie_telefono, ");
            cadSQL.append(" cl.clie_email, m.muni_nombre, d.depa_nombre, u.ubve_descripcion, c.coti_fecha, ");
            cadSQL.append(" tv.tisv_descripcion, cf.cofa_marca, cf.cofa_linea, v.vehi_modelo, v.vehi_placa, ");
            cadSQL.append(" a.aseg_nombre, ts.tise_nombre, dc.deco_tiempoduracion, dc.deco_valortotalseguro, dc.deco_valorasegurado, ");
            cadSQL.append(" dc.deco_valorreferencia, dc.deco_antecedente, dc.deco_cobertura, dc.deco_informacionimportante, cl.clie_cedula, cu.cous_fecharegistro, dc.deco_observacion, c.tise_id ");
            cadSQL.append(" FROM cotizacion AS c ");
            cadSQL.append(" INNER JOIN cliente AS cl ON cl.clie_id = c.clie_id ");
            cadSQL.append(" INNER JOIN tipo_documento AS td ON td.tido_id = cl.tido_id ");
            cadSQL.append(" INNER JOIN vehiculo AS v ON v.vehi_id = c.vehi_id ");
            cadSQL.append(" INNER JOIN municipio AS m ON m.muni_id = v.muni_id ");
            cadSQL.append(" INNER JOIN departamento AS d ON m.depa_id = d.depa_id ");
            cadSQL.append(" INNER JOIN ubicacion_vehiculo AS u ON u.ubve_id = v.ubve_id ");
            cadSQL.append(" INNER JOIN tipo_serviciovehiculo AS tv ON tv.tisv_id = v.tisv_id ");
            cadSQL.append(" INNER JOIN configuracion_fasecolda AS cf ON cf.cofa_codigofasecolda = v.vehi_codigofasecolda ");
            cadSQL.append(" INNER JOIN detalle_cotizacion AS dc ON dc.coti_id = c.coti_id ");
            cadSQL.append(" INNER JOIN aseguradora AS a ON a.aseg_id = dc.aseg_id ");
            cadSQL.append(" INNER JOIN tipo_seguro AS ts ON ts.tise_id = c.tise_id ");
            cadSQL.append("  INNER JOIN cotizacion_usuario AS cu ON cu.coti_id = c.coti_id");
            cadSQL.append(" WHERE c.coti_estado = ? and (cu.cous_fecha BETWEEN '" + parmetros.getFechaInicial() + "' AND '" + parmetros.getFechaFinal() + "') and c.tise_id = ? ");
            
            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, parmetros.getEstadoCotizacion(), ps);
            AsignaAtributoStatement.setString(2, parmetros.getIdTipoSeguro(), ps);
            rs = ps.executeQuery();
            
            listado = new ArrayList();
            
            while (rs.next()) {
                datos = new DetalleCotizacionDTO();
                datos.setIdCotizacion(rs.getString("coti_id"));
                datos.setTipoDocumento(rs.getString("tido_descripcion"));
                datos.setNombre(rs.getString("clie_nombre"));
                datos.setApellido(rs.getString("clie_apellido"));
                datos.setFechaNacimiento(rs.getString("clie_fechanacimiento"));
                datos.setTelefono(rs.getString("clie_telefono"));
                datos.setCorreo(rs.getString("clie_email"));
                datos.setMunicipioNombre(rs.getString("muni_nombre"));
                datos.setDepartamentoNombre(rs.getString("depa_nombre"));
                datos.setUbicacionVehiculo(rs.getString("ubve_descripcion"));
                datos.setFechaCotizacion(rs.getString("coti_fecha"));
                datos.setTipoServicio(rs.getString("tisv_descripcion"));
                datos.setLineaVehiculo(rs.getString("cofa_linea"));
                datos.setMarca(rs.getString("cofa_marca"));
                datos.setModelo(rs.getString("vehi_modelo"));
                datos.setPlaca(rs.getString("vehi_placa"));
                datos.setAseguradora(rs.getString("aseg_nombre"));
                datos.setTipoSeguro(rs.getString("tise_nombre"));
                datos.setTiempoDuracion(rs.getString("deco_tiempoduracion"));
                datos.setValorTotalSeguro(rs.getString("deco_valortotalseguro"));
                datos.setValorAsegurado(rs.getString("deco_valorasegurado"));
                datos.setValorReferencia(rs.getString("deco_valorreferencia"));
                datos.setAntecedente(rs.getString("deco_antecedente"));
                datos.setCobertura(rs.getString("deco_cobertura"));
                datos.setInfomacionImportante(rs.getString("deco_informacionimportante"));
                datos.setDocumento(rs.getString("clie_cedula"));
                datos.setFechaCallCenter(rs.getString("cous_fecharegistro"));
                datos.setObservacion(rs.getString("deco_observacion"));
                datos.setIdTipoSeguro(rs.getString("tise_id"));
                listado.add(datos);
            }
            ps.close();
            ps = null;
            
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param id
     * @return
     */
    public DetalleCotizacionDTO detalleCotizacionSeguroVida(Connection conexion, String id) {
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        DetalleCotizacionDTO datos = null;
        StringBuilder cadSQL = null;
        
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.coti_id, cl.clie_nombre, c.coti_valorasegurado, cl.clie_fechanacimiento, cl.clie_email, cl.clie_apellido, ");
            cadSQL.append(" cl.clie_telefono, m.muni_nombre, dc.deco_tiempoduracion, dc.deco_primacomercialanual, c.tise_id, a.aseg_nombre, c.tise_id, c.coti_observacionasesor ");
            cadSQL.append(" FROM cotizacion AS c ");
            cadSQL.append(" INNER JOIN cliente AS cl ON cl.clie_id = c.clie_id ");
            cadSQL.append(" INNER JOIN municipio AS m ON m.muni_id = cl.muni_id ");
            cadSQL.append(" INNER JOIN detalle_cotizacion AS dc ON dc.coti_id = c.coti_id ");
            cadSQL.append(" INNER JOIN aseguradora AS a ON a.aseg_id = dc.aseg_id ");
            cadSQL.append(" WHERE c.coti_id = ? ");
            
            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, id, ps);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                datos = new DetalleCotizacionDTO();
                datos.setIdCotizacion(rs.getString("coti_id"));
                datos.setNombre(rs.getString("clie_nombre"));
                datos.setValorAsegurado(rs.getString("coti_valorasegurado"));
                datos.setFechaNacimiento(rs.getString("clie_fechanacimiento"));
                datos.setCorreo(rs.getString("clie_email"));
                datos.setTelefono(rs.getString("clie_telefono"));
                datos.setMunicipioNombre(rs.getString("muni_nombre"));
                datos.setTiempoDuracion(rs.getString("deco_tiempoduracion"));
                datos.setPrimaComercialAnual(rs.getString("deco_primacomercialanual"));
                datos.setTipoSeguro(rs.getString("tise_id"));
                datos.setAseguradora(rs.getString("aseg_nombre"));
                datos.setIdTipoSeguro(rs.getString("tise_id"));
                datos.setApellido(rs.getString("clie_apellido"));
                datos.setObservacionAsesor(rs.getString("coti_observacionasesor"));
            }
            ps.close();
            ps = null;
            
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        
        return datos;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarObservacionCotizacion(Connection conexion, CotizacionUsuarioDTO datos) {
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroObservacion = false;
        
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE detalle_cotizacion SET deco_observacion = ? ");
            cadSQL.append(" WHERE coti_id = ? ");
            
            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getObservacion(), ps);
            AsignaAtributoStatement.setString(2, datos.getIdCotizacion(), ps);
            nRows = ps.executeUpdate();
            
            if (nRows > 0) {
                registroObservacion = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroObservacion;
    }

    /**
     *
     * @param conexion
     * @param parmetros
     * @return
     */
    public ArrayList<DetalleCotizacionDTO> listarCotizacionesRealizadasVida(Connection conexion, DetalleCotizacionDTO parmetros) {
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<DetalleCotizacionDTO> listado = null;
        DetalleCotizacionDTO datos = null;
        StringBuilder cadSQL = null;
        
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.coti_id, cl.clie_nombre, cl.clie_telefono, cl.clie_email, cl.clie_fechanacimiento, c.coti_fecha, ");
            cadSQL.append(" a.aseg_nombre, c.coti_valorasegurado, dc.deco_primacomercialanual, cu.cous_fecharegistro, dc.deco_observacion, cl.clie_apellido ");
            cadSQL.append(" FROM cotizacion AS c ");
            cadSQL.append(" INNER JOIN cliente AS cl ON cl.clie_id = c.clie_id ");
            cadSQL.append(" INNER JOIN detalle_cotizacion AS dc ON dc.coti_id = c.coti_id ");
            cadSQL.append(" INNER JOIN aseguradora AS a ON a.aseg_id = dc.aseg_id ");
            cadSQL.append(" INNER JOIN cotizacion_usuario AS cu ON cu.coti_id = c.coti_id");
            cadSQL.append(" WHERE c.coti_estado = ? and (cu.cous_fecha BETWEEN '" + parmetros.getFechaInicial() + "' AND '" + parmetros.getFechaFinal() + "') and c.tise_id = ? ");
            
            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, parmetros.getEstadoCotizacion(), ps);
            AsignaAtributoStatement.setString(2, parmetros.getIdTipoSeguro(), ps);
            rs = ps.executeQuery();
            listado = new ArrayList();
            
            while (rs.next()) {
                datos = new DetalleCotizacionDTO();
                datos.setIdCotizacion(rs.getString("coti_id"));
                datos.setNombre(rs.getString("clie_nombre"));
                datos.setTelefono(rs.getString("clie_telefono"));
                datos.setCorreo(rs.getString("clie_email"));
                datos.setFechaNacimiento(rs.getString("clie_fechanacimiento"));
                datos.setFechaCotizacion(rs.getString("coti_fecha"));
                datos.setAseguradora(rs.getString("aseg_nombre"));
                datos.setValorAsegurado(rs.getString("coti_valorasegurado"));
                datos.setPrimaComercialAnual(rs.getString("deco_primacomercialanual"));
                datos.setFechaCallCenter(rs.getString("cous_fecharegistro"));
                datos.setObservacion(rs.getString("deco_observacion"));
                datos.setApellido(rs.getString("clie_apellido"));
                listado.add(datos);
            }
            ps.close();
            ps = null;
            
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     * @param conexion
     * @param parametros
     * @return
     */
    public ArrayList<DetalleCotizacionDTO> reporteCotizacionesAsesor(Connection conexion, DetalleCotizacionDTO parametros) {
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<DetalleCotizacionDTO> listado = null;
        DetalleCotizacionDTO datos = null;
        StringBuilder cadSQL = null;
        
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT co.coti_id, ts.tise_id, ts.tise_nombre, cl.clie_nombre, cl.clie_apellido, cl.clie_cedula, cl.clie_telefono, cl.clie_email, ");
            cadSQL.append(" CONCAT(de.depa_nombre,', ',mu.muni_nombre) AS ubicacionCliente, uv.ubve_descripcion, cf.cofa_linea, cf.cofa_marca, ");
            cadSQL.append(" ve.vehi_placa, cu.cous_fecharegistro, asg.aseg_nombre, CONCAT(co.coti_valorasegurado,'') AS valorAseguradoVida, ase.ases_nombre, co.coti_fecha, ");
            cadSQL.append(" ase.ases_apellido, ase.ases_documento, ase.ases_porcentajedeauto, ase.ases_porcentajedevida, mu2.muni_nombre as muniVehiculo, tv.tisv_descripcion, ");
            cadSQL.append(" CONCAT(de2.depa_nombre,', ',mu2.muni_nombre) AS ubicacionVehiculo, CONCAT(dc.deco_valorasegurado,'') AS valorAseguradoAuto, ve.vehi_modelo, td.tido_descripcion ");
            cadSQL.append(" FROM cotizacion as co ");
            cadSQL.append(" INNER JOIN tipo_seguro as ts ON ts.tise_id = co.tise_id ");
            cadSQL.append(" INNER JOIN cliente as cl ON cl.clie_id = co.clie_id ");
            cadSQL.append(" INNER JOIN detalle_cotizacion as dc ON dc.coti_id = co.coti_id ");
            cadSQL.append(" INNER JOIN aseguradora as asg ON asg.aseg_id = dc.aseg_id ");
            cadSQL.append(" INNER JOIN asesor as ase ON ase.ases_codigo = co.ases_codigoasesor ");
            cadSQL.append(" INNER JOIN cotizacion_usuario as cu ON cu.coti_id = co.coti_id ");
            cadSQL.append(" LEFT JOIN tipo_documento as td ON td.tido_id = cl.tido_id ");
            cadSQL.append(" LEFT JOIN vehiculo as ve ON ve.vehi_id = co.vehi_id ");
            cadSQL.append(" LEFT JOIN municipio as mu ON mu.muni_id = cl.muni_id ");
            cadSQL.append(" LEFT JOIN municipio as mu2 ON mu2.muni_id = ve.muni_id ");
            cadSQL.append(" LEFT JOIN departamento as de ON de.depa_id = mu.depa_id ");
            cadSQL.append(" LEFT JOIN departamento as de2 ON de2.depa_id = mu2.depa_id ");
            cadSQL.append(" LEFT JOIN configuracion_fasecolda as cf ON cf.cofa_codigofasecolda = ve.vehi_codigofasecolda");
            cadSQL.append(" LEFT JOIN tipo_serviciovehiculo as tv ON tv.tisv_id = ve.tisv_id ");
            cadSQL.append(" LEFT JOIN ubicacion_vehiculo as uv ON uv.ubve_id = ve.ubve_id ");
            cadSQL.append(" WHERE co.coti_fecha BETWEEN '" + parametros.getFechaInicial() + "' AND '" + parametros.getFechaFinal() + "' ");
            
            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();
            
            while (rs.next()) {
                datos = new DetalleCotizacionDTO();
                datos.setIdCotizacion(rs.getString("coti_id"));
                datos.setIdTipoSeguro(rs.getString("tise_id"));
                datos.setTipoSeguro(rs.getString("tise_nombre"));
                datos.setNombre(rs.getString("clie_nombre"));
                datos.setApellido(rs.getString("clie_apellido"));
                datos.setDocumento(rs.getString("clie_cedula"));
                datos.setTelefono(rs.getString("clie_telefono"));
                datos.setCorreo(rs.getString("clie_email"));
                datos.setUbicacionVehiculo(rs.getString("ubve_descripcion"));
                datos.setLineaVehiculo(rs.getString("cofa_linea"));
                datos.setMarca(rs.getString("cofa_marca"));
                datos.setPlaca(rs.getString("vehi_placa"));
                datos.setFechaCallCenter(rs.getString("cous_fecharegistro"));
                datos.setAseguradora(rs.getString("aseg_nombre"));
                datos.setNombreAsesor(rs.getString("ases_nombre"));
                datos.setFechaCotizacion(rs.getString("coti_fecha"));
                datos.setApellidoAsesor(rs.getString("ases_apellido"));
                datos.setDocumentoAsesor(rs.getString("ases_documento"));
                datos.setPorcentajeAuto(rs.getString("ases_porcentajedeauto"));
                datos.setPorcentajeVida(rs.getString("ases_porcentajedevida"));
                String valorAsegurado = null;
                valorAsegurado = rs.getString("valorAseguradoVida");
                if (valorAsegurado != null) {
                    datos.setValorAsegurado(rs.getString("valorAseguradoVida"));
                    float porcentajeVida = Float.parseFloat(datos.getPorcentajeVida());
                    float valorAsegurado1 = Float.parseFloat(datos.getValorAsegurado());
                    float valorPorcentajeVida = (valorAsegurado1 * porcentajeVida) / 100;
                    datos.setValorPorcentajeVida(Formato.formatToMoney(String.valueOf(Math.round(valorPorcentajeVida))));
                } else {
                    datos.setValorAsegurado(rs.getString("valorAseguradoAuto"));
                    float porcentajeAuto = Float.parseFloat(datos.getPorcentajeAuto());
                    float valorAsegurado1 = Float.parseFloat(datos.getValorAsegurado());
                    float valorPorcentajeAuto = (valorAsegurado1 * porcentajeAuto) / 100;
                    datos.setValorPorcentajeAuto(Formato.formatToMoney(String.valueOf(Math.round(valorPorcentajeAuto))));
                }
                String ubicacionCliente = null;
                String ubicacionVehiculo = null;
                ubicacionCliente = rs.getString("ubicacionCliente");
                ubicacionVehiculo = rs.getString("ubicacionVehiculo");
                if (ubicacionCliente != null) {
                    String[] parts = ubicacionCliente.split(", ");
                    String nombreDepa = parts[0];
                    String nombreMuni = parts[1];
                    datos.setDepartamentoNombre(nombreDepa);
                    datos.setMunicipioNombre(nombreMuni);
                } else if (ubicacionVehiculo != null) {
                    String[] parts = ubicacionVehiculo.split(", ");
                    String nombreDepa = parts[0];
                    String nombreMuni = parts[1];
                    datos.setDepartamentoNombre(nombreDepa);
                    datos.setMunicipioNombre(nombreMuni);
                }
                datos.setTipoServicio(rs.getString("tisv_descripcion"));
                datos.setModelo(rs.getString("vehi_modelo"));
                datos.setTipoDocumento(rs.getString("tido_descripcion"));
                listado.add(datos);
            }
            ps.close();
            ps = null;
            
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
}
