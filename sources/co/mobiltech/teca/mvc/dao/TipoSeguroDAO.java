/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.AsignaAtributoStatement;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.TipoSeguroDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author JHONATAN
 */
public class TipoSeguroDAO {

    public ArrayList<TipoSeguroDTO> listarTipoSeguro(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<TipoSeguroDTO> listado = null;
        TipoSeguroDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT tise_id, tise_nombre FROM tipo_seguro ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new TipoSeguroDTO();
                datos.setId(rs.getString("tise_id"));
                datos.setNombre(rs.getString("tise_nombre"));
                listado.add(datos);
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    public String consultarTipoSeguroPorCotizacion(Connection conexion, String id) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        String idTipoSeguro = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.tise_id ");
            cadSQL.append(" FROM cotizacion as c ");
            cadSQL.append(" WHERE c.coti_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, id, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                idTipoSeguro = rs.getString("tise_id");
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }

        return idTipoSeguro;
    }
}
