/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.AsignaAtributoStatement;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.ArchivoFasecoldaDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author JHONATAN
 */
public class ArchivoFasecoldaDAO {

    /**
     *
     * @param conexion
     * @param estado
     * @return
     */
    public boolean inactivarFasecoldasExistentes(Connection conexion, String estado) {

        PreparedStatement ps = null;
        int nRows = 0;
        boolean registro = false;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE configuracion_fasecolda SET cofa_estado = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, estado, ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                registro = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarArchivo(Connection conexion, ArchivoFasecoldaDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroEntidad = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO configuracion_fasecolda (cofa_clase, cofa_marca, cofa_linea, cofa_version, cofa_codigofasecolda, cofa_registradopor) ");
            cadSQL.append(" VALUES ( ?, ?, ?, ?, ?, ?) ");
            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getClase(), ps);
            AsignaAtributoStatement.setString(2, datos.getMarca(), ps);
            AsignaAtributoStatement.setString(3, datos.getLinea(), ps);
            AsignaAtributoStatement.setString(4, datos.getVersion(), ps);
            AsignaAtributoStatement.setString(5, datos.getCodigoFasecolda(), ps);
            AsignaAtributoStatement.setString(6, datos.getRegistradoPor(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    datos.setId(rs.getString(1));
                    registroEntidad = true;
                }
                rs.close();
                rs = null;
            }
        } catch (Exception se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
        }
        return registroEntidad;
    }

}
