/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.AsignaAtributoStatement;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.AsesorDTO;
import co.mobiltech.teca.mvc.dto.AsesorReferidoDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author JHONATAN
 */
public class AsesorDAO {

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public ArrayList<AsesorDTO> listarAsesor(Connection conexion, AsesorDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<AsesorDTO> listado = null;
        AsesorDTO dato = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT a.ases_codigo, a.ases_documento, a.ases_nombre, a.ases_apellido, e.enti_nombre, a.ases_fecharegistro, a.ases_estado ");
            cadSQL.append(" FROM asesor a ");
            cadSQL.append(" INNER JOIN entidad e ON e.enti_id = a.enti_id ");
            if (!datos.getNombre().equals("") && !datos.getDocumento().equals("")) {
                cadSQL.append(" WHERE a.ases_nombre like '" + datos.getNombre() + "%' AND a.ases_documento like '" + datos.getDocumento() + "%' ");
                ps = conexion.prepareStatement(cadSQL.toString());
                rs = ps.executeQuery();
            } else if (!datos.getNombre().equals("") && datos.getDocumento().equals("")) {
                cadSQL.append(" WHERE a.ases_nombre like '" + datos.getNombre() + "%' ");
                ps = conexion.prepareStatement(cadSQL.toString());
                rs = ps.executeQuery();
            } else if (datos.getNombre().equals("") && !datos.getDocumento().equals("")) {
                cadSQL.append(" WHERE a.ases_documento like '" + datos.getDocumento() + "%' ");
                ps = conexion.prepareStatement(cadSQL.toString());
                rs = ps.executeQuery();
            } else if (datos.getNombre().equals("") && datos.getDocumento().equals("")) {
                ps = conexion.prepareStatement(cadSQL.toString());
                rs = ps.executeQuery();
            }

            listado = new ArrayList();

            while (rs.next()) {
                datos = new AsesorDTO();
                datos.setId(rs.getString("ases_codigo"));
                datos.setDocumento(rs.getString("ases_documento"));
                datos.setNombre(rs.getString("ases_nombre"));
                datos.setApellido(rs.getString("ases_apellido"));
                datos.setDescripcionEntidad(rs.getString("enti_nombre"));
                datos.setFechaRegistro(rs.getString("ases_fecharegistro"));
                datos.setEstado(rs.getString("ases_estado"));
                listado.add(datos);
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param documento
     * @return
     */
    public boolean consultarAsesorPorDocumento(Connection conexion, String documento) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder cadSQL = null;
        boolean existe = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT ases_codigo FROM asesor WHERE ases_documento = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, documento, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                existe = true;
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return existe;
        } finally {
            try {
                if (ps != null) {
                    existe = false;
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return existe;
            }
        }

        return existe;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarAsesor(Connection conexion, AsesorDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroEntidad = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO asesor (tido_id, ases_documento, ases_nombre, ases_apellido, ases_correo, ");
            cadSQL.append(" enti_id, ases_porcentajedeauto, ases_porcentajedevida, ases_registradopor) ");
            cadSQL.append(" VALUES ( ?, ?, ?, ? ,? ,? ,? ,? ,? ) ");
            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getIdTipoDocumento(), ps);
            AsignaAtributoStatement.setString(2, datos.getDocumento(), ps);
            AsignaAtributoStatement.setString(3, datos.getNombre(), ps);
            AsignaAtributoStatement.setString(4, datos.getApellido(), ps);
            AsignaAtributoStatement.setString(5, datos.getCorreo(), ps);
            AsignaAtributoStatement.setString(6, datos.getIdEntidad(), ps);
            AsignaAtributoStatement.setString(7, datos.getPorcentajeAuto(), ps);
            AsignaAtributoStatement.setString(8, datos.getPorcentajeVida(), ps);
            AsignaAtributoStatement.setString(9, datos.getRegistradoPor(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    datos.setId(rs.getString(1));
                    registroEntidad = true;
                }
                rs.close();
                rs = null;
            }
        } catch (Exception se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
        }
        return registroEntidad;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean editarEstadoAsesor(Connection conexion, AsesorDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        boolean registro = false;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE asesor SET ases_estado = ? ");
            cadSQL.append(" WHERE ases_codigo = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getEstado(), ps);
            AsignaAtributoStatement.setString(2, datos.getId(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                registro = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public ArrayList<AsesorDTO> listarAsesorPorId(Connection conexion, AsesorDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<AsesorDTO> listado = null;
        AsesorDTO dato = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT a.ases_codigo, a.ases_documento, a.ases_nombre, a.ases_apellido, e.enti_nombre, a.ases_fecharegistro, a.ases_estado ");
            cadSQL.append(" FROM asesor a ");
            cadSQL.append(" INNER JOIN entidad e ON e.enti_id = a.enti_id ");
            cadSQL.append(" WHERE a.ases_codigo = ? ");
            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, datos.getId(), ps);
            rs = ps.executeQuery();

            listado = new ArrayList();

            while (rs.next()) {
                datos = new AsesorDTO();
                datos.setId(rs.getString("ases_codigo"));
                datos.setDocumento(rs.getString("ases_documento"));
                datos.setNombre(rs.getString("ases_nombre"));
                datos.setApellido(rs.getString("ases_apellido"));
                datos.setDescripcionEntidad(rs.getString("enti_nombre"));
                datos.setFechaRegistro(rs.getString("ases_fecharegistro"));
                datos.setEstado(rs.getString("ases_estado"));
                listado.add(datos);
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param cedula
     * @return
     */
    public AsesorDTO buscarAsesorPorCedula(Connection conexion, String cedula) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder cadSQL = null;
        AsesorDTO datosAsesor = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT ases_codigo, ases_documento, ases_nombre, ases_apellido  FROM asesor WHERE ases_documento = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, cedula, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                datosAsesor = new AsesorDTO();
                datosAsesor.setId(rs.getString("ases_codigo"));
                datosAsesor.setDocumento(rs.getString("ases_documento"));
                datosAsesor.setNombre(rs.getString("ases_nombre") + rs.getString("ases_apellido"));
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }

        return datosAsesor;
    }

    /**
     *
     * @param conexion
     * @param idAsesor
     * @return
     */
    public ArrayList<AsesorDTO> buscarReferidosPorIdAsesor(Connection conexion, String idAsesor) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<AsesorDTO> listado = null;
        AsesorDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT ar.asre_id, a.ases_documento, a.ases_nombre, a.ases_apellido, ar.asre_porcentajereferido , ar.ases_idreferido ");
            cadSQL.append(" FROM asesor a ");
            cadSQL.append(" INNER JOIN asesor_referido ar ON a.ases_codigo=ar.ases_idreferido ");
            cadSQL.append(" WHERE ases_idreferido = a.ases_codigo AND ar.ases_id = ? ");
            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idAsesor, ps);
            rs = ps.executeQuery();

            listado = new ArrayList();

            while (rs.next()) {
                datos = new AsesorDTO();
                datos.setId(rs.getString("asre_id"));
                datos.setDocumento(rs.getString("ases_documento"));
                datos.setNombre(rs.getString("ases_nombre"));
                datos.setApellido(rs.getString("ases_apellido"));
                datos.setPorcentajeReferido(rs.getString("asre_porcentajereferido"));
                datos.setIdAsesorReferido(rs.getString("ases_idreferido"));
                listado.add(datos);
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param idAsesorReferido
     * @return
     */
    public boolean desasociarReferido(Connection conexion, String idAsesorReferido) {

        PreparedStatement ps = null;
        int nRows = 0;
        boolean deleteExitoso = false;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" DELETE  FROM asesor_referido WHERE asre_id = ?");
            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);

            AsignaAtributoStatement.setString(1, idAsesorReferido, ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                deleteExitoso = true;
            }
            ps.close();
            ps = null;

        } catch (SQLException e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return false;
            }
        }
        return deleteExitoso;
    }

    /**
     *
     * @param conexion
     * @param idAsesor
     * @param idReferido
     * @return
     */
    public boolean validarReferido(Connection conexion, String idAsesor, String idReferido) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder cadSQL = null;
        boolean existe = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT asre_id FROM asesor_referido WHERE ases_idreferido = ? and ases_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idAsesor, ps);
            AsignaAtributoStatement.setString(2, idReferido, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                existe = true;
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return existe;
        } finally {
            try {
                if (ps != null) {
                    existe = false;
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return existe;
            }
        }

        return existe;
    }

    /**
     *
     * @param conexion
     * @param datosAsesor
     * @return
     */
    public boolean registrarReferido(Connection conexion, AsesorDTO datosAsesor) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroReferido = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO asesor_referido (ases_id, ases_idreferido, asre_porcentajereferido) ");
            cadSQL.append(" VALUES ( ?, ?, ?) ");
            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datosAsesor.getId(), ps);
            AsignaAtributoStatement.setString(2, datosAsesor.getIdAsesorReferido(), ps);
            AsignaAtributoStatement.setString(3, datosAsesor.getPorcentajeReferido(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    datosAsesor.setId(rs.getString(1));
                    registroReferido = true;
                }
                rs.close();
                rs = null;
            }
        } catch (Exception se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
        }
        return registroReferido;
    }

    /**
     *
     * @param conexion
     * @param idAsesor
     * @return
     */
    public boolean asesorExisteComoReferido(Connection conexion, String idAsesor) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder cadSQL = null;
        boolean existe = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT asre_id FROM asesor_referido WHERE ases_idreferido = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idAsesor, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                existe = true;
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return existe;
        } finally {
            try {
                if (ps != null) {
                    existe = false;
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return existe;
            }
        }

        return existe;
    }

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<AsesorReferidoDTO> listarReferidos(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<AsesorReferidoDTO> listado = null;
        AsesorReferidoDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT ar.asre_id, a.ases_codigo, a.ases_documento, a.ases_nombre, a.ases_apellido, a.ases_porcentajedeauto, a.ases_porcentajedevida, ");
            cadSQL.append(" ar.asre_porcentajereferido, a2.ases_codigo, a2.ases_nombre, a2.ases_apellido, a2.ases_documento ");
            cadSQL.append(" FROM asesor_referido ar ");
            cadSQL.append(" INNER JOIN asesor a ON a.ases_codigo=ar.ases_id ");
            cadSQL.append(" INNER JOIN asesor a2 ON a2.ases_codigo=ar.ases_idreferido ");
            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();

            listado = new ArrayList();

            while (rs.next()) {
                datos = new AsesorReferidoDTO();
                datos.setId(rs.getString("ar.asre_id"));
                datos.setIdAsesor(rs.getString("a.ases_codigo"));
                datos.setDocumentoAsesor(rs.getString("a.ases_documento"));
                datos.setNombreAsesor(rs.getString("a.ases_nombre"));
                datos.setApellidoAsesor(rs.getString("a.ases_apellido"));
                datos.setPorcentajeAuto(rs.getString("a.ases_porcentajedeauto"));
                datos.setPorcentajeVida(rs.getString("a.ases_porcentajedevida"));
                datos.setPorcentajeReferido(rs.getString("ar.asre_porcentajereferido"));
                datos.setIdAsesorReferido(rs.getString("a2.ases_codigo"));
                datos.setNombreAsesorReferido(rs.getString("a2.ases_nombre"));
                datos.setApellidoAsesorReferido(rs.getString("a2.ases_apellido"));
                datos.setDocumentoAsesorReferido(rs.getString("a2.ases_documento"));
                listado.add(datos);
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

}
