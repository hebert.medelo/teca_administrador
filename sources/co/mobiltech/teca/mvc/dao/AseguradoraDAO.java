/*
 * FachadaAppTeca.java
 *
 * Proyecto: Cotización de seguros
 * Cliente: Teca
 * Copyright 2017 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.AsignaAtributoStatement;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.AseguradoraDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Ing. Hebert Medelo
 */
public class AseguradoraDAO {

    /**
     * @param conexion
     * @return
     */
    public ArrayList<AseguradoraDTO> listarAseguradoras(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<AseguradoraDTO> listado = null;
        AseguradoraDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT aseg_id, aseg_nombre, aseg_logo, aseg_orden FROM aseguradora ORDER BY aseg_orden; ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new AseguradoraDTO();
                datos.setId(rs.getString("aseg_id"));
                datos.setNombre(rs.getString("aseg_nombre"));
                datos.setLogo(rs.getString("aseg_logo"));
                datos.setOrden(rs.getString("aseg_orden"));
                listado.add(datos);
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarOrdenAseguradora(Connection conexion, AseguradoraDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        boolean registro = false;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE aseguradora SET aseg_orden = ? ");
            cadSQL.append(" WHERE aseg_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getOrden(), ps);
            AsignaAtributoStatement.setString(2, datos.getId(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                registro = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarAseguradora(Connection conexion, AseguradoraDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroFactura = false;

        try {
            cadSQL = new StringBuilder();

            cadSQL.append(" INSERT INTO aseguradora (aseg_orden, aseg_nombre, aseg_logo, aseg_registradopor) ");
            cadSQL.append(" SELECT IFNULL(MAX(aseg_orden),0)+1, ?, ?, ? FROM aseguradora");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getNombre(), ps);
            AsignaAtributoStatement.setString(2, datos.getLogo(), ps);
            AsignaAtributoStatement.setString(3, datos.getRegistradoPor(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    datos.setId(rs.getString(1));
                    registroFactura = true;
                }
                rs.close();
                rs = null;
            }
        } catch (Exception se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
        }
        return registroFactura;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarLogo(Connection conexion, AseguradoraDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        boolean registro = false;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE aseguradora SET aseg_logo = ? ");
            cadSQL.append(" WHERE aseg_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getLogo(), ps);
            AsignaAtributoStatement.setString(2, datos.getId(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                registro = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean editarAseguradora(Connection conexion, AseguradoraDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        boolean registro = false;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE aseguradora SET aseg_nombre = ? ");
            cadSQL.append(" WHERE aseg_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getNombre(), ps);
            AsignaAtributoStatement.setString(2, datos.getId(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                registro = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }
    
    public boolean editarAseguradoraConLogo(Connection conexion, AseguradoraDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        boolean registro = false;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE aseguradora SET aseg_nombre = ?, aseg_logo = ? ");
            cadSQL.append(" WHERE aseg_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getNombre(), ps);
            AsignaAtributoStatement.setString(2, datos.getLogo(), ps);
            AsignaAtributoStatement.setString(3, datos.getId(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                registro = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }
}
