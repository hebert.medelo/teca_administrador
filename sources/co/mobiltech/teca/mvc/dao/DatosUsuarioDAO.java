/*
 * DatosUsuarioDAO.java
 *
 * Proyecto: Gestion de Creditos
 * Cliente: Promociones Empresariales
 * Copyright 2016 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.AsignaAtributoStatement;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.DatosUsuarioDTO;
import co.mobiltech.teca.mvc.dto.UsuarioDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Sys. E. Diego Armando Hernandez
 */
public class DatosUsuarioDAO {

    /**
     *
     * @param conexion
     * @param usuario
     * @return
     */
    public DatosUsuarioDTO consultarDatosUsuarioLogueado(Connection conexion, String usuario) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        DatosUsuarioDTO datosUsuario = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT usua.usua_id, usua.usua_nombre,usua.usua_usuario, usua.usua_clave, usua.usua_correo, usua.usua_estado, usua.usua_imagenperfil, usua.tius_id, tius.tius_descripcion");
            cadSQL.append("   FROM teca.usuario usua ");
            cadSQL.append(" INNER JOIN teca.tipo_usuario tius ON usua.tius_id = tius.tius_id AND usua.usua_usuario  = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, usuario, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                datosUsuario = new DatosUsuarioDTO();
                datosUsuario.setIdUsuario(rs.getString("usua_id"));
                datosUsuario.setNombre(rs.getString("usua_nombre"));
                datosUsuario.setUsuario(rs.getString("usua_usuario"));
                datosUsuario.setClave(rs.getString("usua_clave"));
                datosUsuario.setCorreo(rs.getString("usua_correo"));
                datosUsuario.setEstado(rs.getString("usua_estado"));
                datosUsuario.setImagenPerfil(rs.getString("usua_imagenperfil"));
                datosUsuario.setIdTipoUsuario(rs.getString("tius_id"));
                datosUsuario.setTipoUsuario(rs.getString("tius_descripcion"));
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return datosUsuario;
    }

    /**
     *
     * @param conexion
     * @param correo
     * @return
     */
    public DatosUsuarioDTO recuperarContrasenia(Connection conexion, String correo) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        DatosUsuarioDTO datosUsuario = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT usua_id, usua_correo, usua_clave ");
            cadSQL.append(" FROM usuario");
            cadSQL.append(" WHERE usua_correo = ?");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, correo, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                datosUsuario = new DatosUsuarioDTO();
                datosUsuario.setIdUsuario(rs.getString("usua_id"));
                datosUsuario.setCorreo(rs.getString("usua_correo"));
                datosUsuario.setClave(rs.getString("usua_clave"));
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return datosUsuario;
    }

    /**
     *
     * @param conexion
     * @param usuaId
     * @param contrasenia
     * @return
     */
    public boolean generarContraseña(Connection conexion, String usuaId, String contrasenia) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append("UPDATE usuario SET usua_clave = SHA2(?,256) WHERE  usua_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, contrasenia, ps);
            AsignaAtributoStatement.setString(2, usuaId, ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @param datosUsuario
     * @return
     */
    public boolean validarContrasenia(Connection conexion, DatosUsuarioDTO datosUsuario) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder cadSQL = null;
        boolean validado = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT usua_id, usua_correo, usua_clave ");
            cadSQL.append(" FROM usuario");
            cadSQL.append(" WHERE usua_clave = SHA2(?,256) AND usua_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, datosUsuario.getClave(), ps);
            AsignaAtributoStatement.setString(2, datosUsuario.getIdUsuario(), ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                datosUsuario = new DatosUsuarioDTO();
                datosUsuario.setIdUsuario(rs.getString("usua_id"));
                datosUsuario.setCorreo(rs.getString("usua_correo"));
                datosUsuario.setClave(rs.getString("usua_clave"));
                validado = true;
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return false;
            }
        }
        return validado;
    }

    /**
     *
     * @param conexion
     * @param datosUsuario
     * @return
     */
    public boolean cambiarContrasenia(Connection conexion, DatosUsuarioDTO datosUsuario) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append("UPDATE usuario SET usua_clave = SHA2(?,256) WHERE usua_id = ? AND usua_clave = SHA2(?,256) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datosUsuario.getNuevaClave(), ps);
            AsignaAtributoStatement.setString(2, datosUsuario.getIdUsuario(), ps);
            AsignaAtributoStatement.setString(3, datosUsuario.getClave(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @param cedula
     * @return
     */
    public DatosUsuarioDTO consultarUsuarioPorCedula(Connection conexion, String cedula) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        DatosUsuarioDTO datosUsuario = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT *");
            cadSQL.append(" FROM usuario ");
            cadSQL.append(" WHERE usua_cedula = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, cedula, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                datosUsuario = new DatosUsuarioDTO();
                datosUsuario.setIdUsuario(rs.getString("usua_id"));
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return datosUsuario;
    }

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<UsuarioDTO> listarUsuarios(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<UsuarioDTO> listado = null;
        UsuarioDTO datosUsuario = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT u.usua_id, u.usua_nombre, u.usua_apellido, u.usua_correo, u.usua_usuario, u.tius_id, t.tius_descripcion, u.usua_estado");
            cadSQL.append(" FROM usuario u");
            cadSQL.append(" INNER JOIN tipo_usuario t on t.tius_id=u.tius_id ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datosUsuario = new UsuarioDTO();
                datosUsuario.setIdUsuario(rs.getString("usua_id"));
                datosUsuario.setNombre(rs.getString("usua_nombre"));
                datosUsuario.setApellido(rs.getString("usua_apellido"));
                datosUsuario.setCorreo(rs.getString("usua_correo"));
                datosUsuario.setUsuario(rs.getString("usua_usuario"));
                datosUsuario.setIdTipoUsuario(rs.getString("tius_id"));
                datosUsuario.setTipoUsuario(rs.getString("tius_descripcion"));
                datosUsuario.setEstado(rs.getString("usua_estado"));
                listado.add(datosUsuario);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean editarEstadoUsuario(Connection conexion, UsuarioDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        boolean registro = false;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE usuario SET usua_estado = ? ");
            cadSQL.append(" WHERE usua_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getEstado(), ps);
            AsignaAtributoStatement.setString(2, datos.getIdUsuario(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                registro = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @param conexion
     * @param usuario
     * @return
     */
    public DatosUsuarioDTO consultarUsuarioPorUsuario(Connection conexion, String usuario) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        DatosUsuarioDTO datosUsuario = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT *");
            cadSQL.append(" FROM usuario ");
            cadSQL.append(" WHERE usua_usuario = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, usuario, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                datosUsuario = new DatosUsuarioDTO();
                datosUsuario.setIdUsuario(rs.getString("usua_id"));
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return datosUsuario;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarUsuario(Connection conexion, UsuarioDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroUsuario = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO usuario (usua_nombre, usua_apellido, usua_cedula, usua_usuario, usua_clave, usua_correo, tius_id, usua_registradopor) ");
            cadSQL.append(" VALUES ( ?, ?, ?, ?, SHA2(?,256), ?, ?, ? ) ");
            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getNombre(), ps);
            AsignaAtributoStatement.setString(2, datos.getApellido(), ps);
            AsignaAtributoStatement.setString(3, datos.getCedula(), ps);
            AsignaAtributoStatement.setString(4, datos.getUsuario(), ps);
            AsignaAtributoStatement.setString(5, datos.getClave(), ps);
            AsignaAtributoStatement.setString(6, datos.getCorreo(), ps);
            AsignaAtributoStatement.setString(7, datos.getIdTipoUsuario(), ps);
            AsignaAtributoStatement.setString(8, datos.getRegistradoPor(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    datos.setIdUsuario(rs.getString(1));
                    registroUsuario = true;
                }
                rs.close();
                rs = null;
            }
        } catch (Exception se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
        }
        return registroUsuario;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean editarUsuario(Connection conexion, UsuarioDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        boolean registro = false;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE usuario SET usua_nombre = ?, usua_apellido = ?, tius_id = ?, usua_correo = ?, usua_clave = SHA2(?,256) ");
            cadSQL.append(" WHERE usua_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getNombre(), ps);
            AsignaAtributoStatement.setString(2, datos.getApellido(), ps);
            AsignaAtributoStatement.setString(3, datos.getIdTipoUsuario(), ps);
            AsignaAtributoStatement.setString(4, datos.getCorreo(), ps);
            AsignaAtributoStatement.setString(5, datos.getClave(), ps);
            AsignaAtributoStatement.setString(6, datos.getIdUsuario(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                registro = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }
}
