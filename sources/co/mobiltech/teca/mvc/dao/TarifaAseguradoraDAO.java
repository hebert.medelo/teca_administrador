/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.AsignaAtributoStatement;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.TarifaAseguradoraDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author HEBERT A. MEDELO
 */
public class TarifaAseguradoraDAO {

    /**
     * @param anio
     * @param conexion
     * @param idAseguradora
     * @return
     */
    public ArrayList<TarifaAseguradoraDTO> listarTarifas(Connection conexion, String idAseguradora, String anio) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<TarifaAseguradoraDTO> listado = null;
        TarifaAseguradoraDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT taas_id, taas_vigencia, taas_edad, taas_ipt, aseg_id ");
            cadSQL.append(" FROM tarifa_aseguradora ");
            cadSQL.append(" WHERE aseg_id = ? AND taas_vigencia = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idAseguradora, ps);
            AsignaAtributoStatement.setString(2, anio, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new TarifaAseguradoraDTO();
                datos.setId(rs.getString("taas_id"));
                datos.setVigencia(rs.getString("taas_vigencia"));
                datos.setEdad(rs.getString("taas_edad"));
                datos.setIpt(rs.getString("taas_ipt"));
                datos.setIdAseguradora(rs.getString("aseg_id"));
                listado.add(datos);
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarTarifas(Connection conexion, TarifaAseguradoraDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroFactura = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO tarifa_aseguradora (aseg_id, taas_vigencia, taas_edad, taas_ipt, taas_registradopor, arch_id) ");
            cadSQL.append(" VALUES(?, ?, ?, ?, ?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getIdAseguradora(), ps);
            AsignaAtributoStatement.setString(2, datos.getVigencia(), ps);
            AsignaAtributoStatement.setString(3, datos.getEdad(), ps);
            AsignaAtributoStatement.setString(4, datos.getIpt(), ps);
            AsignaAtributoStatement.setString(5, datos.getRegistradoPor(), ps);
            AsignaAtributoStatement.setString(6, datos.getIdArchivo(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    datos.setId(rs.getString(1));
                    registroFactura = true;
                }
                rs.close();
                rs = null;
            }
        } catch (Exception se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
        }
        return registroFactura;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean validarTarifa(Connection conexion, TarifaAseguradoraDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<TarifaAseguradoraDTO> listado = null;
        boolean validado = false;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT aseg_id, taas_vigencia, taas_edad ");
            cadSQL.append(" FROM tarifa_aseguradora ");
            cadSQL.append(" WHERE  aseg_id = ? AND taas_vigencia = ? AND taas_edad = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, datos.getIdAseguradora(), ps);
            AsignaAtributoStatement.setString(2, datos.getVigencia(), ps);
            AsignaAtributoStatement.setString(3, datos.getEdad(), ps);

            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                validado = true;
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return validado;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return validado;
            }
        }
        return validado;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean validarTarifaEditar(Connection conexion, TarifaAseguradoraDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<TarifaAseguradoraDTO> listado = null;
        boolean validado = false;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT aseg_id, taas_vigencia, taas_edad ");
            cadSQL.append(" FROM tarifa_aseguradora ");
            cadSQL.append(" WHERE  aseg_id = ? AND taas_vigencia = ? AND taas_edad = ? AND taas_id != ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, datos.getIdAseguradora(), ps);
            AsignaAtributoStatement.setString(2, datos.getVigencia(), ps);
            AsignaAtributoStatement.setString(3, datos.getEdad(), ps);
            AsignaAtributoStatement.setString(4, datos.getId(), ps);

            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                validado = true;
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return validado;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return validado;
            }
        }
        return validado;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean editarTarifa(Connection conexion, TarifaAseguradoraDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        boolean registro = false;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE tarifa_aseguradora SET aseg_id = ?, taas_vigencia = ?, taas_edad = ?, taas_ipt = ?, taas_registradopor = ? ");
            cadSQL.append(" WHERE taas_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getIdAseguradora(), ps);
            AsignaAtributoStatement.setString(2, datos.getVigencia(), ps);
            AsignaAtributoStatement.setString(3, datos.getEdad(), ps);
            AsignaAtributoStatement.setString(4, datos.getIpt(), ps);
            AsignaAtributoStatement.setString(5, datos.getRegistradoPor(), ps);
            AsignaAtributoStatement.setString(6, datos.getId(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                registro = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }
}
