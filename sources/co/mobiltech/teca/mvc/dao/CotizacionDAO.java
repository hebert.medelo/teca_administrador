/*
 * FachadaAppTeca.java
 *
 * Proyecto: Cotización de seguros
 * Cliente: Teca
 * Copyright 2017 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.AsignaAtributoStatement;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.CotizacionDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Ing. Hebert Medelo
 */
public class CotizacionDAO {

    /**
     * @param datos
     * @param conexion
     * @return
     */
    public ArrayList<CotizacionDTO> listarCotizaciones(Connection conexion, CotizacionDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<CotizacionDTO> listado = null;
        CotizacionDTO datosCotizacion = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT distinct c.coti_id, c.coti_fecha, c.coti_estado, v.vehi_codigofasecolda, d.aseg_id, a.aseg_nombre, v.vehi_placa, cli.clie_cedula, cli.clie_nombre, cli.clie_apellido ");
            cadSQL.append(" FROM cotizacion AS c ");
            cadSQL.append(" INNER JOIN detalle_cotizacion AS d ON d.coti_id = c.coti_id ");
            cadSQL.append(" INNER JOIN aseguradora AS a ON a.aseg_id = d.aseg_id ");
            cadSQL.append(" INNER JOIN vehiculo AS v ON v.vehi_id = c.vehi_id ");
            cadSQL.append(" INNER JOIN cliente AS cli ON cli.clie_id = c.clie_id ");
            cadSQL.append(" WHERE c.coti_estado = ? AND c.tise_id = ? AND ((c.coti_fecha BETWEEN '" + datos.getFechaInicial() + "' AND '" + datos.getFechaFinal() + "' ) OR v.vehi_placa = ? OR cli.clie_cedula = ? OR cli.clie_nombre = ? OR cli.clie_apellido = ?) ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, datos.getEstado(), ps);
            AsignaAtributoStatement.setString(2, datos.getIdTipoSeguro(), ps);
            AsignaAtributoStatement.setString(3, datos.getPlaca(), ps);
            AsignaAtributoStatement.setString(4, datos.getCedula(), ps);
            AsignaAtributoStatement.setString(5, datos.getNombre(), ps);
            AsignaAtributoStatement.setString(6, datos.getApellido(), ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datosCotizacion = new CotizacionDTO();
                datosCotizacion.setId(rs.getString("coti_id"));
                datosCotizacion.setFecha(rs.getString("coti_fecha"));
                datosCotizacion.setEstado(rs.getString("coti_estado"));
                datosCotizacion.setCodigoFasecolda(rs.getString("vehi_codigofasecolda"));
                datosCotizacion.setIdAseguradora(rs.getString("aseg_id"));
                datosCotizacion.setNombraAseguradora(rs.getString("aseg_nombre"));
                datosCotizacion.setPlaca(rs.getString("vehi_placa"));
                datosCotizacion.setCedula(rs.getString("clie_cedula"));
                datosCotizacion.setNombre(rs.getString("clie_nombre"));
                datosCotizacion.setApellido(rs.getString("clie_apellido"));
                listado.add(datosCotizacion);
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param id
     * @param estado
     * @return
     */
    public boolean actualizarEstado(Connection conexion, String id, String estado) {

        PreparedStatement ps = null;
        int nRows = 0;
        boolean datos = false;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE cotizacion SET coti_estado = ? ");
            cadSQL.append(" WHERE coti_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, estado, ps);
            AsignaAtributoStatement.setString(2, id, ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                datos = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return datos;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public ArrayList<CotizacionDTO> listarCotizacionesSeguroVida(Connection conexion, CotizacionDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<CotizacionDTO> listado = null;
        CotizacionDTO datosCotizacion = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.coti_id, c.coti_fecha, c.coti_estado, d.aseg_id, cli.clie_nombre, cli.clie_nombre, c.coti_valorasegurado, cli.clie_apellido, a.aseg_nombre ");
            cadSQL.append(" FROM cotizacion AS c ");
            cadSQL.append(" INNER JOIN cliente AS cli ON cli.clie_id = c.clie_id ");
            cadSQL.append(" INNER JOIN detalle_cotizacion AS d ON d.coti_id = c.coti_id ");
            cadSQL.append(" INNER JOIN aseguradora AS a ON a.aseg_id = d.aseg_id ");
            if (!datos.getNombre().equals("") && !datos.getApellido().equals("")) {
                cadSQL.append(" WHERE coti_estado = ? AND (c.coti_fecha BETWEEN '" + datos.getFechaInicial() + "' AND '" + datos.getFechaFinal() + "' ) AND cli.clie_nombre like '" + datos.getNombre() + "%' AND cli.clie_apellido like '" + datos.getApellido() + "%'  AND c.tise_id = ? ");
                ps = conexion.prepareStatement(cadSQL.toString());
                AsignaAtributoStatement.setString(1, datos.getEstado(), ps);
                AsignaAtributoStatement.setString(2, datos.getIdTipoSeguro(), ps);
                rs = ps.executeQuery();
            } else if (!datos.getNombre().equals("") && datos.getApellido().equals("")) {
                cadSQL.append(" WHERE coti_estado = ? AND (c.coti_fecha BETWEEN '" + datos.getFechaInicial() + "' AND '" + datos.getFechaFinal() + "' ) AND cli.clie_nombre like '" + datos.getNombre() + "%'  AND c.tise_id = ? ");
                ps = conexion.prepareStatement(cadSQL.toString());
                AsignaAtributoStatement.setString(1, datos.getEstado(), ps);
                AsignaAtributoStatement.setString(2, datos.getIdTipoSeguro(), ps);
                rs = ps.executeQuery();
            } else if (datos.getNombre().equals("") && !datos.getApellido().equals("")) {
                cadSQL.append(" WHERE coti_estado = ? AND (c.coti_fecha BETWEEN '" + datos.getFechaInicial() + "' AND '" + datos.getFechaFinal() + "' ) AND cli.clie_apellido like '" + datos.getApellido() + "%'  AND c.tise_id = ? ");
                ps = conexion.prepareStatement(cadSQL.toString());
                AsignaAtributoStatement.setString(1, datos.getEstado(), ps);
                AsignaAtributoStatement.setString(2, datos.getIdTipoSeguro(), ps);
                rs = ps.executeQuery();
            } else if (datos.getNombre().equals("") && datos.getApellido().equals("")) {
                cadSQL.append(" WHERE coti_estado = ? AND (c.coti_fecha BETWEEN '" + datos.getFechaInicial() + "' AND '" + datos.getFechaFinal() + "' ) AND c.tise_id = ? ");
                ps = conexion.prepareStatement(cadSQL.toString());
                AsignaAtributoStatement.setString(1, datos.getEstado(), ps);
                AsignaAtributoStatement.setString(2, datos.getIdTipoSeguro(), ps);
                rs = ps.executeQuery();
            }
            listado = new ArrayList();

            while (rs.next()) {
                datosCotizacion = new CotizacionDTO();
                datosCotizacion.setId(rs.getString("coti_id"));
                datosCotizacion.setFecha(rs.getString("coti_fecha"));
                datosCotizacion.setEstado(rs.getString("coti_estado"));
                datosCotizacion.setIdAseguradora(rs.getString("aseg_id"));
                datosCotizacion.setNombraAseguradora(rs.getString("aseg_nombre"));
                datosCotizacion.setNombre(rs.getString("clie_nombre"));
                datosCotizacion.setValorAsegurado(rs.getString("coti_valorasegurado"));
                datosCotizacion.setApellido(rs.getString("clie_apellido"));
                listado.add(datosCotizacion);
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param parametros
     * @return
     */
    public ArrayList<CotizacionDTO> listarPosiblesCotizacionesAuto(Connection conexion, CotizacionDTO parametros) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<CotizacionDTO> listado = null;
        CotizacionDTO datos = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.coti_id, td.tido_descripcion, cl.clie_nombre, cl.clie_apellido, cl.clie_fechanacimiento, cl.clie_telefono, cl.clie_cedula, ");
            cadSQL.append(" cl.clie_email, m.muni_nombre, d.depa_nombre, u.ubve_descripcion, c.coti_fecha, tv.tisv_descripcion, cf.cofa_linea, cf.cofa_marca, ");
            cadSQL.append(" v.vehi_modelo, v.vehi_placa, ts.tise_nombre, c.tise_id, v.vehi_codigofasecolda ");
            cadSQL.append(" FROM cotizacion AS c ");
            cadSQL.append(" INNER JOIN cliente AS cl ON cl.clie_id = c.clie_id ");
            cadSQL.append(" INNER JOIN tipo_documento AS td ON td.tido_id = cl.tido_id ");
            cadSQL.append(" INNER JOIN vehiculo AS v ON v.vehi_id = c.vehi_id ");
            cadSQL.append(" INNER JOIN municipio AS m ON m.muni_id = v.muni_id ");
            cadSQL.append(" INNER JOIN departamento AS d ON m.depa_id = d.depa_id ");
            cadSQL.append(" INNER JOIN ubicacion_vehiculo AS u ON u.ubve_id = v.ubve_id ");
            cadSQL.append(" INNER JOIN tipo_serviciovehiculo AS tv ON tv.tisv_id = v.tisv_id ");
            cadSQL.append(" INNER JOIN configuracion_fasecolda AS cf ON cf.cofa_codigofasecolda = v.vehi_codigofasecolda ");
            cadSQL.append(" INNER JOIN tipo_seguro AS ts ON ts.tise_id = c.tise_id ");
            cadSQL.append(" WHERE c.coti_estado = ? and (c.coti_fecha BETWEEN '" + parametros.getFechaInicial() + "' AND '" + parametros.getFechaFinal() + "') and c.tise_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, parametros.getEstado(), ps);
            AsignaAtributoStatement.setString(2, parametros.getIdTipoSeguro(), ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new CotizacionDTO();
                datos.setId(rs.getString("coti_id"));
                datos.setTipoDocumento(rs.getString("tido_descripcion"));
                datos.setNombre(rs.getString("clie_nombre"));
                datos.setApellido(rs.getString("clie_apellido"));
                datos.setFechaNacimiento(rs.getString("clie_fechanacimiento"));
                datos.setTelefono(rs.getString("clie_telefono"));
                datos.setCorreo(rs.getString("clie_email"));
                datos.setCiudad(rs.getString("muni_nombre"));
                datos.setDepartamento(rs.getString("depa_nombre"));
                datos.setUbicacionVehiculo(rs.getString("ubve_descripcion"));
                datos.setFecha(rs.getString("coti_fecha"));
                datos.setTipoServicio(rs.getString("tisv_descripcion"));
                datos.setLinea(rs.getString("cofa_linea"));
                datos.setMarca(rs.getString("cofa_marca"));
                datos.setModelo(rs.getString("vehi_modelo"));
                datos.setPlaca(rs.getString("vehi_placa"));
                datos.setTipoSeguro(rs.getString("tise_nombre"));
                datos.setCedula(rs.getString("clie_cedula"));
                datos.setIdTipoSeguro(rs.getString("tise_id"));
                datos.setCodigoFasecolda(rs.getString("vehi_codigofasecolda"));
                datos.setTipoServicio(rs.getString("tisv_descripcion"));
                listado.add(datos);
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param parametros
     * @return
     */
    public ArrayList<CotizacionDTO> listarPosiblesCotizacionesVida(Connection conexion, CotizacionDTO parametros) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<CotizacionDTO> listado = null;
        CotizacionDTO datos = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT c.coti_id, cl.clie_nombre, cl.clie_telefono, cl.clie_email, cl.clie_fechanacimiento, c.coti_fecha, c.coti_valorasegurado, cl.clie_apellido ");
            cadSQL.append(" FROM cotizacion AS c ");
            cadSQL.append(" INNER JOIN cliente AS cl ON cl.clie_id = c.clie_id ");
            cadSQL.append(" WHERE c.coti_estado = ? and (c.coti_fecha BETWEEN '" + parametros.getFechaInicial() + "' AND '" + parametros.getFechaFinal() + "') and c.tise_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, parametros.getEstado(), ps);
            AsignaAtributoStatement.setString(2, parametros.getIdTipoSeguro(), ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new CotizacionDTO();
                datos.setId(rs.getString("coti_id"));
                datos.setNombre(rs.getString("clie_nombre"));
                datos.setTelefono(rs.getString("clie_telefono"));
                datos.setCorreo(rs.getString("clie_email"));
                datos.setFechaNacimiento(rs.getString("clie_fechanacimiento"));
                datos.setFecha(rs.getString("coti_fecha"));
                datos.setValorAsegurado(rs.getString("coti_valorasegurado"));
                datos.setApellido(rs.getString("clie_apellido"));
                listado.add(datos);
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
}
