/*
 * FachadaAppTeca.java
 *
 * Proyecto: Cotización de seguros
 * Cliente: Teca
 * Copyright 2017 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.AsignaAtributoStatement;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.PrioridadParametrizacionDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

/**
 *
 * @author Ing. Hebert Medelo
 */
public class PrioridadParametrizacionDAO {

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarPrioridadParametrizacion(Connection conexion, PrioridadParametrizacionDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        boolean registro = false;
        StringBuilder cadSQL = null;

        try {
            
            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE prioridad_parametrizacion SET prpa_estado = ?, prpa_ordenamiento = ? ");
            cadSQL.append(" WHERE prpa_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getEstado(), ps);
            AsignaAtributoStatement.setString(2, datos.getOrden(), ps);
            AsignaAtributoStatement.setString(3, datos.getId(), ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                registro = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }
}
