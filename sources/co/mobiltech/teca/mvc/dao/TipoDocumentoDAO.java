/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.TipoDocumentoDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class TipoDocumentoDAO {

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<TipoDocumentoDTO> listarTipoDocumento(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<TipoDocumentoDTO> listado = null;
        TipoDocumentoDTO datosTipoDocumento = null;

        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT tido_id, tido_codigo, tido_descripcion FROM tipo_documento ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList<TipoDocumentoDTO>();

            while (rs.next()) {
                datosTipoDocumento = new TipoDocumentoDTO();
                datosTipoDocumento.setId(rs.getString("tido_id"));
                datosTipoDocumento.setCodigo(rs.getString("tido_codigo"));
                datosTipoDocumento.setDescripcion(rs.getString("tido_descripcion"));

                listado.add(datosTipoDocumento);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

}
