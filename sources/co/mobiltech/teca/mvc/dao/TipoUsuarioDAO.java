/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.TipoUsuarioDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class TipoUsuarioDAO {

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<TipoUsuarioDTO> listarTipoUsuario(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<TipoUsuarioDTO> listado = null;
        TipoUsuarioDTO datosTipoUsuario = null;

        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT tius_id, tius_descripcion FROM tipo_usuario ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList<TipoUsuarioDTO>();

            while (rs.next()) {
                datosTipoUsuario = new TipoUsuarioDTO();
                datosTipoUsuario.setId(rs.getString("tius_id"));
                datosTipoUsuario.setDescripcion(rs.getString("tius_descripcion"));
                listado.add(datosTipoUsuario);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

}
