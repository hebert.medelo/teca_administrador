/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.AsignaAtributoStatement;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.FuncionalidadDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class FuncionalidadDAO {

    /**
     *
     * @param conexion
     * @param idMenu
     * @param idUsuario
     * @return
     */
    public ArrayList<FuncionalidadDTO> listarFuncionalidadesPorMenu(Connection conexion, String idMenu, String idUsuario) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<FuncionalidadDTO> listado = null;
        FuncionalidadDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT func.func_id, func.func_pagina, func.func_titulo ");
            cadSQL.append(" FROM funcionalidad AS func ");
            cadSQL.append(" INNER JOIN tipousuario_funcionalidad tifu ON tifu.func_id = func.func_id");
            cadSQL.append(" WHERE menu_id = ? AND  tifu.tius_id = ? ");
            cadSQL.append(" ORDER BY tifu.func_id asc ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idMenu, ps);
            AsignaAtributoStatement.setString(2, idUsuario, ps);
            rs = ps.executeQuery();
            listado = new ArrayList<FuncionalidadDTO>();

            while (rs.next()) {
                datos = new FuncionalidadDTO();
                datos.setId(rs.getString("func_id"));
                datos.setPagina(rs.getString("func_pagina"));
                datos.setTitulo(rs.getString("func_titulo"));

                listado.add(datos);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
}
