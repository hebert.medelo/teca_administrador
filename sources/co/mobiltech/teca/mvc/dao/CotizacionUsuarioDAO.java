/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.mvc.dao;

import co.mobiltech.teca.common.util.AsignaAtributoStatement;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dto.CotizacionUsuarioDTO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Administrator
 */
public class CotizacionUsuarioDAO {

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarCotizacionUsuario(Connection conexion, CotizacionUsuarioDTO datos, String fecha) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroFactura = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO cotizacion_usuario(coti_id, usua_id, cous_fecha) ");
            cadSQL.append(" VALUES(?, ?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getIdCotizacion(), ps);
            AsignaAtributoStatement.setString(2, datos.getIdUsuario(), ps);
            AsignaAtributoStatement.setString(3, fecha, ps);
            nRows = ps.executeUpdate();

            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    datos.setId(rs.getString(1));
                    registroFactura = true;
                }
                rs.close();
                rs = null;
            }
        } catch (Exception se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
        }
        return registroFactura;
    }
}
