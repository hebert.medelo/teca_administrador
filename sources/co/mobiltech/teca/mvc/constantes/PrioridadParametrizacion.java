/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.mvc.constantes;

/**
 *
 * @author Administrator
 */
public class PrioridadParametrizacion {

    //ID's Formas de ordenar.
    public static final String POR_ASEGURADORA = "1";
    public static final String POR_VALOR_DETALLE = "2";

    //Estados Formas de ordenar.
    public static final String ESTADO_ACTIVO = "1";
    public static final String ESTADO_INACTIVO = "0";

    //Formas de ordenar Decendente/Acendente.
    public static final String ASCENDENTE = "1";
    public static final String DESCENDENTE = "0";
}
