/*
 * Constantes.java
 *
 * Proyecto: Doctor Pin
 * Cliente: 
 * Copyright 2016 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.constantes;

/**
 *
 * @author Sys. E. Diego Armando Hernandez
 *
 */
public interface Constantes {

    public static final String CORREO = "joseramirez.teca1@gmail.com";
    public static final String CLAVE_CORREO = "Joseramirez";
    public static final String FASECOLDA_ACTIVO = "1";
    public static final String FASECOLDA_INACTIVO = "0";
}
