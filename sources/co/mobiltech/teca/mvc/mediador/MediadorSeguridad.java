 /*
 * MediadorSeguridad.java
 *
 * Proyecto: Gestion de Creditos
 * Cliente: Promociones Empresariales
 * Copyright 2016 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.mediador;

import co.mobiltech.teca.common.connection.ContextDataResourceNames;
import co.mobiltech.teca.common.connection.DataBaseConnection;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.dao.DatosUsuarioDAO;
import co.mobiltech.teca.mvc.dao.FuncionalidadDAO;
import co.mobiltech.teca.mvc.dao.MenuDAO;
import co.mobiltech.teca.mvc.dto.DatosUsuarioDTO;
import co.mobiltech.teca.mvc.dto.FuncionalidadDTO;
import co.mobiltech.teca.mvc.dto.MenuDTO;
import java.sql.Connection;
import java.util.ArrayList;

/**
 *
 * @author Sys. E. Diego Armando Hernandez
 */
public class MediadorSeguridad {

    /**
     *
     */
    private final static MediadorSeguridad instancia = null;
    private final LoggerMessage logMsg;

    /**
     *
     */
    public MediadorSeguridad() {
        logMsg = LoggerMessage.getInstancia();
    }

    /**
     *
     * @return
     */
    public static synchronized MediadorSeguridad getInstancia() {
        return instancia == null ? new MediadorSeguridad() : instancia;
    }

    /**
     *
     * @param usuario
     * @return
     */
    public DatosUsuarioDTO consultarDatosUsuarioLogueado(String usuario) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        DatosUsuarioDTO datosUsuario = null;
        ArrayList<MenuDTO> datosMenu = null;
        ArrayList<FuncionalidadDTO> datosFuncionalidades = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            datosUsuario = new DatosUsuarioDAO().consultarDatosUsuarioLogueado(conexion, usuario);

            if (datosUsuario == null) {
                throw new Exception("Error : al consultar usuario");
            }

            datosMenu = new MenuDAO().listarMenusPorUsuario(conexion, datosUsuario.getIdTipoUsuario());

            if (datosMenu == null) {
                throw new Exception("No se encotro menu para este tipo de usuario");
            }

            datosUsuario.setMenu(datosMenu);

            for (int i = 0; i < datosMenu.size(); i++) {
                datosFuncionalidades = new FuncionalidadDAO().listarFuncionalidadesPorMenu(conexion, datosMenu.get(i).getId(), datosUsuario.getIdTipoUsuario());
                if (datosFuncionalidades == null) {
                    throw new Exception("No se encontro funcionalidad para este tipo de usuario");
                }
                datosMenu.get(i).setFuncionalidad(datosFuncionalidades);
            }

        } catch (Exception e) {
            logMsg.loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                logMsg.loggerMessageException(e);
            }
        }
        return datosUsuario;
    }

    public DatosUsuarioDTO consultaUsuarioPorId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public DatosUsuarioDTO consultarUsuarioLogeado(String usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * LOS METODOS APARTIR DE AQUI NO HAN SIDO VALIDADOS
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     */
}
