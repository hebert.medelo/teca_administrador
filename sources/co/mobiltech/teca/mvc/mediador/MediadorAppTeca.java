/*
 * FachadaAppTeca.java
 *
 * Proyecto: Cotización de seguros
 * Cliente: Teca
 * Copyright 2017 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.mediador;

import co.mobiltech.teca.common.connection.ContextDataResourceNames;
import co.mobiltech.teca.common.connection.DataBaseConnection;
import co.mobiltech.teca.common.util.Formato;
import co.mobiltech.teca.common.util.Generales;
import co.mobiltech.teca.common.util.LoggerMessage;
import co.mobiltech.teca.mvc.constantes.Constantes;
import co.mobiltech.teca.mvc.constantes.Cotizacion;
import co.mobiltech.teca.mvc.constantes.PrioridadParametrizacion;
import co.mobiltech.teca.mvc.constantes.TipoSeguro;
import co.mobiltech.teca.mvc.dao.ArchivoFasecoldaDAO;
import co.mobiltech.teca.mvc.dao.AseguradoraDAO;
import co.mobiltech.teca.mvc.dao.AsesorDAO;
import co.mobiltech.teca.mvc.dao.CotizacionDAO;
import co.mobiltech.teca.mvc.dao.CotizacionUsuarioDAO;
import co.mobiltech.teca.mvc.dao.DatosUsuarioDAO;
import co.mobiltech.teca.mvc.dao.DetalleCotizacionDAO;
import co.mobiltech.teca.mvc.dao.EntidadDAO;
import co.mobiltech.teca.mvc.dao.PrioridadParametrizacionDAO;
import co.mobiltech.teca.mvc.dao.TarifaAseguradoraDAO;
import co.mobiltech.teca.mvc.dao.TipoDocumentoDAO;
import co.mobiltech.teca.mvc.dao.TipoSeguroDAO;
import co.mobiltech.teca.mvc.dao.TipoUsuarioDAO;
import co.mobiltech.teca.mvc.dto.ArchivoFasecoldaDTO;
import co.mobiltech.teca.mvc.dto.AseguradoraDTO;
import co.mobiltech.teca.mvc.dto.AsesorDTO;
import co.mobiltech.teca.mvc.dto.AsesorReferidoDTO;
import co.mobiltech.teca.mvc.dto.CotizacionDTO;
import co.mobiltech.teca.mvc.dto.CotizacionUsuarioDTO;
import co.mobiltech.teca.mvc.dto.DatosUsuarioDTO;
import co.mobiltech.teca.mvc.dto.DetalleCotizacionDTO;
import co.mobiltech.teca.mvc.dto.ListadoTarifasDTO;
import co.mobiltech.teca.mvc.dto.TarifaAseguradoraDTO;
import co.mobiltech.teca.mvc.dto.EntidadDTO;
import co.mobiltech.teca.mvc.dto.PrioridadParametrizacionDTO;
import co.mobiltech.teca.mvc.dto.TipoDocumentoDTO;
import co.mobiltech.teca.mvc.dto.TipoSeguroDTO;
import co.mobiltech.teca.mvc.dto.TipoUsuarioDTO;
import co.mobiltech.teca.mvc.dto.UsuarioDTO;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import javax.servlet.http.HttpSession;
import org.directwebremoting.WebContextFactory;

/**
 *
 * @author Ing. Hebert Medelo
 */
public class MediadorAppTeca {

    /**
     *
     */
    private final static MediadorAppTeca instancia = null;
    private final LoggerMessage logMsg;

    /**
     *
     */
    public MediadorAppTeca() {
        logMsg = LoggerMessage.getInstancia();
    }

    /**
     *
     * @return
     */
    public static synchronized MediadorAppTeca getInstancia() {
        return instancia == null ? new MediadorAppTeca() : instancia;
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * LOS METODOS APARTIR DE AQUI NO HAN SIDO VALIDADOS
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     *
     */
    /**
     * @param datos
     * @return
     */
    public ArrayList<CotizacionDTO> listarCotizaciones(CotizacionDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<CotizacionDTO> listado = null;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            datos.setEstado(Cotizacion.ESTADO_COTIZACION_ACEPTA_CLIENTE);
            if (datos.getIdTipoSeguro().equals(TipoSeguro.AUTO)) {
                listado = new CotizacionDAO().listarCotizaciones(conexion, datos);
            } else {
                listado = new CotizacionDAO().listarCotizacionesSeguroVida(conexion, datos);
            }

            if (listado != null) {
                for (int i = 0; i < listado.size(); i++) {
                    listado.get(i).setConcecutivo(Integer.toString(i + 1));
                    String fecha = Formato.formatoFechaMostrar(listado.get(i).getFecha());
                    listado.get(i).setFecha(fecha);
                }
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     * @param id
     * @return
     */
    public DetalleCotizacionDTO detalleCotizacion(String id) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        DetalleCotizacionDTO datos = null;
        String idTipoSeguro = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            idTipoSeguro = new TipoSeguroDAO().consultarTipoSeguroPorCotizacion(conexion, id);

            if (idTipoSeguro.equals(TipoSeguro.AUTO)) {
                datos = new DetalleCotizacionDAO().detalleCotizacion(conexion, id);
            } else {
                datos = new DetalleCotizacionDAO().detalleCotizacionSeguroVida(conexion, id);
            }
            if (datos != null) {
                datos.setValorAsegurado(Formato.formatToMoney(datos.getValorAsegurado()));
                datos.setPrimaComercialAnual(Formato.formatToMoney(datos.getPrimaComercialAnual()));
                datos.setValorTotalSeguro(Formato.formatToMoney(datos.getValorTotalSeguro()));
            }

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return datos;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarCotizacionUsuario(CotizacionUsuarioDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registro = false;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);
            conexion.setAutoCommit(false);

            HttpSession session = WebContextFactory.get().getSession();
            DatosUsuarioDTO usuarioLogueado = (DatosUsuarioDTO) session.getAttribute("datosUsuario");

            datos.setIdUsuario(usuarioLogueado.getIdUsuario());
            Date hoy = new Date();
            Format formatter = new SimpleDateFormat("yyyy-MM-dd");
            String fecha = formatter.format(hoy);

            if (!new CotizacionUsuarioDAO().registrarCotizacionUsuario(conexion, datos, fecha)) {
                throw new Exception("Error al realizar el registro.");
            }

            if (!new DetalleCotizacionDAO().registrarObservacionCotizacion(conexion, datos)) {
                throw new Exception("Error al registrar la observacion de la cotización.");
            }

            String estado = Cotizacion.ESTADO_COTIZACION_ACEPTA_CALLCENTER;
            if (!new CotizacionDAO().actualizarEstado(conexion, datos.getIdCotizacion(), estado)) {
                throw new Exception("Error al actualizar el estado de la cotizacion.");
            }

            registro = true;

        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (registro) {
                    conexion.commit();
                } else {
                    conexion.rollback();
                }
                conexion.setAutoCommit(true);
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     * @return
     */
    public ArrayList<AseguradoraDTO> listarAseguradoras() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<AseguradoraDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            listado = new AseguradoraDAO().listarAseguradoras(conexion);

            if (listado == null) {
                throw new Exception("Error al listar aseguradoras.");
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean actualizarPrioridadParametrizacion(PrioridadParametrizacionDTO datos) {

        Connection conexion = null;
        DataBaseConnection dbcon = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);
            conexion.setAutoCommit(false);

            if (datos.getAseguradora().equals(PrioridadParametrizacion.ESTADO_ACTIVO)) {

                if (datos.getListadoAseguradoras() != null && !datos.getListadoAseguradoras().isEmpty()) {
                    for (int i = 0; i < datos.getListadoAseguradoras().size(); i++) {

                        if (!new AseguradoraDAO().actualizarOrdenAseguradora(conexion, datos.getListadoAseguradoras().get(i))) {
                            throw new Exception("Error al realizar el registro.");
                        }
                    }
                }
                datos.setEstado(PrioridadParametrizacion.ESTADO_ACTIVO);
                datos.setId(PrioridadParametrizacion.POR_ASEGURADORA);
                datos.setOrden(PrioridadParametrizacion.ASCENDENTE);
                if (!new PrioridadParametrizacionDAO().actualizarPrioridadParametrizacion(conexion, datos)) {
                    throw new Exception("Error al realizar el registro.");
                }

                datos.setEstado(PrioridadParametrizacion.ESTADO_INACTIVO);
                datos.setId(PrioridadParametrizacion.POR_VALOR_DETALLE);
                datos.setOrden(PrioridadParametrizacion.ASCENDENTE);
                if (!new PrioridadParametrizacionDAO().actualizarPrioridadParametrizacion(conexion, datos)) {
                    throw new Exception("Error al realizar el registro.");
                }
            }

            if (datos.getValorDetalle().equals(PrioridadParametrizacion.ESTADO_ACTIVO)) {

                datos.setEstado(PrioridadParametrizacion.ESTADO_ACTIVO);
                datos.setId(PrioridadParametrizacion.POR_VALOR_DETALLE);

                if (!new PrioridadParametrizacionDAO().actualizarPrioridadParametrizacion(conexion, datos)) {
                    throw new Exception("Error al realizar el registro.");
                }

                datos.setEstado(PrioridadParametrizacion.ESTADO_INACTIVO);
                datos.setId(PrioridadParametrizacion.POR_ASEGURADORA);
                datos.setOrden(PrioridadParametrizacion.ASCENDENTE);
                if (!new PrioridadParametrizacionDAO().actualizarPrioridadParametrizacion(conexion, datos)) {
                    throw new Exception("Error al realizar el registro.");
                }
            }

            registro = true;

        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (registro) {
                    conexion.commit();
                } else {
                    conexion.rollback();
                }
                conexion.setAutoCommit(true);
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     * @param datos
     * @return
     */
    public ArrayList<DetalleCotizacionDTO> listarCotizacionesRealizadas(DetalleCotizacionDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<DetalleCotizacionDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            datos.setEstadoCotizacion(Cotizacion.ESTADO_COTIZACION_ACEPTA_CALLCENTER);
            if (datos.getIdTipoSeguro().equals(TipoSeguro.AUTO)) {
                listado = new DetalleCotizacionDAO().listarCotizacionesRealizadas(conexion, datos);
            } else {
                listado = new DetalleCotizacionDAO().listarCotizacionesRealizadasVida(conexion, datos);
            }

            if (listado == null) {
                throw new Exception("No se encontraron cotizaciones.");
            }
            for (int i = 0; i < listado.size(); i++) {
                listado.get(i).setValorAsegurado(Formato.formatToMoney(listado.get(i).getValorAsegurado()));
                listado.get(i).setValorTotalSeguro(Formato.formatToMoney(listado.get(i).getValorTotalSeguro()));
                listado.get(i).setValorReferencia(Formato.formatToMoney(listado.get(i).getValorReferencia()));
                listado.get(i).setPrimaComercialAnual(Formato.formatToMoney(listado.get(i).getPrimaComercialAnual()));
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @return
     */
    public ArrayList<TipoSeguroDTO> listarTipoSeguro() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<TipoSeguroDTO> listado = null;
        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            listado = new TipoSeguroDAO().listarTipoSeguro(conexion);

            if (listado == null) {
                throw new Exception("Error al listar aseguradoras.");
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            if (listado != null && listado.isEmpty()) {
                listado = null;
            }
        }
        return listado;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarAseguradora(AseguradoraDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);
            conexion.setAutoCommit(false);

            HttpSession session = WebContextFactory.get().getSession();
            DatosUsuarioDTO usuarioLogueado = (DatosUsuarioDTO) session.getAttribute("datosUsuario");
            datos.setRegistradoPor(usuarioLogueado.getUsuario());

            if (!new AseguradoraDAO().registrarAseguradora(conexion, datos)) {
                throw new Exception("Error al realizar el registro.");
            }

            String logo = "img" + datos.getId() + datos.getLogo();
            datos.setLogo(logo);

            if (!new AseguradoraDAO().actualizarLogo(conexion, datos)) {
                throw new Exception("Error al realizar el registro.");
            }
            session.setAttribute("logo", logo);
            registro = true;

        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (registro) {
                    conexion.commit();
                } else {
                    conexion.rollback();
                }
                conexion.setAutoCommit(true);

                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }

        }
        return registro;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean editarAseguradora(AseguradoraDTO datos) {

        Connection conexion = null;
        DataBaseConnection dbcon = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            HttpSession session = WebContextFactory.get().getSession();
            if (datos.getLogo() != "") {
                String logo = "img" + datos.getId() + datos.getLogo();
                datos.setLogo(logo);
                session.setAttribute("logo", logo);
                if (!new AseguradoraDAO().editarAseguradoraConLogo(conexion, datos)) {
                    throw new Exception("Error al realizar la actualizacion.");
                }
            } else {
                if (!new AseguradoraDAO().editarAseguradora(conexion, datos)) {
                    throw new Exception("Error al realizar la actualizacion.");
                }
            }
            registro = true;

            conexion.close();
            conexion = null;
        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @return
     */
    public ArrayList<EntidadDTO> listarEntidades() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<EntidadDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            listado = new EntidadDAO().listarEntidades(conexion);
            if (listado != null) {
                for (int i = 0; i < listado.size(); i++) {
                    listado.get(i).setFechaCreacion(Formato.formatoFechaMostrar(listado.get(i).getFechaCreacion()));
                }
            }

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarEntidad(EntidadDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            HttpSession session = WebContextFactory.get().getSession();
            DatosUsuarioDTO usuarioLogueado = (DatosUsuarioDTO) session.getAttribute("datosUsuario");
            datos.setRegistradoPor(usuarioLogueado.getUsuario());

            if (!new EntidadDAO().registrarEntidad(conexion, datos)) {
                throw new Exception("Error al realizar el registro.");
            }
            registro = true;

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }

        }
        return registro;
    }

    /**
     *
     * @return
     */
    public ArrayList<TipoDocumentoDTO> listarTipoDocumento() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<TipoDocumentoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            listado = new TipoDocumentoDAO().listarTipoDocumento(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param datos
     * @return
     */
    public ArrayList<AsesorDTO> listarAsesor(AsesorDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<AsesorDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            listado = new AsesorDAO().listarAsesor(conexion, datos);
            if (listado != null) {
                for (int i = 0; i < listado.size(); i++) {
                    listado.get(i).setFechaRegistro(Formato.formatoFechaMostrar(listado.get(i).getFechaRegistro()));
                }
            }

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param documento
     * @return
     */
    public boolean consultarAsesorPorDocumento(String documento) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean existe = false;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            existe = new AsesorDAO().consultarAsesorPorDocumento(conexion, documento);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return existe;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarAsesor(AsesorDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            HttpSession session = WebContextFactory.get().getSession();
            DatosUsuarioDTO usuarioLogueado = (DatosUsuarioDTO) session.getAttribute("datosUsuario");
            datos.setRegistradoPor(usuarioLogueado.getUsuario());

            if (!new AsesorDAO().registrarAsesor(conexion, datos)) {
                throw new Exception("Error al realizar el registro.");
            }
            registro = true;

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }

        }
        return registro;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean editarEstadoAsesor(AsesorDTO datos) {

        Connection conexion = null;
        DataBaseConnection dbcon = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            if (!new AsesorDAO().editarEstadoAsesor(conexion, datos)) {
                throw new Exception("Error al realizar la actualizacion.");
            }
            registro = true;

            conexion.close();
            conexion = null;
        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @param datos
     * @return
     */
    public ArrayList<AsesorDTO> listarAsesorPorId(AsesorDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<AsesorDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            listado = new AsesorDAO().listarAsesorPorId(conexion, datos);
            if (listado != null) {
                for (int i = 0; i < listado.size(); i++) {
                    listado.get(i).setFechaRegistro(Formato.formatoFechaMostrar(listado.get(i).getFechaRegistro()));
                }
            }

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean editarEntidad(EntidadDTO datos) {

        Connection conexion = null;
        DataBaseConnection dbcon = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            if (!new EntidadDAO().editarEntidad(conexion, datos)) {
                throw new Exception("Error al realizar la modificación.");
            }
            registro = true;

            conexion.close();
            conexion = null;
        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     * @param anio
     * @return
     */
    public ArrayList<ListadoTarifasDTO> listarTarifas(String anio) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<TarifaAseguradoraDTO> listado = null;
        ArrayList<AseguradoraDTO> aseguradoras = null;
        ArrayList<ListadoTarifasDTO> listadoTarifas = null;
        ListadoTarifasDTO tarifas = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            aseguradoras = new AseguradoraDAO().listarAseguradoras(conexion);

            if (aseguradoras != null) {
                listadoTarifas = new ArrayList();
                for (int i = 0; i < aseguradoras.size(); i++) {
                    listado = new TarifaAseguradoraDAO().listarTarifas(conexion, aseguradoras.get(i).getId(), anio);
                    if (listado != null) {
                        tarifas = new ListadoTarifasDTO();
                        tarifas.setIdAseguradora(aseguradoras.get(i).getId());
                        tarifas.setNombreAseguradora(aseguradoras.get(i).getNombre());
                        tarifas.setTarifasAseguradoras(listado);
                        listadoTarifas.add(tarifas);
                    }
                }
            }

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listadoTarifas != null && listadoTarifas.isEmpty()) {
                    listadoTarifas = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listadoTarifas;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarTarifas(TarifaAseguradoraDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registro = false;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            HttpSession session = WebContextFactory.get().getSession();
            DatosUsuarioDTO usuarioLogueado = (DatosUsuarioDTO) session.getAttribute("datosUsuario");
            datos.setRegistradoPor(usuarioLogueado.getUsuario());

            if (!new TarifaAseguradoraDAO().registrarTarifas(conexion, datos)) {
                throw new Exception("Error al realizar el registro.");
            }
            registro = true;

            conexion.close();
            conexion = null;
        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean validarTarifa(TarifaAseguradoraDTO datos) {

        Connection conexion = null;
        DataBaseConnection dbcon = null;
        boolean rta = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            rta = new TarifaAseguradoraDAO().validarTarifa(conexion, datos);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            rta = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return rta;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean validarTarifaEditar(TarifaAseguradoraDTO datos) {

        Connection conexion = null;
        DataBaseConnection dbcon = null;
        boolean rta = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            rta = new TarifaAseguradoraDAO().validarTarifaEditar(conexion, datos);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            rta = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return rta;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean editarTarifa(TarifaAseguradoraDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registro = false;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            HttpSession session = WebContextFactory.get().getSession();
            DatosUsuarioDTO usuarioLogueado = (DatosUsuarioDTO) session.getAttribute("datosUsuario");
            datos.setRegistradoPor(usuarioLogueado.getUsuario());

            if (!new TarifaAseguradoraDAO().editarTarifa(conexion, datos)) {
                throw new Exception("Error al realizar el registro.");
            }
            registro = true;

            conexion.close();
            conexion = null;
        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     * @param datos
     * @return
     */
    public ArrayList<DetalleCotizacionDTO> reporteCotizacionesAsesor(DetalleCotizacionDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<DetalleCotizacionDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            datos.setEstadoCotizacion(Cotizacion.ESTADO_COTIZACION_ACEPTA_CALLCENTER);
            listado = new DetalleCotizacionDAO().reporteCotizacionesAsesor(conexion, datos);

            if (listado == null) {
                throw new Exception("No se encontraron cotizaciones.");
            }
            for (int i = 0; i < listado.size(); i++) {
                listado.get(i).setValorAsegurado(Formato.formatToMoney(listado.get(i).getValorAsegurado()));
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param correo
     * @return
     */
    public DatosUsuarioDTO recuperarContrasenia(String correo) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        DatosUsuarioDTO datosUsuario = null;
        String nuevaContrasenia = Generales.EMPTYSTRING;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            datosUsuario = new DatosUsuarioDAO().recuperarContrasenia(conexion, correo);

            if (datosUsuario == null) {
                throw new Exception("no se encotraron datos con con ese correo");
            }

            String usauId = datosUsuario.getIdUsuario();

            nuevaContrasenia = UUID.randomUUID().toString().substring(0, 6);

            if (!new DatosUsuarioDAO().generarContraseña(conexion, usauId, nuevaContrasenia)) {
                throw new Exception("no se actualiza la contraseña");
            }

            datosUsuario.setClave(nuevaContrasenia);

            conexion.close();
            conexion = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return datosUsuario;
    }

    /**
     *
     * @param datosUsuarioLlega
     * @return
     */
    public boolean validarContrasenia(DatosUsuarioDTO datosUsuarioLlega) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;

        boolean validado = false;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            HttpSession session = WebContextFactory.get().getSession();
            DatosUsuarioDTO datosUsuario = (DatosUsuarioDTO) session.getAttribute("datosUsuario");

            datosUsuarioLlega.setIdUsuario(datosUsuario.getIdUsuario());

            validado = new DatosUsuarioDAO().validarContrasenia(conexion, datosUsuarioLlega);

            conexion.close();
            conexion = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return validado;
    }

    /**
     *
     * @param datosUsuarioLlega
     * @return
     */
    public boolean cambiarContrasenia(DatosUsuarioDTO datosUsuarioLlega) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;

        boolean editarUsuario = false;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            HttpSession session = WebContextFactory.get().getSession();
            DatosUsuarioDTO datosUsuario = (DatosUsuarioDTO) session.getAttribute("datosUsuario");

            datosUsuarioLlega.setIdUsuario(datosUsuario.getIdUsuario());

            if (!new DatosUsuarioDAO().cambiarContrasenia(conexion, datosUsuarioLlega)) {
                throw new Exception("Error, no se pudo actualizar la contraseña");
            }

            editarUsuario = true;
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return editarUsuario;
    }

    /**
     *
     * @return
     */
    public ArrayList<TipoUsuarioDTO> listarTipoUsuario() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<TipoUsuarioDTO> listado = null;
        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            listado = new TipoUsuarioDAO().listarTipoUsuario(conexion);

            if (listado == null) {
                throw new Exception("Error al listar tipos de usuario.");
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            if (listado != null && listado.isEmpty()) {
                listado = null;
            }
        }
        return listado;
    }

    /**
     *
     * @param cedula
     * @return
     */
    public boolean validarExistenciaUsuarioCedula(String cedula) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        DatosUsuarioDTO datosUsuario = null;
        boolean validado = false;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            datosUsuario = new DatosUsuarioDAO().consultarUsuarioPorCedula(conexion, cedula);
            if (datosUsuario != null) {
                validado = true;
            }

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return validado;
    }

    /**
     *
     * @return
     */
    public ArrayList<UsuarioDTO> listarUsuarios() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<UsuarioDTO> listado = null;
        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            listado = new DatosUsuarioDAO().listarUsuarios(conexion);

            if (listado == null) {
                throw new Exception("Error al listar datos de usuarios.");
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            if (listado != null && listado.isEmpty()) {
                listado = null;
            }
        }
        return listado;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean editarEstadoUsuario(UsuarioDTO datos) {

        Connection conexion = null;
        DataBaseConnection dbcon = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            if (!new DatosUsuarioDAO().editarEstadoUsuario(conexion, datos)) {
                throw new Exception("Error al realizar la actualizacion.");
            }
            registro = true;
            conexion.close();
            conexion = null;
        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @param usuario
     * @return
     */
    public boolean validarExistenciaUsuario(String usuario) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        DatosUsuarioDTO datosUsuario = null;
        boolean validado = false;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            datosUsuario = new DatosUsuarioDAO().consultarUsuarioPorUsuario(conexion, usuario);
            if (datosUsuario != null) {
                validado = true;
            }
            conexion.close();
            conexion = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return validado;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarUsuario(UsuarioDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            HttpSession session = WebContextFactory.get().getSession();
            DatosUsuarioDTO usuarioLogueado = (DatosUsuarioDTO) session.getAttribute("datosUsuario");
            datos.setRegistradoPor(usuarioLogueado.getUsuario());

            if (!new DatosUsuarioDAO().registrarUsuario(conexion, datos)) {
                throw new Exception("Error al realizar el registro.");
            }
            registro = true;
            conexion.close();
            conexion = null;
        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }

        }
        return registro;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean editarUsuario(UsuarioDTO datos) {

        Connection conexion = null;
        DataBaseConnection dbcon = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            if (!new DatosUsuarioDAO().editarUsuario(conexion, datos)) {
                throw new Exception("Error al realizar la actualizacion.");
            }
            registro = true;
            conexion.close();
            conexion = null;
        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @param cedula
     * @return
     */
    public AsesorDTO buscarAsesorPorCedula(String cedula) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        AsesorDTO datosAsesor = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            datosAsesor = new AsesorDAO().buscarAsesorPorCedula(conexion, cedula);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return datosAsesor;
    }

    /**
     *
     * @param idAsesor
     * @return
     */
    public ArrayList<AsesorDTO> buscarReferidosPorIdAsesor(String idAsesor) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<AsesorDTO> listado = null;
        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            listado = new AsesorDAO().buscarReferidosPorIdAsesor(conexion, idAsesor);

            if (listado == null) {
                throw new Exception("Error al listar datos de usuarios.");
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            if (listado != null && listado.isEmpty()) {
                listado = null;
            }
        }
        return listado;
    }

    /**
     *
     * @param idAsesorReferido
     * @return
     */
    public boolean desasociarReferido(String idAsesorReferido) {

        Connection conexion = null;
        DataBaseConnection dbcon = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            if (!new AsesorDAO().desasociarReferido(conexion, idAsesorReferido)) {
                throw new Exception("Error al realizar la desasociacion.");
            }
            registro = true;

            conexion.close();
            conexion = null;
        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @param idAsesor
     * @param idReferido
     * @return
     */
    public boolean validarReferido(String idAsesor, String idReferido) {

        Connection conexion = null;
        DataBaseConnection dbcon = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            registro = new AsesorDAO().validarReferido(conexion, idAsesor, idReferido);

            conexion.close();
            conexion = null;
        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @param datosAsesor
     * @return
     */
    public boolean registrarReferido(AsesorDTO datosAsesor) {

        Connection conexion = null;
        DataBaseConnection dbcon = null;
        boolean registro = false;
        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            if (!new AsesorDAO().registrarReferido(conexion, datosAsesor)) {
                throw new Exception("Error al realizar el registro del referido.");
            }
            registro = true;
            conexion.close();
            conexion = null;
        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @param idAsesor
     * @return
     */
    public boolean asesorExisteComoReferido(String idAsesor) {

        Connection conexion = null;
        DataBaseConnection dbcon = null;
        boolean registro = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            registro = new AsesorDAO().asesorExisteComoReferido(conexion, idAsesor);

            conexion.close();
            conexion = null;
        } catch (Exception e) {
            registro = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registro;
    }

    /**
     *
     * @return
     */
    public ArrayList<AsesorReferidoDTO> listarReferidos() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<AsesorReferidoDTO> listado = null;
        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            listado = new AsesorDAO().listarReferidos(conexion);

            if (listado == null) {
                throw new Exception("Error al listar datos de usuarios.");
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            if (listado != null && listado.isEmpty()) {
                listado = null;
            }
        }
        return listado;
    }

    /**
     *
     * @param listadoFasecolda
     * @return
     */
    public boolean registrarConfiguracionFasecolda(ArrayList<ArchivoFasecoldaDTO> listadoFasecolda) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;
        boolean desactivaFasecoldaAnterior = false;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);
            conexion.setAutoCommit(false);

            desactivaFasecoldaAnterior = new ArchivoFasecoldaDAO().inactivarFasecoldasExistentes(conexion, Constantes.FASECOLDA_INACTIVO);

            for (int i = 0; i < listadoFasecolda.size(); i++) {
                registroExitoso = new ArchivoFasecoldaDAO().registrarArchivo(conexion, listadoFasecolda.get(i));
                if (!registroExitoso) {
                    registroExitoso = false;
                    break;
                }
            }
            if (registroExitoso) {
                conexion.commit();
            } else {
                conexion.rollback();
            }

        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (SQLException e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param datos
     * @return
     */
    public ArrayList<CotizacionDTO> listarPosiblesCotizaciones(CotizacionDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<CotizacionDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);

            datos.setEstado(Cotizacion.ESTADO_COTIZACION_CREADA);
            if (datos.getIdTipoSeguro().equals(TipoSeguro.AUTO)) {
                listado = new CotizacionDAO().listarPosiblesCotizacionesAuto(conexion, datos);
            } else {
                listado = new CotizacionDAO().listarPosiblesCotizacionesVida(conexion, datos);
            }

            if (listado == null) {
                throw new Exception("No se encontraron cotizaciones.");
            }
            for (int i = 0; i < listado.size(); i++) {
                if (listado.get(i).getValorAsegurado() != "") {
                    listado.get(i).setValorAsegurado(Formato.formatToMoney(listado.get(i).getValorAsegurado()));
                }
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

}
