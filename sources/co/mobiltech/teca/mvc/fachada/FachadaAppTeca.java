/*
 * FachadaAppTeca.java
 *
 * Proyecto: Cotización de seguros
 * Cliente: Teca
 * Copyright 2017 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.fachada;

import co.mobiltech.teca.mvc.dto.ArchivoFasecoldaDTO;
import co.mobiltech.teca.mvc.dto.AseguradoraDTO;
import co.mobiltech.teca.mvc.dto.AsesorDTO;
import co.mobiltech.teca.mvc.dto.AsesorReferidoDTO;
import co.mobiltech.teca.mvc.dto.CotizacionDTO;
import co.mobiltech.teca.mvc.dto.CotizacionUsuarioDTO;
import co.mobiltech.teca.mvc.dto.DatosUsuarioDTO;
import co.mobiltech.teca.mvc.dto.DetalleCotizacionDTO;
import co.mobiltech.teca.mvc.dto.EntidadDTO;
import co.mobiltech.teca.mvc.dto.ListadoTarifasDTO;
import co.mobiltech.teca.mvc.dto.PrioridadParametrizacionDTO;
import co.mobiltech.teca.mvc.dto.TarifaAseguradoraDTO;
import co.mobiltech.teca.mvc.dto.TipoDocumentoDTO;
import co.mobiltech.teca.mvc.dto.TipoSeguroDTO;
import co.mobiltech.teca.mvc.dto.TipoUsuarioDTO;
import co.mobiltech.teca.mvc.dto.UsuarioDTO;
import co.mobiltech.teca.mvc.mediador.MediadorAppTeca;
import java.util.ArrayList;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.directwebremoting.annotations.ScriptScope;

/**
 *
 * @author Ing. Hebert Medelo
 */
@RemoteProxy(name = "ajaxTeca", scope = ScriptScope.SESSION)
public class FachadaAppTeca {

    /**
     *
     */
    public FachadaAppTeca() {
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public boolean servicioActivo() {
        return true;
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * LOS METODOS APARTIR DE AQUI NO HAN SIDO VALIDADOS
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     */
    /**
     * @param datos
     * @return
     */
    @RemoteMethod
    public ArrayList<CotizacionDTO> listarCotizaciones(CotizacionDTO datos) {
        return MediadorAppTeca.getInstancia().listarCotizaciones(datos);
    }

    /**
     * @param id
     * @return
     */
    @RemoteMethod
    public DetalleCotizacionDTO detalleCotizacion(String id) {
        return MediadorAppTeca.getInstancia().detalleCotizacion(id);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarCotizacionUsuario(CotizacionUsuarioDTO datos) {
        return MediadorAppTeca.getInstancia().registrarCotizacionUsuario(datos);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<AseguradoraDTO> listarAseguradoras() {
        return MediadorAppTeca.getInstancia().listarAseguradoras();
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean actualizarPrioridadParametrizacion(PrioridadParametrizacionDTO datos) {
        return MediadorAppTeca.getInstancia().actualizarPrioridadParametrizacion(datos);
    }

    @RemoteMethod
    public ArrayList<TipoSeguroDTO> listarTipoSeguro() {
        return MediadorAppTeca.getInstancia().listarTipoSeguro();
    }

    /**
     * @param datos
     * @return
     */
    @RemoteMethod
    public ArrayList<DetalleCotizacionDTO> listarCotizacionesRealizadas(DetalleCotizacionDTO datos) {
        return MediadorAppTeca.getInstancia().listarCotizacionesRealizadas(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarAseguradora(AseguradoraDTO datos) {
        return MediadorAppTeca.getInstancia().registrarAseguradora(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean editarAseguradora(AseguradoraDTO datos) {
        return MediadorAppTeca.getInstancia().editarAseguradora(datos);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<EntidadDTO> listarEntidades() {
        return MediadorAppTeca.getInstancia().listarEntidades();
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarEntidad(EntidadDTO datos) {
        return MediadorAppTeca.getInstancia().registrarEntidad(datos);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<TipoDocumentoDTO> listarTipoDocumento() {
        return MediadorAppTeca.getInstancia().listarTipoDocumento();
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public ArrayList<AsesorDTO> listarAsesor(AsesorDTO datos) {
        return MediadorAppTeca.getInstancia().listarAsesor(datos);
    }

    /**
     *
     * @param documento
     * @return
     */
    @RemoteMethod
    public boolean consultarAsesorPorDocumento(String documento) {
        return MediadorAppTeca.getInstancia().consultarAsesorPorDocumento(documento);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarAsesor(AsesorDTO datos) {
        return MediadorAppTeca.getInstancia().registrarAsesor(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean editarEstadoAsesor(AsesorDTO datos) {
        return MediadorAppTeca.getInstancia().editarEstadoAsesor(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public ArrayList<AsesorDTO> listarAsesorPorId(AsesorDTO datos) {
        return MediadorAppTeca.getInstancia().listarAsesorPorId(datos);
    }

    /**
     * @param anio
     * @return
     */
    @RemoteMethod
    public ArrayList<ListadoTarifasDTO> listarTarifas(String anio) {
        return MediadorAppTeca.getInstancia().listarTarifas(anio);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarTarifas(TarifaAseguradoraDTO datos) {
        return MediadorAppTeca.getInstancia().registrarTarifas(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean validarTarifa(TarifaAseguradoraDTO datos) {
        return MediadorAppTeca.getInstancia().validarTarifa(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean editarEntidad(EntidadDTO datos) {
        return MediadorAppTeca.getInstancia().editarEntidad(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean validarTarifaEditar(TarifaAseguradoraDTO datos) {
        return MediadorAppTeca.getInstancia().validarTarifaEditar(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean editarTarifa(TarifaAseguradoraDTO datos) {
        return MediadorAppTeca.getInstancia().editarTarifa(datos);
    }

    /**
     * @param datos
     * @return
     */
    @RemoteMethod
    public ArrayList<DetalleCotizacionDTO> reporteCotizacionesAsesor(DetalleCotizacionDTO datos) {
        return MediadorAppTeca.getInstancia().reporteCotizacionesAsesor(datos);
    }

    /**
     *
     * @param correo
     * @return
     */
    @RemoteMethod
    public DatosUsuarioDTO recuperarContrasenia(String correo) {
        return MediadorAppTeca.getInstancia().recuperarContrasenia(correo);
    }

    /**
     *
     * @param datosUsuario
     * @return
     */
    @RemoteMethod
    public boolean validarContrasenia(DatosUsuarioDTO datosUsuario) {
        return MediadorAppTeca.getInstancia().validarContrasenia(datosUsuario);
    }

    /**
     *
     * @param datosUsuario
     * @return
     */
    @RemoteMethod
    public boolean cambiarContrasenia(DatosUsuarioDTO datosUsuario) {
        return MediadorAppTeca.getInstancia().cambiarContrasenia(datosUsuario);

    }

    /**
     * @return
     */
    @RemoteMethod
    public ArrayList<TipoUsuarioDTO> listarTipoUsuario() {
        return MediadorAppTeca.getInstancia().listarTipoUsuario();
    }

    /**
     *
     * @param cedula
     * @return
     */
    @RemoteMethod
    public boolean validarExistenciaUsuarioCedula(String cedula) {
        return MediadorAppTeca.getInstancia().validarExistenciaUsuarioCedula(cedula);
    }

    /**
     *
     * @param usuario
     * @return
     */
    @RemoteMethod
    public boolean validarExistenciaUsuario(String usuario) {
        return MediadorAppTeca.getInstancia().validarExistenciaUsuario(usuario);
    }

    /**
     * @return
     */
    @RemoteMethod
    public ArrayList<UsuarioDTO> listarUsuarios() {
        return MediadorAppTeca.getInstancia().listarUsuarios();
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean editarEstadoUsuario(UsuarioDTO datos) {
        return MediadorAppTeca.getInstancia().editarEstadoUsuario(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarUsuario(UsuarioDTO datos) {
        return MediadorAppTeca.getInstancia().registrarUsuario(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean editarUsuario(UsuarioDTO datos) {
        return MediadorAppTeca.getInstancia().editarUsuario(datos);
    }

    /**
     *
     * @param cedula
     * @return
     */
    @RemoteMethod
    public AsesorDTO buscarAsesorPorCedula(String cedula) {
        return MediadorAppTeca.getInstancia().buscarAsesorPorCedula(cedula);
    }

    /**
     *
     * @param IdAsesor
     * @return
     */
    @RemoteMethod
    public ArrayList<AsesorDTO> buscarReferidosPorIdAsesor(String IdAsesor) {
        return MediadorAppTeca.getInstancia().buscarReferidosPorIdAsesor(IdAsesor);
    }

    /**
     *
     * @param idAsesorReferido
     * @return
     */
    @RemoteMethod
    public boolean desasociarReferido(String idAsesorReferido) {
        return MediadorAppTeca.getInstancia().desasociarReferido(idAsesorReferido);
    }

    /**
     *
     * @param idAsesor
     * @param idReferido
     * @return
     */
    @RemoteMethod
    public boolean validarReferido(String idAsesor, String idReferido) {
        return MediadorAppTeca.getInstancia().validarReferido(idAsesor, idReferido);
    }

    /**
     *
     * @param datosAsesor
     * @return
     */
    @RemoteMethod
    public boolean registrarReferido(AsesorDTO datosAsesor) {
        return MediadorAppTeca.getInstancia().registrarReferido(datosAsesor);
    }

    /**
     *
     * @param idAsesor
     * @return
     */
    @RemoteMethod
    public boolean asesorExisteComoReferido(String idAsesor) {
        return MediadorAppTeca.getInstancia().asesorExisteComoReferido(idAsesor);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<AsesorReferidoDTO> listarReferidos() {
        return MediadorAppTeca.getInstancia().listarReferidos();
    }

    /**
     *
     * @param listadoFasecolda
     * @return
     */
    @RemoteMethod
    public boolean registrarConfiguracionFasecolda(ArrayList<ArchivoFasecoldaDTO> listadoFasecolda) {
        return MediadorAppTeca.getInstancia().registrarConfiguracionFasecolda(listadoFasecolda);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public ArrayList<CotizacionDTO> listarPosiblesCotizaciones(CotizacionDTO datos) {
        return MediadorAppTeca.getInstancia().listarPosiblesCotizaciones(datos);
    }
}
