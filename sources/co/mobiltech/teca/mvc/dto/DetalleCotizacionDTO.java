/*
 * FachadaAppTeca.java
 *
 * Proyecto: Cotización de seguros
 * Cliente: Teca
 * Copyright 2017 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.dto;

import co.mobiltech.teca.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;

/**
 *
 * @author Ing. Hebert Medelo
 */
public class DetalleCotizacionDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String idCotizacion = Generales.EMPTYSTRING;
    String idTipoDocumento = Generales.EMPTYSTRING;
    String tipoDocumento = Generales.EMPTYSTRING;
    String documento = Generales.EMPTYSTRING;
    String nombre = Generales.EMPTYSTRING;
    String apellido = Generales.EMPTYSTRING;
    String fechaNacimiento = Generales.EMPTYSTRING;
    String telefono = Generales.EMPTYSTRING;
    String correo = Generales.EMPTYSTRING;
    String idVehiculo = Generales.EMPTYSTRING;
    String idmunicipio = Generales.EMPTYSTRING;
    String municipioNombre = Generales.EMPTYSTRING;
    String idDepartamento = Generales.EMPTYSTRING;
    String departamentoNombre = Generales.EMPTYSTRING;
    String idUbicacionVehiculo = Generales.EMPTYSTRING;
    String ubicacionVehiculo = Generales.EMPTYSTRING;
    String idTipoServicio = Generales.EMPTYSTRING;
    String tipoServicio = Generales.EMPTYSTRING;
    String idLineaVehiculo = Generales.EMPTYSTRING;
    String lineaVehiculo = Generales.EMPTYSTRING;
    String idMarca = Generales.EMPTYSTRING;
    String marca = Generales.EMPTYSTRING;
    String modelo = Generales.EMPTYSTRING;
    String placa = Generales.EMPTYSTRING;
    String idAseguradora = Generales.EMPTYSTRING;
    String aseguradora = Generales.EMPTYSTRING;
    String idTipoSeguro = Generales.EMPTYSTRING;
    String tipoSeguro = Generales.EMPTYSTRING;
    String tiempoDuracion = Generales.EMPTYSTRING;
    String valorTotalSeguro = Generales.EMPTYSTRING;
    String valorAsegurado = Generales.EMPTYSTRING;
    String valorReferencia = Generales.EMPTYSTRING;
    String antecedente = Generales.EMPTYSTRING;
    String cobertura = Generales.EMPTYSTRING;
    String infomacionImportante = Generales.EMPTYSTRING;
    String fechaCotizacion = Generales.EMPTYSTRING;
    String fechaCallCenter = Generales.EMPTYSTRING;
    String estadoCotizacion = Generales.EMPTYSTRING;
    String fechaInicial = Generales.EMPTYSTRING;
    String fechaFinal = Generales.EMPTYSTRING;
    String primaComercialAnual = Generales.EMPTYSTRING;
    String observacion = Generales.EMPTYSTRING;
    String nombreAsesor = Generales.EMPTYSTRING;
    String apellidoAsesor = Generales.EMPTYSTRING;
    String documentoAsesor = Generales.EMPTYSTRING;
    String porcentajeAuto = Generales.EMPTYSTRING;
    String porcentajeVida = Generales.EMPTYSTRING;
    String valorPorcentajeAuto = Generales.EMPTYSTRING;
    String valorPorcentajeVida = Generales.EMPTYSTRING;
    String observacionAsesor = Generales.EMPTYSTRING;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCotizacion() {
        return idCotizacion;
    }

    public void setIdCotizacion(String idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public String getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(String idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(String idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getIdmunicipio() {
        return idmunicipio;
    }

    public void setIdmunicipio(String idmunicipio) {
        this.idmunicipio = idmunicipio;
    }

    public String getMunicipioNombre() {
        return municipioNombre;
    }

    public void setMunicipioNombre(String municipioNombre) {
        this.municipioNombre = municipioNombre;
    }

    public String getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(String idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getDepartamentoNombre() {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre) {
        this.departamentoNombre = departamentoNombre;
    }

    public String getIdUbicacionVehiculo() {
        return idUbicacionVehiculo;
    }

    public void setIdUbicacionVehiculo(String idUbicacionVehiculo) {
        this.idUbicacionVehiculo = idUbicacionVehiculo;
    }

    public String getUbicacionVehiculo() {
        return ubicacionVehiculo;
    }

    public void setUbicacionVehiculo(String ubicacionVehiculo) {
        this.ubicacionVehiculo = ubicacionVehiculo;
    }

    public String getIdTipoServicio() {
        return idTipoServicio;
    }

    public void setIdTipoServicio(String idTipoServicio) {
        this.idTipoServicio = idTipoServicio;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public String getIdLineaVehiculo() {
        return idLineaVehiculo;
    }

    public void setIdLineaVehiculo(String idLineaVehiculo) {
        this.idLineaVehiculo = idLineaVehiculo;
    }

    public String getLineaVehiculo() {
        return lineaVehiculo;
    }

    public void setLineaVehiculo(String lineaVehiculo) {
        this.lineaVehiculo = lineaVehiculo;
    }

    public String getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(String idMarca) {
        this.idMarca = idMarca;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getIdAseguradora() {
        return idAseguradora;
    }

    public void setIdAseguradora(String idAseguradora) {
        this.idAseguradora = idAseguradora;
    }

    public String getAseguradora() {
        return aseguradora;
    }

    public void setAseguradora(String aseguradora) {
        this.aseguradora = aseguradora;
    }

    public String getIdTipoSeguro() {
        return idTipoSeguro;
    }

    public void setIdTipoSeguro(String idTipoSeguro) {
        this.idTipoSeguro = idTipoSeguro;
    }

    public String getTipoSeguro() {
        return tipoSeguro;
    }

    public void setTipoSeguro(String tipoSeguro) {
        this.tipoSeguro = tipoSeguro;
    }

    public String getTiempoDuracion() {
        return tiempoDuracion;
    }

    public void setTiempoDuracion(String tiempoDuracion) {
        this.tiempoDuracion = tiempoDuracion;
    }

    public String getValorTotalSeguro() {
        return valorTotalSeguro;
    }

    public void setValorTotalSeguro(String valorTotalSeguro) {
        this.valorTotalSeguro = valorTotalSeguro;
    }

    public String getValorAsegurado() {
        return valorAsegurado;
    }

    public void setValorAsegurado(String valorAsegurado) {
        this.valorAsegurado = valorAsegurado;
    }

    public String getValorReferencia() {
        return valorReferencia;
    }

    public void setValorReferencia(String valorReferencia) {
        this.valorReferencia = valorReferencia;
    }

    public String getAntecedente() {
        return antecedente;
    }

    public void setAntecedente(String antecedente) {
        this.antecedente = antecedente;
    }

    public String getCobertura() {
        return cobertura;
    }

    public void setCobertura(String cobertura) {
        this.cobertura = cobertura;
    }

    public String getInfomacionImportante() {
        return infomacionImportante;
    }

    public void setInfomacionImportante(String infomacionImportante) {
        this.infomacionImportante = infomacionImportante;
    }

    public String getFechaCotizacion() {
        return fechaCotizacion;
    }

    public void setFechaCotizacion(String fechaCotizacion) {
        this.fechaCotizacion = fechaCotizacion;
    }

    public String getFechaCallCenter() {
        return fechaCallCenter;
    }

    public void setFechaCallCenter(String fechaCallCenter) {
        this.fechaCallCenter = fechaCallCenter;
    }

    public String getEstadoCotizacion() {
        return estadoCotizacion;
    }

    public void setEstadoCotizacion(String estadoCotizacion) {
        this.estadoCotizacion = estadoCotizacion;
    }

    public String getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public String getPrimaComercialAnual() {
        return primaComercialAnual;
    }

    public void setPrimaComercialAnual(String primaComercialAnual) {
        this.primaComercialAnual = primaComercialAnual;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getNombreAsesor() {
        return nombreAsesor;
    }

    public void setNombreAsesor(String nombreAsesor) {
        this.nombreAsesor = nombreAsesor;
    }

    public String getApellidoAsesor() {
        return apellidoAsesor;
    }

    public void setApellidoAsesor(String apellidoAsesor) {
        this.apellidoAsesor = apellidoAsesor;
    }

    public String getDocumentoAsesor() {
        return documentoAsesor;
    }

    public void setDocumentoAsesor(String documentoAsesor) {
        this.documentoAsesor = documentoAsesor;
    }

    public String getPorcentajeAuto() {
        return porcentajeAuto;
    }

    public void setPorcentajeAuto(String porcentajeAuto) {
        this.porcentajeAuto = porcentajeAuto;
    }

    public String getPorcentajeVida() {
        return porcentajeVida;
    }

    public void setPorcentajeVida(String porcentajeVida) {
        this.porcentajeVida = porcentajeVida;
    }

    public String getValorPorcentajeAuto() {
        return valorPorcentajeAuto;
    }

    public void setValorPorcentajeAuto(String valorPorcentajeAuto) {
        this.valorPorcentajeAuto = valorPorcentajeAuto;
    }

    public String getValorPorcentajeVida() {
        return valorPorcentajeVida;
    }

    public void setValorPorcentajeVida(String valorPorcentajeVida) {
        this.valorPorcentajeVida = valorPorcentajeVida;
    }

    public String getObservacionAsesor() {
        return observacionAsesor;
    }

    public void setObservacionAsesor(String observacionAsesor) {
        this.observacionAsesor = observacionAsesor;
    }

    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
