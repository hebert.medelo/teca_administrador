 /*
 * MenuDTO.java
 *
 * Proyecto: Gestion de Creditos
 * Cliente: Promociones Empresariales
 * Copyright 2016 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.dto;

import co.mobiltech.teca.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Sys. E. Diego Armando Hernandez
 *
 */
public class MenuDTO implements Serializable {

    String id = Generales.EMPTYSTRING;    
    String tituloMenu = Generales.EMPTYSTRING;
    String iconoMenu = Generales.EMPTYSTRING;
    String fechaRegistro = Generales.EMPTYSTRING;
    ArrayList<FuncionalidadDTO> funcionalidad = new ArrayList<>();    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTituloMenu() {
        return tituloMenu;
    }

    public void setTituloMenu(String tituloMenu) {
        this.tituloMenu = tituloMenu;
    }

    public String getIconoMenu() {
        return iconoMenu;
    }

    public void setIconoMenu(String iconoMenu) {
        this.iconoMenu = iconoMenu;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public ArrayList<FuncionalidadDTO> getFuncionalidad() {
        return funcionalidad;
    }

    public void setFuncionalidad(ArrayList<FuncionalidadDTO> funcionalidad) {
        this.funcionalidad = funcionalidad;
    }
    
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
