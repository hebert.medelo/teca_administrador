/*
 * FachadaAppTeca.java
 *
 * Proyecto: Cotización de seguros
 * Cliente: Teca
 * Copyright 2017 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.dto;

import co.mobiltech.teca.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ing. Hebert Medelo
 */
public class PrioridadParametrizacionDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    List<AseguradoraDTO> listadoAseguradoras = new ArrayList<>();
    String aseguradora = Generales.EMPTYSTRING;
    String valorDetalle = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;
    String orden = Generales.EMPTYSTRING;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<AseguradoraDTO> getListadoAseguradoras() {
        return listadoAseguradoras;
    }

    public void setListadoAseguradoras(List<AseguradoraDTO> listadoAseguradoras) {
        this.listadoAseguradoras = listadoAseguradoras;
    }

    public String getAseguradora() {
        return aseguradora;
    }

    public void setAseguradora(String aseguradora) {
        this.aseguradora = aseguradora;
    }

    public String getValorDetalle() {
        return valorDetalle;
    }

    public void setValorDetalle(String valorDetalle) {
        this.valorDetalle = valorDetalle;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }
    
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
