/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.mvc.dto;

import co.mobiltech.teca.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class ListadoTarifasDTO implements Serializable {

    String idAseguradora = Generales.EMPTYSTRING;
    String nombreAseguradora = Generales.EMPTYSTRING;
    ArrayList<TarifaAseguradoraDTO> tarifasAseguradoras = new ArrayList<>();    

    public String getIdAseguradora() {
        return idAseguradora;
    }

    public void setIdAseguradora(String idAseguradora) {
        this.idAseguradora = idAseguradora;
    }

    public String getNombreAseguradora() {
        return nombreAseguradora;
    }

    public void setNombreAseguradora(String nombreAseguradora) {
        this.nombreAseguradora = nombreAseguradora;
    }

    public ArrayList<TarifaAseguradoraDTO> getTarifasAseguradoras() {
        return tarifasAseguradoras;
    }

    public void setTarifasAseguradoras(ArrayList<TarifaAseguradoraDTO> tarifasAseguradoras) {
        this.tarifasAseguradoras = tarifasAseguradoras;
    }
    
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
