/*
 * FachadaAppTeca.java
 *
 * Proyecto: Cotización de seguros
 * Cliente: Teca
 * Copyright 2017 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.dto;

import co.mobiltech.teca.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;

/**
 *
 * @author Ing. Hebert Medelo
 */
public class CotizacionDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String concecutivo = Generales.EMPTYSTRING;
    String fecha = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;
    String codigoFasecolda = Generales.EMPTYSTRING;
    String idAseguradora = Generales.EMPTYSTRING;
    String nombraAseguradora = Generales.EMPTYSTRING;
    String fechaInicial = Generales.EMPTYSTRING;
    String fechaFinal = Generales.EMPTYSTRING;
    String placa = Generales.EMPTYSTRING;
    String cedula = Generales.EMPTYSTRING;
    String nombre = Generales.EMPTYSTRING;
    String apellido = Generales.EMPTYSTRING;
    String idTipoSeguro = Generales.EMPTYSTRING;
    String valorAsegurado = Generales.EMPTYSTRING;
    String idTipoDocumento = Generales.EMPTYSTRING;
    String tipoDocumento = Generales.EMPTYSTRING;
    String telefono = Generales.EMPTYSTRING;
    String correo = Generales.EMPTYSTRING;
    String fechaNacimiento = Generales.EMPTYSTRING;
    String idUbicacionVehiculo = Generales.EMPTYSTRING;
    String ubicacionVehiculo = Generales.EMPTYSTRING;
    String departamento = Generales.EMPTYSTRING;
    String ciudad = Generales.EMPTYSTRING;
    String tipoServicio = Generales.EMPTYSTRING;
    String modelo = Generales.EMPTYSTRING;
    String marca = Generales.EMPTYSTRING;
    String linea = Generales.EMPTYSTRING;
    String tipoSeguro = Generales.EMPTYSTRING;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConcecutivo() {
        return concecutivo;
    }

    public void setConcecutivo(String concecutivo) {
        this.concecutivo = concecutivo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoFasecolda() {
        return codigoFasecolda;
    }

    public void setCodigoFasecolda(String codigoFasecolda) {
        this.codigoFasecolda = codigoFasecolda;
    }

    public String getIdAseguradora() {
        return idAseguradora;
    }

    public void setIdAseguradora(String idAseguradora) {
        this.idAseguradora = idAseguradora;
    }

    public String getNombraAseguradora() {
        return nombraAseguradora;
    }

    public void setNombraAseguradora(String nombraAseguradora) {
        this.nombraAseguradora = nombraAseguradora;
    }

    public String getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getIdTipoSeguro() {
        return idTipoSeguro;
    }

    public void setIdTipoSeguro(String idTipoSeguro) {
        this.idTipoSeguro = idTipoSeguro;
    }

    public String getValorAsegurado() {
        return valorAsegurado;
    }

    public void setValorAsegurado(String valorAsegurado) {
        this.valorAsegurado = valorAsegurado;
    }

    public String getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(String idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getIdUbicacionVehiculo() {
        return idUbicacionVehiculo;
    }

    public void setIdUbicacionVehiculo(String idUbicacionVehiculo) {
        this.idUbicacionVehiculo = idUbicacionVehiculo;
    }

    public String getUbicacionVehiculo() {
        return ubicacionVehiculo;
    }

    public void setUbicacionVehiculo(String ubicacionVehiculo) {
        this.ubicacionVehiculo = ubicacionVehiculo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getTipoSeguro() {
        return tipoSeguro;
    }

    public void setTipoSeguro(String tipoSeguro) {
        this.tipoSeguro = tipoSeguro;
    }

    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
