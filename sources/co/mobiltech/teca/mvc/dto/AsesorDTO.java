/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.mvc.dto;

import co.mobiltech.teca.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;

/**
 *
 * @author JHONATAN
 */
public class AsesorDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String nombre = Generales.EMPTYSTRING;
    String apellido = Generales.EMPTYSTRING;
    String documento = Generales.EMPTYSTRING;
    String correo = Generales.EMPTYSTRING;
    String porcentajeAuto = Generales.EMPTYSTRING;
    String porcentajeVida = Generales.EMPTYSTRING;
    String idEntidad = Generales.EMPTYSTRING;
    String descripcionEntidad = Generales.EMPTYSTRING;
    String idTipoDocumento = Generales.EMPTYSTRING;
    String descripcionTipoDocumento = Generales.EMPTYSTRING;
    String fechaRegistro = Generales.EMPTYSTRING;
    String registradoPor = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;
    String porcentajeReferido = Generales.EMPTYSTRING;
    String idAsesorReferido = Generales.EMPTYSTRING;

    public AsesorDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPorcentajeAuto() {
        return porcentajeAuto;
    }

    public void setPorcentajeAuto(String porcentajeAuto) {
        this.porcentajeAuto = porcentajeAuto;
    }

    public String getPorcentajeVida() {
        return porcentajeVida;
    }

    public void setPorcentajeVida(String porcentajeVida) {
        this.porcentajeVida = porcentajeVida;
    }

    public String getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(String idEntidad) {
        this.idEntidad = idEntidad;
    }

    public String getDescripcionEntidad() {
        return descripcionEntidad;
    }

    public void setDescripcionEntidad(String descripcionEntidad) {
        this.descripcionEntidad = descripcionEntidad;
    }

    public String getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(String idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getDescripcionTipoDocumento() {
        return descripcionTipoDocumento;
    }

    public void setDescripcionTipoDocumento(String descripcionTipoDocumento) {
        this.descripcionTipoDocumento = descripcionTipoDocumento;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getRegistradoPor() {
        return registradoPor;
    }

    public void setRegistradoPor(String registradoPor) {
        this.registradoPor = registradoPor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPorcentajeReferido() {
        return porcentajeReferido;
    }

    public void setPorcentajeReferido(String porcentajeReferido) {
        this.porcentajeReferido = porcentajeReferido;
    }

    public String getIdAsesorReferido() {
        return idAsesorReferido;
    }

    public void setIdAsesorReferido(String idAsesorReferido) {
        this.idAsesorReferido = idAsesorReferido;
    }

    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
