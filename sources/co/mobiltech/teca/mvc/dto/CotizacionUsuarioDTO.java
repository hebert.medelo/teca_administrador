/*
 * FachadaAppTeca.java
 *
 * Proyecto: Cotización de seguros
 * Cliente: Teca
 * Copyright 2017 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.mvc.dto;

import co.mobiltech.teca.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;

/**
 *
 * @author Ing. Hebert Medelo
 */
public class CotizacionUsuarioDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String idCotizacion = Generales.EMPTYSTRING;
    String idUsuario = Generales.EMPTYSTRING;
    String observacion = Generales.EMPTYSTRING;
    String fecha = Generales.EMPTYSTRING;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCotizacion() {
        return idCotizacion;
    }

    public void setIdCotizacion(String idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
