/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.servlet;

import co.mobiltech.teca.common.connection.ContextDataResourceNames;
import co.mobiltech.teca.common.connection.DataBaseConnection;
import co.mobiltech.teca.common.util.Generales;
import co.mobiltech.teca.common.util.ValidaString;
import static co.mobiltech.teca.common.util.ValidaString.isNumeric;
import co.mobiltech.teca.mvc.dto.ArchivoFasecoldaDTO;
import co.mobiltech.teca.mvc.dto.DatosUsuarioDTO;
import co.mobiltech.teca.mvc.fachada.FachadaAppTeca;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author JHONATAN
 */
@WebServlet(name = "UploadExcel", urlPatterns = {"/UploadExcel"})
public class UploadExcel extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathToWeb = Generales.EMPTYSTRING;
        String nameFile = Generales.EMPTYSTRING;
        ArrayList listadoConsignacion = new ArrayList();
        ArrayList<ArchivoFasecoldaDTO> listadoConsignacion2 = new ArrayList();
        ArrayList ArrayErrores = new ArrayList();
        ArchivoFasecoldaDTO datosConsignacion = null;
        String error = "";
        String stringCellValue = "";
        boolean registro = false;
        DataBaseConnection dbcon = null;
        Connection conexion = null;
        HttpSession servletSesion = null;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);
            servletSesion = request.getSession(false);
            DatosUsuarioDTO usuarioLogueado = (DatosUsuarioDTO) servletSesion.getAttribute("datosUsuario");

            FileItemFactory itemfactory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(itemfactory);
            List<FileItem> items = upload.parseRequest(request);

            if (System.getProperty("os.name").toLowerCase().startsWith("window")) {
                pathToWeb = (String) new InitialContext().lookup("java:comp/env/ruta_archivo_windows");
            } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
                pathToWeb = (String) new InitialContext().lookup("java:comp/env/ruta_archivo_linux");
            }

            for (FileItem item : items) {
                if (item.getName() != null) {
                    if (item.getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                        File uploadDir = new File(pathToWeb);
                        nameFile = UUID.randomUUID().toString() + ".xlsx";
                        File file = new File(uploadDir, nameFile);
                        item.write(file);
                    } else {
                        System.out.println("solo se reciben archivos .xlsx");
                        error = "Tipo de archivo no permitido";
                        ArrayErrores.add(error);
                    }
                }
            }

            File uploadDir = new File(pathToWeb);
            File file = new File(uploadDir, nameFile);
            List cellDataList = new ArrayList();
            FileInputStream fileInputStream = new FileInputStream(file);
            XSSFWorkbook workBook = new XSSFWorkbook(fileInputStream);
            XSSFSheet hssfSheet = workBook.getSheetAt(0);
            Iterator rowIterator = hssfSheet.rowIterator();
            while (rowIterator.hasNext()) {
                XSSFRow hssfRow = (XSSFRow) rowIterator.next();
                Iterator iterator = hssfRow.cellIterator();
                List cellTempList = new ArrayList();
                while (iterator.hasNext()) {
                    XSSFCell hssfCell = (XSSFCell) iterator.next();
                    cellTempList.add(hssfCell);
                }
                cellDataList.add(cellTempList);
            }
            System.out.println("cellDataList.size()" + cellDataList.size());
            if (cellDataList.size() > 1) {
                for (int i = 0; i < cellDataList.size(); i++) {
                    List cellTempList = (List) cellDataList.get(i);
                    datosConsignacion = new ArchivoFasecoldaDTO();
                    for (int j = 0; j < cellTempList.size(); j++) {
                        XSSFCell hssfCell = (XSSFCell) cellTempList.get(j);
                        stringCellValue = hssfCell.toString();
                        if (i >= 1) {
                            if (j >= 0) {
                                if (!ValidaString.isNullOrEmptyString(stringCellValue)) {
                                    if (j == 0) {
                                        datosConsignacion.setClase(stringCellValue);
                                    } else if (j == 1) {
                                        datosConsignacion.setMarca(stringCellValue);
                                    } else if (j == 2) {
                                        datosConsignacion.setLinea(stringCellValue);
                                    } else if (j == 3) {
                                        datosConsignacion.setVersion(stringCellValue);
                                    } else if (j == 4) {
                                        datosConsignacion.setCodigoFasecolda(stringCellValue);
                                        if (isNumeric(stringCellValue)) {
                                            if (listadoConsignacion.contains(datosConsignacion.getCodigoFasecolda())) {
                                                error = "El archivo contiene un codigo fasecolda repetido en la celda D" + (i + 1) + " \n";
                                                System.out.println("hay error de codigo duplicado");
                                                ArrayErrores.add(error);
                                            } else {
                                                listadoConsignacion.add(datosConsignacion.getCodigoFasecolda());
                                                System.out.println("no está");
                                            }
                                        } else {
                                            System.out.println("*/*/*/*/*/*/*/*/*/*/*/*/ " + stringCellValue);
                                            error = "Codigo fasecolda no valido en la línea ::: Linea " + (i + 1) + "\n";
                                            ArrayErrores.add(error);
                                        }
                                    }
                                    datosConsignacion.setRegistradoPor(usuarioLogueado.getUsuario());
                                } else {
                                    System.out.println("*/*/*/*/*/*/*/*/*/*/*/*/ " + stringCellValue);
                                    error = "Linea o parametro vacio ::: Linea " + (i + 1) + "\n";
                                    ArrayErrores.add(error);
                                }
                            }
                        }
                    }
                    if (i >= 1) {
                        listadoConsignacion2.add(datosConsignacion);
                    }
                }
            } else {
                error = "El archivo no tine contenido\n";
                ArrayErrores.add(error);
            }

            System.out.println("ArrayErrores tamaño" + ArrayErrores.size());
            if (ArrayErrores.size() == 0) {
                if (!new FachadaAppTeca().registrarConfiguracionFasecolda(listadoConsignacion2)) {
                    throw new Exception("Error al realizar el registro.");
                }
                registro = true;
                response.getWriter().print(registro);
            } else {
                for (int r = 0; r < ArrayErrores.size(); r++) {
                    response.getWriter().print(ArrayErrores.get(r));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            response.getWriter().print(e.getMessage());
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
