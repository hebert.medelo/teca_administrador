/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.servlet;

import co.mobiltech.teca.common.connection.ContextDataResourceNames;
import co.mobiltech.teca.common.connection.DataBaseConnection;
import co.mobiltech.teca.common.util.Generales;
import co.mobiltech.teca.common.util.ValidaString;
import co.mobiltech.teca.mvc.dao.ArchivoTarifasDAO;
import co.mobiltech.teca.mvc.dao.TarifaAseguradoraDAO;
import co.mobiltech.teca.mvc.dto.ArchivoTarifasDTO;
import co.mobiltech.teca.mvc.dto.AseguradoraDTO;
import co.mobiltech.teca.mvc.dto.DatosUsuarioDTO;
import co.mobiltech.teca.mvc.dto.TarifaAseguradoraDTO;
import co.mobiltech.teca.mvc.mediador.MediadorAppTeca;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "ServletArchivoTarifas", urlPatterns = {"/ServletArchivoTarifas"})
public class ServletArchivoTarifas extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

        String pathToWeb = Generales.EMPTYSTRING;
        String nameFile = Generales.EMPTYSTRING;
        ArrayList<AseguradoraDTO> aseguradoras = null;
        TarifaAseguradoraDTO tarifas = null;
        ArrayList<TarifaAseguradoraDTO> listadoTarifas = new ArrayList();
        ArrayList ArrayErrores = new ArrayList();
        String error = "";
        boolean registro = false;
        DataBaseConnection dbcon = null;
        Connection conexion = null;
        HttpSession servletSesion = null;

        Calendar c1 = Calendar.getInstance();
        int anioActual = c1.get(Calendar.YEAR);

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_TECA_JDBC);
            servletSesion = request.getSession(false);
            DatosUsuarioDTO usuarioLogueado = (DatosUsuarioDTO) servletSesion.getAttribute("datosUsuario");

            FileItemFactory itemfactory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(itemfactory);
            List<FileItem> items = upload.parseRequest(request);

            if (System.getProperty("os.name").toLowerCase().startsWith("window")) {
                pathToWeb = (String) new InitialContext().lookup("java:comp/env/ruta_archivo_windows");
            } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
                pathToWeb = (String) new InitialContext().lookup("java:comp/env/ruta_archivo_linux");
            }

            for (FileItem item : items) {
                if (item.getName() != null) {
                    if (item.getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                        File uploadDir = new File(pathToWeb);
                        nameFile = UUID.randomUUID().toString() + ".xlsx";
                        File file = new File(uploadDir, nameFile);
                        item.write(file);
                        System.out.println("subido con exito");
                    } else {
                        System.out.println((char) 27 + "[32;43m solo se reciben archivos .xlsx");
                        error = "Tipo de archivo no permitido";
                        ArrayErrores.add(error);
                    }
                }
            }

            aseguradoras = MediadorAppTeca.getInstancia().listarAseguradoras();

            File uploadDir = new File(pathToWeb);
            File file = new File(uploadDir, nameFile);
            List cellDataList = new ArrayList();
            FileInputStream fileInputStream = new FileInputStream(file);
            XSSFWorkbook workBook = new XSSFWorkbook(fileInputStream);
            XSSFSheet hssfSheet = workBook.getSheetAt(0);
            Iterator rowIterator = hssfSheet.rowIterator();
            while (rowIterator.hasNext()) {
                XSSFRow hssfRow = (XSSFRow) rowIterator.next();
                Iterator iterator = hssfRow.cellIterator();
                List cellTempList = new ArrayList();
                while (iterator.hasNext()) {
                    XSSFCell hssfCell = (XSSFCell) iterator.next();
                    cellTempList.add(hssfCell);
                }
                cellDataList.add(cellTempList);
            }
            if (cellDataList.size() > 1) {
                for (int i = 0; i < cellDataList.size(); i++) {
                    List cellTempList1 = (List) cellDataList.get(i);
                    tarifas = new TarifaAseguradoraDTO();

                    for (int j = 0; j < cellTempList1.size(); j++) {
                        XSSFCell hssfCell = (XSSFCell) cellTempList1.get(j);
                        String stringCellValue = hssfCell.toString();

                        if (i > 0) {
                            if (!ValidaString.isNullOrEmptyString(stringCellValue)) {                               
                                String a = stringCellValue.substring(0, stringCellValue.lastIndexOf("."));
                                if (j == 0) {
                                    for (int l = 0; l < aseguradoras.size(); l++) {
                                        if (a.equals(aseguradoras.get(l).getId())) {
                                            tarifas.setIdAseguradora(a);
                                            break;
                                        } else {
                                            if (l == aseguradoras.size() - 1) {
                                                error = "Aseguradora no exite ::: Linea " + (i + 1) + "\n";
                                                ArrayErrores.add(error);
                                            }
                                        }
                                    }
                                }
                                if (j == 1) {
                                    if (Integer.parseInt(a) >= anioActual && Integer.parseInt(a) <= 9999) {
                                        tarifas.setVigencia(a);
                                    } else {
                                        error = "Vigencia incorrecta ::: Linea " + (i + 1) + "\n";
                                        ArrayErrores.add(error);
                                    }
                                }
                                if (j == 2) {
                                    if (Integer.parseInt(a) >= 18 && Integer.parseInt(a) <= 79) {
                                        tarifas.setEdad(a);
                                    } else {
                                        error = "Edad incorrecta ::: Linea " + (i + 1) + "\n";
                                        ArrayErrores.add(error);
                                    }
                                }
                                if (j == 3) {
                                    tarifas.setIpt(stringCellValue);
                                }
                            } else {
                                 System.out.println("*/*/*/*/*/*/*/*/*/*/*/*/ " + stringCellValue);
                                error = "Linea o parametro vacio ::: Linea " + (i + 1) + "\n";
                                ArrayErrores.add(error);
                            }
                        }
                    }
                    if (i > 0) {
                        tarifas.setRegistradoPor(usuarioLogueado.getUsuario());
                        listadoTarifas.add(tarifas);
                    }
                }
            } else {
                error = "El archivo no tine contenido\n";
                ArrayErrores.add(error);
            }

            for (int y = 0; y < listadoTarifas.size(); y++) {
                System.out.println("*****************************************" + listadoTarifas.get(y).toStringJson());
                if (new TarifaAseguradoraDAO().validarTarifa(conexion, listadoTarifas.get(y))) {
                    error = "La tarifa ya existe ::: linea " + (y + 2) + "\n";
                    ArrayErrores.add(error);
                }
            }

            if (ArrayErrores.size() == 0) {
                ArchivoTarifasDTO archivo = new ArchivoTarifasDTO();
                archivo.setNombre(nameFile);
                archivo.setRegistradoPor(usuarioLogueado.getUsuario());
                if (!new ArchivoTarifasDAO().registrarArchivo(conexion, archivo)) {
                    throw new Exception("Error al realizar el registro.");
                }

                for (int y = 0; y < listadoTarifas.size(); y++) {
                    listadoTarifas.get(y).setIdArchivo(archivo.getId());
                    if (!new TarifaAseguradoraDAO().registrarTarifas(conexion, listadoTarifas.get(y))) {
                        throw new Exception("Error al realizar el registro.");
                    }
                }
                registro = true;
                response.getWriter().print(registro);
            } else {
                for (int r = 0; r < ArrayErrores.size(); r++) {
                    response.getWriter().print(ArrayErrores.get(r));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            response.getWriter().print(e.getMessage());
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
