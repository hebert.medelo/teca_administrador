 /*
 * ContextDataResourceNames.java
 *
 * Proyecto: Doctor Pin
 * Cliente: 
 * Copyright 2016 by Mobiltech SAS 
 * All rights reserved
 */
package co.mobiltech.teca.common.connection;

/**
 *
 * @author Sys. E. Diego Armando Hernandez
 */
public class ContextDataResourceNames {

    public final static String MYSQL_TECA_JDBC = "jdbc/teca";
}
