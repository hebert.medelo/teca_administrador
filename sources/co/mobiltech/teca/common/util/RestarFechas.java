/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.teca.common.util;

import java.text.DateFormat;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class RestarFechas {

    public static String RestarFechas(String fechaInicio, Date date) {

        String respuesta = Generales.EMPTYSTRING;
        String fechaActual = Generales.EMPTYSTRING;

        try {

            DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
            fechaActual = df.format(date);
            String[] aFechaIng = fechaInicio.split("/");
            Integer diaInicio = Integer.parseInt(aFechaIng[0]);
            Integer mesInicio = Integer.parseInt(aFechaIng[1]);
            Integer anioInicio = Integer.parseInt(aFechaIng[2]);

            String[] aFecha = fechaActual.split("/");
            Integer diaActual = Integer.parseInt(aFecha[0]);
            Integer mesActual = Integer.parseInt(aFecha[1]);
            Integer anioActual = Integer.parseInt(aFecha[2]);

            int dias = 0;
            int anios = 0;
            int meses = 0;
            int b = 0;
            int mes = 0;

            mes = mesInicio - 1;
            if (mes == 2) {
                if ((anioActual % 4 == 0) && ((anioActual % 100 != 0) || (anioActual % 400 == 0))) {
                    b = 29;
                } else {
                    b = 28;
                }
            } else if (mes <= 7) {
                if (mes == 0) {
                    b = 31;
                } else if (mes % 2 == 0) {
                    b = 30;
                } else {
                    b = 31;
                }
            } else if (mes > 7) {
                if (mes % 2 == 0) {
                    b = 31;
                } else {
                    b = 30;
                }
            }
            if (mesInicio <= mesActual) {
                anios = anioActual - anioInicio;
                if (diaInicio <= diaActual) {
                    meses = mesActual - mesInicio;
                    dias = (diaActual - diaInicio);
                } else {
                    meses = (mesActual - mesInicio - 1 + 12) % 12;
                    dias = b - (diaInicio - diaActual);
                }
            } else {
                anios = anioActual - anioInicio - 1;

                if (diaInicio > diaActual) {
                    meses = mesActual - mesInicio - 1 + 12;
                    dias = b - (diaInicio - diaActual);
                } else {
                    meses = mesActual - mesInicio + 12;
                    dias = diaActual - diaInicio;
                }
            }
            //respuesta = Integer.toString(meses) + " Meses";
            //respuesta = Integer.toString(dias) + " Dias";
            respuesta = Integer.toString(anios);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return respuesta;
    }
}
