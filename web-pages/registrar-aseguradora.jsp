<%-- 
    Document   : registrar-aseguradora
    Created on : 15/09/2017, 03:21:25 PM
    Author     : Ing. Hebert Medelo
--%>
<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Gestionar Aseguradoras</h3>
        <form role="form" id="registerForm" action="return:false" autocomplete="off">  
            <div id="divMensajeOk" class="form-group has-success" style="display: none;">
                <div id="mensajeOk" class="form-control1">-</div>
            </div>
            <div id="divMensajeError" class="form-group has-error" style="display: none;">
                <div id="mensajeError" class="form-control1">-</div>
            </div>
            <table class="table table-hover table-striped" id="tablaAseguradoras">
                <thead>
                    <tr>
                        <th>Logo</th>
                        <th>Nombre</th>
                        <th>Orden</th>  
                        <th>Editar Orden</th> 
                    </tr>
                </thead>                    
                <tbody id="listado">
                </tbody>
            </table>  
            <div class="row" hidden id="divForm">
                <div class="col-md-12">                  
                    <div class="form-group col-md-6">                        
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" placeholder="Nombre" maxlength="50">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="logo">Logo</label>
                        <input type="file" id="logo" name="logo" placeholder="Logo" class="form-control">
                        <p class="help-block">Solo permite imagenes .svg .jpg .png.</p>
                    </div>                                       
                </div>           
            </div>
            <div class="col-sm-offset-9"> 
                <button type="button" class="btn btn-default" id="btnVolver" onclick="javascript:verVolver()">Volver</button>
                <button type="button" class="btn btn-primary" id="btnAgergar" onclick="javascript:verForm()">Agregar</button>
                <button type="button" class="btn btn-primary" id="btnRegistrar" onclick="javascript:registrar()">Registrar</button>
                <button type="button" class="btn btn-primary" id="btnEditar" onclick="javascript:registrarEditar()">Guardar</button>
            </div>
        </form>
    </div>   
</div>
<script>
    jQuery(document).ready(function () {
        listarAseguradoras();
        $("#btnVolver").hide();
        $("#btnRegistrar").hide();
        $("#btnEditar").hide();
    });

    function verForm() {
        $("#divForm").show();
        $("#tablaAseguradoras").hide();
        $("#btnVolver").show();
        $("#btnRegistrar").show();
        $("#btnAgergar").hide();
    }

    function verVolver() {
        $("#nombre").val("");
        $("#nombre").val("");
        $("#divForm").hide();
        $("#tablaAseguradoras").show();
        $("#btnVolver").hide();
        $("#btnRegistrar").hide();
        $("#btnAgergar").show();
        $("#btnEditar").hide();
    }

    function registrar() {
        if ($('#nombre').val() === '') {
            mostrarMensaje('error', 'Ingrese nombre.');
            $('#nombre').focus();
            return;
        }

        if ($('#logo').val() === '') {
            mostrarMensaje('error', 'A�n no ha seleccionado una imagen.');
            $('#logo').focus();
            return;
        }

        if ($('#logo')[0].files[0].type === "image/jpeg" || $('#logo')[0].files[0].type === "image/png" || $('#logo')[0].files[0].type === "image/svg+xml") {

            var datos = {
                nombre: $("#nombre").val(),
                logo: "." + $('#logo')[0].files[0].name.split('.').pop()
            };
            ajaxTeca.registrarAseguradora(datos, {
                callback: function (data) {
                    if (data) {
                        montarImg();
                    } else {
                        mostrarMensaje('error', 'No se pudo realizar el registro.');
                    }
                },
                timeout: 20000
            });
        } else {
            mostrarMensaje('error', 'El tipo de archivo que intenta agregar no esta permitido.');
        }
    }

    function  montarImg() {
        var data;
        data = new FormData();
        data.append('file', $('#logo')[0].files[0]);
        jQuery.ajax({
            url: 'Uploader?nombre',
            data: data,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (r) {
                if (r = !"true") {
                    mostrarMensaje('error', 'No se pudo subir el logo.');
                } else {
                    $("#nombre").val("");
                    $("#nombre").val("");
                    listarAseguradoras();
                    mostrarMensaje('ok', 'Se realiz� el registro con �xito.');
                }
            }
        });
    }

    function listarAseguradoras() {

        ajaxTeca.listarAseguradoras({
            callback: function (data) {
                if (data != null) {
                    verVolver();
                    listado = data;
                    dwr.util.removeAllRows("listado");
                    dwr.util.addRows("listado", data, mapa, {
                        escapeHtml: false
                    });
                } else {
                    mostrarMensaje('error', 'No se encontraron aseguradores registradas.');
                    $("#tablaCotizaciones").hide();
                }
            },
            timeout: 20000
        });
    }

    function obtenerId(r) {
        $("#divForm").show();
        $("#tablaAseguradoras").hide();
        $("#btnVolver").show();
        $("#btnRegistrar").hide();
        $("#btnAgergar").hide();
        $("#btnEditar").show();

        for (var i = 0; i < listado.length; i++) {
            if (listado[i].id == r) {
                $('#nombre').val(listado[i].nombre);
                id = r;
            }
        }
    }
    var id = "";
    var listado = [];
    var mapa = [
        function (data) {
            return '<td><img src="<%= request.getContextPath()%>/imagenPerfil?imagen=' + data.logo + '" style="width:100px; height:55px; cursor: pointer;"> </td>';
        },
        function (data) {
            return data.nombre;
        },
        function (data) {
            return data.orden;
        },
        function (data) {
            return "<td><button class='label label-success' onclick='obtenerId(" + data.id + ");'>Editar</button></td>";
        }
    ];

    function  editar() {

        var data;
        data = new FormData();
        data.append('file', $('#logo')[0].files[0]);
        jQuery.ajax({
            url: 'Uploader?nombre',
            data: data,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (r) {
                if (r == "true") {
                    mostrarMensaje('ok', 'Se realizo el registro con �xito');
                    $("#nombre").val("");
                    $("#logo").val("");
                    listarAseguradoras();
                } else {
                    mostrarMensaje('error', 'No se pudo editar el registro de la imagen.');
                }
            }
        });
    }

    function registrarEditar() {

        var logo = null;
        if ($('#nombre').val() === '') {
            mostrarMensaje('error', 'Ingrese nombre.');
            $('#nombre').focus();
            return;
        }
        if ($('#logo').val() !== '') {
            if ($('#logo')[0].files[0].type === "image/jpeg" || $('#logo')[0].files[0].type === "image/png" || $('#logo')[0].files[0].type === "image/svg+xml") {
                logo = "." + $('#logo')[0].files[0].name.split('.').pop();
                var datos = {
                    nombre: $("#nombre").val(),
                    logo: logo,
                    id: id
                };
                ajaxTeca.editarAseguradora(datos, {
                    callback: function (data) {
                        if (data) {
                            editar();
                        } else {
                            mostrarMensaje('error', 'No se pudo realizar el registro.');
                        }
                    },
                    timeout: 20000
                });
            } else {
                mostrarMensaje('error', 'El tipo de archivo que intenta agregar no esta permitido.');
                return;
            }
        } else {
            var datos = {
                nombre: $("#nombre").val(),
                id: id
            };
            ajaxTeca.editarAseguradora(datos, {
                callback: function (data) {
                    if (data) {
                        mostrarMensaje('ok', 'Se realizo el registro con �xito');
                        $("#nombre").val("");
                        $("#logo").val("");
                        listarAseguradoras();
                    } else {
                        mostrarMensaje('error', 'No se pudo realizar el registro.');
                    }
                },
                timeout: 20000
            });
        }
    }
</script>