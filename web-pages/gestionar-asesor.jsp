<%-- 
    Document   : gestionar-asesor
    Created on : 21/12/2017, 02:41:44 PM
    Author     : JHONATAN
--%>


<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Gestionar Asesor</h3>
        <div id="divMensajeOk" class="form-group has-success" style="display: none;">
            <div id="mensajeOk" class="form-control1">-</div>
        </div>
        <div id="divMensajeError" class="form-group has-error" style="display: none;">
            <div id="mensajeError" class="form-control1">-</div>
        </div>

        <div class="row" id="filtrosBusqueda">
            <div class="col-md-12">                  
                <div class="form-group col-md-6" >                        
                    <label for="nombreFiltro">Nombre</label>
                    <input type="text" class="form-control" id="nombreFiltro" name="nombreFiltro" onkeypress="return soloLetras(event);" placeholder="Nombre" maxlength="50" >
                </div>
                <div class="form-group col-md-6" >
                    <label for="documentoFiltro">N�mero documento</label>
                    <input type="text" class="form-control" id="documentoFiltro" name="documentoFiltro" placeholder="Documento" maxlength="15">
                </div>
            </div>
            <div class="col-sm-offset-9">     
                <button type="button" class="btn btn-primary" id="botonBuscar" onclick="javascript:buscarAsesor()">Buscar</button>
            </div>
        </div>
        <br>
        <div class="row" id="botonAgregar" hidden>
            <div class="col-sm-offset-9">     
                <button type="button" class="btn btn-primary" onclick="javascript:agregarAsesor()">Agregar</button>
            </div>
        </div>
        <table class="table table-hover table-striped" hidden id="tablaAsesor">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>No. doc.</th>
                    <th>Nombre</th> 
                    <th>Apellido</th> 
                    <th>Entidad</th>
                    <th>Fecha de creaci�n</th>
                    <th>Estado</th>
                </tr>
            </thead>                    
            <tbody id="listado">
            </tbody>
        </table>
        <form role="form" id="registerForm" action="return:false" autocomplete="off">
            <div class="row" id="divForm" hidden>
                <div class="col-md-12">
                    <div class="form-group col-md-4">
                        <label for="selectTipoDocumento">Tipo de documento</label>
                        <select class="form-control" title="Seleccione tip. de doc." id="selectTipoDocumento" name="selectTipoDocumento" required></select>
                    </div>
                    <div class="form-group col-md-4">                        
                        <label for="numeroDocumento">N�mero de documento</label>
                        <input type="text" class="form-control" title="Seleccione documento" id="numeroDocumento" name="numeroDocumento" maxlength="15" onkeypress="return soloNumeros(event);" placeholder="Documento" required >
                    </div>
                    <div class="form-group col-md-4">                        
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" title="Seleccione nombre" id="nombre" name="nombre" maxlength="50" placeholder="Nombre" onkeypress="return soloLetras(event);" required >
                    </div>
                </div> 
                <div class="col-md-12">
                    <div class="form-group col-md-4">                        
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" title="Seleccione apellido" id="apellido" name="apellido" maxlength="50" placeholder="Apellido" onkeypress="return soloLetras(event);" required >
                    </div>
                    <div class="form-group col-md-4">                        
                        <label for="correo">Correo</label>
                        <input type="email" class="form-control" title="Seleccione correo" id="correo" name="correo" maxlength="100" placeholder="Correo" required >
                    </div>
                    <div class="form-group col-md-4">
                        <label for="selectEntidad">Entidad</label>
                        <select class="form-control" title="Seleccione entidad" id="selectEntidad" name="selectEntidad" required></select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group col-md-4">                        
                        <label for="porcentajeAuto">% de Auto</label>
                        <input type="text" class="form-control" title="Seleccione % de auto" id="porcentajeAuto" name="porcentajeAuto" maxlength="10" onkeypress="return soloNumeros(event);" placeholder="Porcentaje Auto" required >
                    </div>
                    <div class="form-group col-md-4">                        
                        <label for="porcentajeVida">% de Vida</label>
                        <input type="text" class="form-control" title="Seleccione % de vida" id="porcentajeVida" name="porcentajeVida" maxlength="10" onkeypress="return soloNumeros(event);" placeholder="Porcentaje Vida" required >
                    </div>
                </div>
                <br>
                <div class="col-sm-offset-9"> 
                    <button type="submit" class="btn btn-primary" id="btnRegistrar" onclick="javascript:validar('registerForm', 1);">Registrar</button>
                </div>
            </div>
        </form>

    </div>   
</div>
<script>
    jQuery(document).ready(function () {
        buscarAsesor();
    });

    function listarTipoDocumento(idCombo, valorSeleccionando) {
        ajaxTeca.listarTipoDocumento({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione tipo de documento'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    $('#' + idCombo).val(valorSeleccionando);
                }
            },
            timeout: 20000
        });
    }

    function listarEntidad(idCombo, valorSeleccionando) {
        ajaxTeca.listarEntidades({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            nombre: 'Seleccione entidad'
                        }], 'id', 'nombre');
                    dwr.util.addOptions(idCombo, data, 'id', 'nombre');
                    $('#' + idCombo).val(valorSeleccionando);
                }
            },
            timeout: 20000
        });
    }

    var listado = [];
    var mapa = [
        function (data) {
            return data.id;
        },
        function (data) {
            return data.documento;
        },
        function (data) {
            return data.nombre;
        },
        function (data) {
            return data.apellido;
        },
        function (data) {
            return data.descripcionEntidad;
        },
        function (data) {
            return data.fechaRegistro;
        },
        function (data) {
            if (data.estado == 1) {
                return "<td><button class='label label-success' onclick='cambiarEstadoInactivo(" + data.id + ");'>Activo</button></td>";
            } else {
                return "<td><button class='label label-error' onclick='cambiarEstadoActivo(" + data.id + ");'>Inactivo</button></td>";
            }
        }
    ];

    function buscarAsesor() {
        var datos = {
            documento: $("#documentoFiltro").val(),
            nombre: $("#nombreFiltro").val()
        };
        ajaxTeca.listarAsesor(datos, {
            callback: function (data) {
                if (data != null) {
                    $("#tablaAsesor").show();
                    dwr.util.removeAllRows("listado");
                    dwr.util.addRows("listado", data, mapa, {
                        escapeHtml: false
                    });
                } else {
                    $("#tablaAsesor").hide();
                    $("#botonAgregar").show();
                    mostrarMensaje('error', 'No se encontraron resultados.');
                }
            },
            timeout: 20000
        });
    }

    function agregarAsesor() {
        listarEntidad("selectEntidad");
        listarTipoDocumento("selectTipoDocumento");
        $("#botonAgregar").hide();
        $("#tablaAsesor").hide();
        $("#filtrosBusqueda").hide();
        $("#divForm").show();
    }

    function registrar() {
        var documento = $('#numeroDocumento').val();
        var datos = {
            idTipoDocumento: $("#selectTipoDocumento").val(),
            documento: $("#numeroDocumento").val(),
            nombre: $("#nombre").val(),
            apellido: $("#apellido").val(),
            correo: $("#correo").val(),
            idEntidad: $("#selectEntidad").val(),
            porcentajeAuto: $("#porcentajeAuto").val(),
            porcentajeVida: $("#porcentajeVida").val()
        };
        ajaxTeca.consultarAsesorPorDocumento(documento, {
            callback: function (data) {
                if (data) {
                    mostrarMensaje('error', 'Ya existe un asesor con el mismo n�mero de identificaci�n, no se puede registrar.');
                } else {
                    ajaxTeca.registrarAsesor(datos, {
                        callback: function (data) {
                            if (data) {
                                $("#divForm").hide();
                                $("#filtrosBusqueda").show();
                                $("#nombre").val("");
                                $("#selectTipoDocumento").val("");
                                $("#numeroDocumento").val("");
                                $("#apellido").val("");
                                $("#correo").val("");
                                $("#selectEntidad").val("");
                                $("#porcentajeVida").val("");
                                $("#porcentajeAuto").val("");
                                $("#nombreFiltro").val("");
                                $("#documentoFiltro").val("");
                                buscarAsesor();
                                $("#tablaAsesor").show();
                                mostrarMensaje('ok', 'Se registr� con exito.');
                            } else {
                                mostrarMensaje('error', 'No se pudo realizar el registro.');
                            }
                        },
                        timeout: 20000
                    });
                }
            },
            timeout: 20000
        });
    }

    function soloNumeros(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = "0,1,2,3,4,5,6,7,8,9,.";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }

    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " �����abcdefghijklmn�opqrstuvwxyz";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }

    function validar(form, Id) {
        nextFunction = Id;
        $(".form-group").removeClass("has-error");
        $('.error').remove();
        $('input[type="text"]').each(function () {
            $(this).val($(this).val().trim());
        });
        $('textarea').each(function () {
            $(this).val($(this).val().trim());
        });
        $('#' + form).validate({
            highlight: function (label) {
                jQuery(label).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (label) {
                jQuery(label).closest('.form-group').removeClass('has-error');
                label.remove();
            }, errorPlacement: function (error, element) {
                var placement = element.closest('.input-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            },
            submitHandler: function () {
                ejecutarPostValidate();
            }
        });
    }

    function ejecutarPostValidate() {
        switch (nextFunction) {
            case 1:
                registrar();
                break;
        }
        nextFunction = null;
    }

    function cambiarEstadoInactivo(idAsesor) {
        var datos = {
            id: idAsesor,
            estado: 0
        };
        ajaxTeca.editarEstadoAsesor(datos, {
            callback: function (data) {
                if (data) {
                    buscarAsesor();
                    mostrarMensaje('ok', 'Cambio de estado exitoso.');
                } else {
                    mostrarMensaje('error', 'Problema en cambio de estado.');
                }
            },
            timeout: 20000
        });
    }

    function cambiarEstadoActivo(idAsesor) {
        var datos = {
            id: idAsesor,
            estado: 1
        };
        ajaxTeca.editarEstadoAsesor(datos, {
            callback: function (data) {
                if (data) {
                    buscarAsesor();
                    mostrarMensaje('ok', 'Cambio de estado exitoso.');
                } else {
                    mostrarMensaje('error', 'Problema en cambio de estado.');
                }
            },
            timeout: 20000
        });
    }

    function listarAsesorPorId(datos) {
        ajaxTeca.listarAsesorPorId(datos, {
            callback: function (data) {
                if (data != null) {
                    $("#tablaAsesor").show();
                    dwr.util.removeAllRows("listado");
                    dwr.util.addRows("listado", data, mapa, {
                        escapeHtml: false
                    });
                } else {
                    $("#tablaAsesor").hide();
                }
            },
            timeout: 20000
        });
    }

</script>
