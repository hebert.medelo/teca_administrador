<%-- 
    Document   : paramatrizacion
    Created on : 19/09/2017, 09:35:38 AM
    Author     : Ing. Hebert Medelo
--%>
<%@page import="co.mobiltech.teca.mvc.constantes.PrioridadParametrizacion"%>
<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Prioridad de Parametrización</h3>
        <form class="form-horizontal">
            <div class="form-group">
                <label for="checkbox" class="col-sm-6 control-label">Prioridad de Parametrización: </label>
                <div class="col-sm-6">
                    <div class="radio-inline"><label><input onclick="javascript:listarAseguradoras()" type="radio" name="options" id="PrioridadAse" value="1">Aseguradora</label></div>
                    <div class="radio-inline"><label><input onclick="javascript:valorDetalle();" type="radio" name="options" id="PrioridadValor" value="2">Valor</label></div>
                </div>
            </div>
        </form>
        <div id="divMensajeOk" class="form-group has-success" style="display: none;">
            <div id="mensajeOk" class="form-control1">-</div>
        </div>
        <div id="divMensajeError" class="form-group has-error" style="display: none;">
            <div id="mensajeError" class="form-control1">-</div>
        </div>
        <table class="table table-hover table-striped" id="tablaAseguradoras" hidden>
            <thead>
                <tr>
                    <th>Logo</th>
                    <th>Nombre</th>
                    <th>Orden</th>  
                    <th>Editar Orden</th> 
                </tr>
            </thead>                    
            <tbody id="listado">
            </tbody>
        </table> 
        <form class="">
            <div class="form-group col-sm-12" id="divFormaOrdenar" hidden>
                <label for="formaOrdenar">Forma de ordenar</label>             
                <select id="formaOrdenar" name="formaOrdenar" class="form-control"  title="Seleccione una opcion">
                    <option>Seleccione una opción</option>
                    <option value="<%=PrioridadParametrizacion.ASCENDENTE%>">Ascendente</option>
                    <option value="<%=PrioridadParametrizacion.DESCENDENTE%>">Descendente</option>
                </select>
            </div> 
        </form>
        <div class="col-sm-offset-8" id=""> 
            <button type="button" class="btn btn-primary" id="divBtn" onclick="javascript:actualizarPrioridadParametrizacion()">Registrar</button>                     
        </div>
    </div>  
</div>
</div>


<script>
    jQuery(document).ready(function () {
        $("#divBtn").hide();
    });

    var id;
    var listado = [];
    var mapa = [
        function (data) {
            return '<td><img src="<%= request.getContextPath()%>/imagenPerfil?imagen=' + data.logo + '" style="width:100px; height:55px; cursor: pointer;"> </td>';
        },
        function (data) {
            return data.nombre;
        },
        function (data) {
            return data.orden;
        },
        function (data) {
            return "<td><a data-toggle='modal' href='#myModal' data-target='#myModal' class='label label-success' onclick='obtenerId(" + data.id + ");'>Editar Orden</a></td>";
        }
    ];

    function obtenerId(t) {
         $("#iptOrden").val("");
        id = t;
        for (var i = 0; i < listado.length; i++) {
            if (listado[i].id == t) {
                $("#lblAseguradora").val(listado[i].nombre);
            }
        }
    }

    function editarOrden() {

        var m = $("#iptOrden").val();
         if ($("#iptOrden").val() == "") {
            alert("Debe digitar un valor.");
            return;
        }
        
        if (m > listado.length) {
            alert("El orden asignado no puede ser mayor a la cantidad de aseguradoras registradas. Cantidad Aseguradoras = " + listado.length);
            return;
        }
        if (m < 1) {
            alert("El orden asignado no puede ser menor que 1. ");
            return;
        }

        var orden2;
        for (var i = 0; i < listado.length; i++) {
            if (listado[i].id == id) {
                orden2 = listado[i].orden;
                listado[i].orden = m;
            }
        }
        var cont = parseInt(m) - 1;
        if (parseInt(orden2) > parseInt(m)) {
            var x = parseInt(m);
            for (cont; cont < listado.length; cont++) {
                if (listado[cont].id != id) {
                    x++;
                    listado[cont].orden = x;
                }
            }
        }

        if (parseInt(orden2) < parseInt(m)) {
            for (var r = parseInt(orden2); r < listado.length; r++) {
                if (parseInt(listado[r].orden) > parseInt(m)) {
                    break;
                }
                if (listado[r].id != id) {
                    var x = parseInt(listado[r].orden);
                    listado[r].orden = x - 1;
                }
            }
        }
        listado.sort(function (a, b) {
            return (a.orden - b.orden);
        });

        dwr.util.removeAllRows("listado");
        dwr.util.addRows("listado", listado, mapa, {
            escapeHtml: false
        });
        $("#iptOrden").val("");
        $("#cerrarModal").click();
    }

    function listarAseguradoras() {

        ajaxTeca.listarAseguradoras({
            callback: function (data) {
                if (data != null) {
                    $("#tablaAseguradoras").show();
                    $("#divFormaOrdenar").hide();
                    $("#divBtn").show();
                    listado = data;
                    dwr.util.removeAllRows("listado");
                    dwr.util.addRows("listado", data, mapa, {
                        escapeHtml: false
                    });
                } else {
                    $("#tablaCotizaciones").hide();
                    alert("No se encontraron resultados.");
                }
            },
            timeout: 20000
        });
    }

    function valorDetalle() {
        $("#tablaAseguradoras").hide();
        $("#divBtn").show();
        $("#divFormaOrdenar").show();
    }



    function actualizarPrioridadParametrizacion() {

        if ($("#PrioridadAse").is(':checked')) {
            var datos = {
                aseguradora: <%=PrioridadParametrizacion.ESTADO_ACTIVO%>,
                valorDetalle: <%=PrioridadParametrizacion.ESTADO_INACTIVO%>,
                listadoAseguradoras: listado
            };
        }

        if ($("#PrioridadValor").is(':checked')) {
            var datos = {
                aseguradora: <%=PrioridadParametrizacion.ESTADO_INACTIVO%>,
                valorDetalle: <%=PrioridadParametrizacion.ESTADO_ACTIVO%>,
                orden: $("#formaOrdenar").val()
            };
        }

        ajaxTeca.actualizarPrioridadParametrizacion(datos, {
            callback: function (data) {
                if (data) {
                    alert("Se registro con éxito.");
                } else {
                    $("#tablaCotizaciones").hide();
                    alert("No se encontraron resultados.");
                }
            },
            timeout: 20000
        });
    }
    function validarUsuarios(e) {

        key = e.keyCode || e.which;
        if (e.keyCode === 44) {
            return false;
        }
        tecla = String.fromCharCode(key).toLowerCase();
        letras = "1,2,3,4,5,6,7,8,9";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }

</script>

<div   id="myModal" class="modal fade" tabindex="-1" role="form" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Asignar orden</h4>
            </div>
            <form role="form" action="return:false" id="formModalAddAtencion" autocomplete="off">
                <div class="modal-body"> 

                    <div class="row">
                        <div class="col-md-12">                           
                            <div class="form-group col-md-6">
                                <label for="lblAseguradora" class="control-label">Aseguradora</label>                               
                                <input type="text" class="form-control" name="lblAseguradora" id="lblAseguradora" placeholder="Aseguradora" disabled>                                                       
                            </div>
                            <div class="form-group col-md-6">
                                <label for="iptOrden" class="control-label">Orden</label>
                                <input type="text" class="form-control"  name="iptOrden" id="iptOrden" placeholder="Nuevo orden" title="Digite oreden." onkeypress="return validarUsuarios(event);">                                                       
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" onclick="javascript:editarOrden();">Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrarModal">Cancelar</button>
                </div>
            </form>      
        </div>
    </div>
</div>
