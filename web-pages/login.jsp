<%-- 
    Document   : login
    Created on : 15/09/2017, 09:19:15 AM
    Author     : Administrator
--%>
<%@ page import="javax.servlet.*"%>
<%@ page import="co.mobiltech.teca.common.util.*"%>
<%@ page import="java.util.*,java.io.*"  contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%

    session.invalidate();

    String mensajeError = request.getParameter("error");

    boolean mostrarError = false;

    if (!ValidaString.isNullOrEmptyString(mensajeError)) {
        mostrarError = true;
    }
%>

<!DOCTYPE HTML>
<html>
    <head>
        <title>Teca | Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <link href="css/font-awesome.css" rel="stylesheet"> 
        <script src="js/jquery.min.js"> </script>
        <script src="js/bootstrap.min.js"> </script>
    </head>
    <body>
        <div class="login">
            <h1><a href="login.jsp">TECA</a></h1>
            <div class="login-bottom">
                <h2>Ingresar</h2>
                <form role="form" action="<%= request.getContextPath()%>/j_security_check" method="post" autocomplete="off">
                    <%if (mostrarError) {%>                           
                    <p>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><%= mensajeError%></strong>
                    </div>
                    </p>
                    <% }%>
                    <div class="col-md-6">
                        <div class="login-mail">
                            <input type="text" name="j_username" placeholder="User" required="">
                            <i class="fa fa-user"></i>
                        </div>
                        <div class="login-mail">
                            <input type="password" name="j_password" placeholder="Password" required="">
                            <i class="fa fa-lock"></i>
                        </div>                       
                    </div>
                    <div class="col-md-6 login-do">
                        <label class="hvr-shutter-in-horizontal login-sub">
                            <input type="submit" value="Ingresar">
                        </label>
                        <p>Recuperar contraseña</p>
                        <a href="recuperar-contrasenia.jsp" class="hvr-shutter-in-horizontal" >Recuperar</a>
                    </div>
                    <div class="clearfix"> </div>
                </form>
            </div>
        </div>
        <!---->
        <div class="copy-right">
            <p> &copy; 2016 Minimal. All Rights Reserved | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>	    </div>  
        <!---->
        <!--scrolling js-->
        <script src="js/jquery.nicescroll.js"></script>
        <script src="js/scripts.js"></script>
        <!--//scrolling js-->
    </body>
</html>

