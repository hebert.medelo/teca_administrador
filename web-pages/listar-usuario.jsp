<%-- 
    Document   : listar-usuario
    Created on : 5/02/2018, 10:26:08 AM
    Author     : JHONATAN
--%>

<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Listado de usuarios</h3>
        <div id="divMensajeOk" class="form-group has-success" style="display: none;">
            <div id="mensajeOk" class="form-control1">-</div>
        </div>
        <div id="divMensajeError" class="has-error" style="display: none;">
            <div id="mensajeError" class="form-control1">-</div>
        </div>
        <div id="listadoUsuarios">
            <div class="row">
                <div class="table-responsive col-md-12" id="divListaUsuarios">
                    <table class="table" id="tablaUsuarios">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Rol</th>
                                <th>Correo</th>                           
                                <th>Usuario</th>
                                <th>Estado</th>
                                <th>Editar</th>
                            </tr>
                        </thead>                    
                        <tbody id="listado">
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>

        <form role="form" id="registerForm" action="return:false" autocomplete="off" hidden>
            <div class="row" id="divForm">
                <div class="col-md-12">
                    <div class="form-group col-md-4">                        
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" title="Ingrese nombre" id="nombre" name="nombre" maxlength="25" placeholder="Nombre" onkeypress="return soloLetras(event);" required >
                    </div>
                    <div class="form-group col-md-4">                        
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" title="Ingrese apellido" id="apellido" name="apellido" maxlength="25" placeholder="Apellido" onkeypress="return soloLetras(event);" required >
                    </div>
                    <div class="form-group col-md-4">
                        <label for="selectRol">Rol</label>
                        <select class="form-control" title="Seleccione rol" id="selectRol" name="selectRol" required></select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group col-md-4">                        
                        <label for="correo">Correo</label>
                        <input type="email" class="form-control" title="Ingrese correo" id="correo" name="correo" maxlength="50" placeholder="Correo" required >
                    </div>
                    <div class="form-group col-md-4">                        
                        <label for="contrasenia">Contrase�a</label>
                        <input type="password" class="form-control" title="Ingrese contrase�a, m�nimo 6 caracteres" id="contrasenia" name="contrasenia" maxlength="15" placeholder="Contrase�a" required autocomplete="off" minlength="6">
                    </div>
                </div>
                <br>
                <div class="col-sm-offset-9"> 
                    <button type="submit" class="btn btn-primary" id="btnRegistrar" onclick="javascript:validar('registerForm', 1);">Editar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    jQuery(document).ready(function () {
        listarUsuarios();
        listarTipoUsuario("selectRol");
    });

    function listarTipoUsuario(idCombo, valorSeleccionando) {
        ajaxTeca.listarTipoUsuario({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione rol'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    $('#' + idCombo).val(valorSeleccionando);
                }
            },
            timeout: 20000
        });
    }

    function listarUsuarios() {
        ajaxTeca.listarUsuarios({
            callback: function (data) {
                if (data != null) {
                    //$("#divListaUsuarios").show();
                    dwr.util.removeAllRows("listado");
                    listado = data;
                    dwr.util.addRows("listado", data, mapa, {
                        escapeHtml: false
                    });
                } else {
                    //$("#divListaUsuarios").hide();
                    mostrarMensaje('error', 'No se encontraron resultados.');
                }
            },
            timeout: 20000
        });
    }

    var listado = [];
    var mapa = [
        function (data) {
            return data.nombre;
        },
        function (data) {
            return data.apellido;
        },
        function (data) {
            return data.tipoUsuario;
        },
        function (data) {
            return data.correo;
        },
        function (data) {
            return data.usuario;
        },
        function (data) {
            if (data.estado == 1) {
                return "<td><button class='label label-success' onclick='cambiarEstadoInactivo(" + data.idUsuario + ");'>Activo</button></td>";
            } else {
                return "<td><button class='label label-error' onclick='cambiarEstadoActivo(" + data.idUsuario + ");'>Inactivo</button></td>";
            }
        },
        function (data) {
            return "<td><button class='label label-success' onclick='cambiarFormlularioEditar(" + data.idUsuario + ");'>Editar</button></td>";
        }
    ];

    function cambiarEstadoInactivo(idUsuario) {
        for (var i = 0; i < listado.length; i++) {
            if (parseInt(listado[i].idUsuario) == idUsuario) {
                if (listado[i].tipoUsuario === "administrador") {
                    mostrarMensaje('error', 'El usuario tipo administrador no permite cambiar de estado.');
                    return;
                }
            }
        }
        console.log("idUsuario" + idUsuario);
        var datos = {
            idUsuario: idUsuario,
            estado: 0
        };
        ajaxTeca.editarEstadoUsuario(datos, {
            callback: function (data) {
                if (data) {
                    listarUsuarios();
                    mostrarMensaje('ok', 'Cambio de estado exitoso.');
                } else {
                    mostrarMensaje('error', 'Problema en cambio de estado.');
                }
            },
            timeout: 20000
        });
    }

    function cambiarEstadoActivo(idUsuario) {
        console.log("idUsuario" + idUsuario);
        var datos = {
            idUsuario: idUsuario,
            estado: 1
        };
        ajaxTeca.editarEstadoUsuario(datos, {
            callback: function (data) {
                if (data) {
                    listarUsuarios();
                    mostrarMensaje('ok', 'Cambio de estado exitoso.');
                } else {
                    mostrarMensaje('error', 'Problema en cambio de estado.');
                }
            },
            timeout: 20000
        });
    }

    function validar(form) {
        $('#' + form).validate({
            highlight: function (label) {
                jQuery(label).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (label) {
                jQuery(label).closest('.form-group').removeClass('has-error');
                label.remove();
            }, errorPlacement: function (error, element) {
                var placement = element.closest('.input-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            },
            submitHandler: function () {
                editarUsuario();
            }
        });
    }

    function editarUsuario() {
        var datos = {
            idUsuario: idUsuario,
            idTipoUsuario: $("#selectRol").val(),
            nombre: $("#nombre").val(),
            apellido: $("#apellido").val(),
            correo: $("#correo").val(),
            clave: $("#contrasenia").val()
        };
        ajaxTeca.editarUsuario(datos, {
            callback: function (data) {
                if (data) {
                    mostrarMensaje('ok', 'se edit� correctamente.');
                    limpiarFormulario('registerForm');
                    listarUsuarios();
                    $("#divListaUsuarios").show();
                    $("#registerForm").hide();
                } else {
                    mostrarMensaje('error', 'se present� un problema al registrar.');
                }
            }, timeout: 20000
        });
    }

    function soloNumeros(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = "0,1,2,3,4,5,6,7,8,9,.";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }

    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " �����abcdefghijklmn�opqrstuvwxyz";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }

    function limpiarFormulario(form) {
        $('#' + form + ' input[type="text"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="number"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="email"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="date"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' select').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="password"]').each(function () {
            $(this).val("");
        });
        $("input:radio").prop("checked", false);
    }

    var idUsuario = null;

    function cambiarFormlularioEditar(id) {
        idUsuario = id;

        $("#divListaUsuarios").hide();
        $("#registerForm").show();
        for (var i = 0; i < listado.length; i++) {
            if (listado[i].idUsuario == idUsuario) {
                $("#selectRol").val(listado[i].idTipoUsuario);
                $("#nombre").val(listado[i].nombre);
                $("#apellido").val(listado[i].apellido);
                $("#correo").val(listado[i].correo);
            }
        }
    }

</script>

