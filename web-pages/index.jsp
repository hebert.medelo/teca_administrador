<%-- 
    Document   : index
    Created on : 15/09/2017, 03:28:48 PM
    Author     : Administrator
--%>

<%@page import="co.mobiltech.teca.mvc.dto.MenuDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="co.mobiltech.teca.mvc.dto.DatosUsuarioDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    DatosUsuarioDTO datosUsuario = (DatosUsuarioDTO) session.getAttribute("datosUsuario");
    ArrayList<MenuDTO> menu = datosUsuario.getMenu();
%>
<!DOCTYPE html>

<html>
    <head>
        <title>Teca | Index</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <link href="css/font-awesome.css" rel="stylesheet"> 
        <script src="js/jquery.min.js"></script>
        <!-- Mainly scripts -->
        <script src="js/jquery.metisMenu.js"></script>
        <script src="js/jquery.slimscroll.min.js"></script>
        <!-- Custom and plugin javascript -->
        <link href="css/custom.css" rel="stylesheet">
        <script src="js/custom.js"></script>
        <script src="js/screenfull.js"></script>    

        <script src="js/pie-chart.js" type="text/javascript"></script>

        <script src="js/skycons.js"></script>
        <!--//skycons-icons-->
        <style>
            label.error {
                color: #a94442 ;
                font-size: 0.8em;
                margin-top: -2px;
                padding: 0;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!----->
            <nav class="navbar-default navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h1> <a class="navbar-brand" href="index.jsp">TECA</a></h1>         
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="drop-men" >
                    <ul class=" nav_1">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown"><span class=" name-caret"><%=datosUsuario.getNombre()%><i class="caret"></i></span><img src="images/wo.jpg"></a>
                            <ul class="dropdown-menu " role="menu">                                    
                                <li><a href="login.jsp"><i class="fa fa-clipboard"></i>Salir</a></li>
                                <li><a href="javascript:cargarPagina('cambiar-contrasenia.jsp');"><i class="fa fa-clipboard"></i>Cambiar contraseña</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
                <div class="clearfix">
                </div>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <%for (int i = 0; i < menu.size(); i++) {%>
                            <li>
                                <a href="#" class=" hvr-bounce-to-right">
                                    <i class="<%=menu.get(i).getIconoMenu()%>"></i> 
                                    <span class="nav-label"><%=menu.get(i).getTituloMenu()%></span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <% for (int j = 0; j < menu.get(i).getFuncionalidad().size(); j++) {%>
                                    <li>
                                        <a href="javascript:cargarPagina('<%=menu.get(i).getFuncionalidad().get(j).getPagina()%>', '<%=menu.get(i).getFuncionalidad().get(j).getTitulo()%>');" class=" hvr-bounce-to-right">
                                            <i class="fa fa-area-chart nav_icon"></i>
                                            <%=menu.get(i).getFuncionalidad().get(j).getTitulo()%>
                                        </a>
                                    </li>
                                    <% } %>
                                </ul>
                            </li>
                            <% }%>
                        </ul>
                    </div>
                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="content-main">
                    <!--banner-->	
                    <div id="contenido1">

                    </div>
                    <br>
                    <div class="copy">
                        <p> &copy; 2018 Mobiltech.</p>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!---->
        <!--scrolling js-->
        <script src="js/jquery.nicescroll.js"></script>
        <script src="js/scripts.js"></script>
        <!--//scrolling js-->
        <script src="js/bootstrap.min.js"></script>
        <script type='text/javascript' src="<%= request.getContextPath()%>/dwr/interface/ajaxTeca.js"></script>
        <script type='text/javascript' src="<%= request.getContextPath()%>/dwr/interface/ajaxSeguridad.js"></script>
        <script type='text/javascript' src="<%= request.getContextPath()%>/dwr/engine.js"></script>
        <script type='text/javascript' src="<%= request.getContextPath()%>/dwr/util.js"></script>
        <script src="js/jquery.table2excel.js"></script>
        <script src="js/jquery.calendario.js"></script>
        <script src="js/jquery.alerts.js"></script>
        <script src="js/jquery.validate.min.js"></script>


        <script>

            dwr.util.useLoadingMessage = function (message) {
                dwr.engine.setPreHook(function () {
                    //jLoading('Estamos procesando su solititud, espere un momento por favor.', '-');
                    jLoading('', '');
                });
                dwr.engine.setPostHook(function () {
                    ocultarMensajes();
                });
            };




            function mostrarMensaje(tipo, mensaje) {

                if (tipo == 'ok') {
                    jQuery('#mensajeOk').html(mensaje);
                    jQuery('#divMensajeOk').show('fast');
                    jQuery("#divMensajeOk").delay(3000).fadeOut('slow');
                }
                if (tipo == 'error') {
                    jQuery('#mensajeError').html(mensaje);
                    jQuery('#divMensajeError').show('fast');
                    jQuery("#divMensajeError").delay(3000).fadeOut('slow');
                }

            }

            function ocultarMensajes() {

                jQuery("#divMensajeOk").hide('slow');
                jQuery("#divMensajeError").hide('slow');
                jQuery("#popup_container").trigger('click');

            }

            function cargarPagina(pagina) {
                $('#contenido1').load(pagina);

            }

            jQuery(document).ready(function () {

                dwr.util.useLoadingMessage();

                cargarPagina("reporte-cotizaciones.jsp");

            });

            dwr.engine.setTextHtmlHandler(function () {

                alert("Su sesión ha expirado por favor ingrese de nuevo");
                setTimeout("location.reload()", "1000");
            });


        </script>
    </body>
</html>

