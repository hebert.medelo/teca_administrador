<%-- 
    Document   : index_ctr
    Created on : 27/10/2016, 04:08:38 PM
    Author     : Administrator
--%>

<%@page import="co.mobiltech.teca.mvc.constantes.Usuario"%>
<%@ page import="javax.servlet.*"%>
<%@ page import="java.util.*,java.io.*"%>

<%@ page import="co.mobiltech.teca.mvc.dto.*"%>
<%@ page import="co.mobiltech.teca.common.util.*"%>

<jsp:useBean type="co.mobiltech.teca.mvc.fachada.FachadaSeguridad" scope="application" id="fachadaSeguridad"/>

<%
    DatosUsuarioDTO datosUsuario = null;
    
    try {
        
        if (!ValidaString.isNullOrEmptyString(request.getRemoteUser())) {
            
            datosUsuario = fachadaSeguridad.consultarDatosUsuarioLogueado(request.getRemoteUser());            
            
            if (datosUsuario != null && datosUsuario.getEstado().equals(Usuario.ESTADO_ACTIVO)) {
                
                session.setAttribute("datosUsuario", datosUsuario);
                
                request.getRequestDispatcher("/index.jsp").forward(request, response);
                
            } else {
                request.getRequestDispatcher("/login.jsp?error=Ingreso no autorizado").forward(request, response);
            }
        } else {
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
    } catch (Exception e) {
        request.getRequestDispatcher("/login.jsp").forward(request, response);
    }
%>   