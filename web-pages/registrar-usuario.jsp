<%-- 
    Document   : registrar-usuario
    Created on : 5/02/2018, 10:25:46 AM
    Author     : JHONATAN
--%>

<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Registro de usuarios</h3>
        <div id="divMensajeOk" class="form-group has-success" style="display: none;">
            <div id="mensajeOk" class="form-control1">-</div>
        </div>
        <div id="divMensajeError" class="has-error" style="display: none;">
            <div id="mensajeError" class="form-control1">-</div>
        </div>
        <form role="form" id="registerForm" action="return:false" autocomplete="off">

            <div class="row" id="divForm">
                <div class="col-md-12">
                    <div class="form-group col-md-4">                        
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" title="Ingrese nombre" id="nombre" name="nombre" maxlength="25" placeholder="Nombre" onkeypress="return soloLetras(event);" required >
                    </div>
                    <div class="form-group col-md-4">                        
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" title="Ingrese apellido" id="apellido" name="apellido" maxlength="25" placeholder="Apellido" onkeypress="return soloLetras(event);" required >
                    </div>
                    <div class="form-group col-md-4">                        
                        <label for="numeroDocumento">N�mero de documento</label>
                        <input type="text" class="form-control" title="Ingrese documento" id="numeroDocumento" name="numeroDocumento" maxlength="11" onkeypress="return soloNumeros(event);" placeholder="Documento" required >
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group col-md-6">
                        <label for="selectRol">Rol</label>
                        <select class="form-control" title="Seleccione rol" id="selectRol" name="selectRol" required></select>
                    </div>
                    <div class="form-group col-md-6">                        
                        <label for="correo">Correo</label>
                        <input type="email" class="form-control" title="Ingrese correo" id="correo" name="correo" maxlength="50" placeholder="Correo" required >
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="form-group col-md-6">                        
                        <label for="usuario">Usuario</label>
                        <input type="text" class="form-control" title="ingrese usuario" id="usuario" name="usuario" maxlength="25" placeholder="Usuario" required autocomplete="off">
                    </div>
                    <div class="form-group col-md-6">                        
                        <label for="contrasenia">Contrase�a</label>
                        <input type="password" class="form-control" title="Ingrese contrase�a, m�nimo 6 caracteres" id="contrasenia" name="contrasenia" maxlength="15" placeholder="Contrase�a" required autocomplete="off" minlength="6">
                    </div>
                </div>
                <br>
                <div class="col-sm-offset-9"> 
                    <button type="submit" class="btn btn-primary" id="btnRegistrar" onclick="javascript:validar('registerForm', 1);">Registrar</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    jQuery(document).ready(function () {
        listarTipoUsuario("selectRol");
    });

    function listarTipoUsuario(idCombo, valorSeleccionando) {
        ajaxTeca.listarTipoUsuario({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            descripcion: 'Seleccione rol'
                        }], 'id', 'descripcion');
                    dwr.util.addOptions(idCombo, data, 'id', 'descripcion');
                    $('#' + idCombo).val(valorSeleccionando);
                }
            },
            timeout: 20000
        });
    }

    function validar(form) {
        $('#' + form).validate({
            highlight: function (label) {
                jQuery(label).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (label) {
                jQuery(label).closest('.form-group').removeClass('has-error');
                label.remove();
            }, errorPlacement: function (error, element) {
                var placement = element.closest('.input-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            },
            submitHandler: function () {
                registrarUsuario();
            }
        });
    }

    function registrarUsuario() {
        var cedula = $("#numeroDocumento").val();
        var usuario = $("#usuario").val();
        var datos = {
            idTipoUsuario: $("#selectRol").val(),
            cedula: $("#numeroDocumento").val(),
            nombre: $("#nombre").val(),
            apellido: $("#apellido").val(),
            correo: $("#correo").val(),
            usuario: $("#usuario").val(),
            clave: $("#contrasenia").val()
        };
        ajaxTeca.validarExistenciaUsuario(usuario, {
            callback: function (data) {
                if (!data) {
                    ajaxTeca.validarExistenciaUsuarioCedula(cedula, {
                        callback: function (data2) {
                            if (!data2) {
                                ajaxTeca.registrarUsuario(datos, {
                                    callback: function (data3) {
                                        if (data3) {
                                            mostrarMensaje('ok', 'se registr� correctamente.');
                                            limpiarFormulario('registerForm');
                                        } else {
                                            mostrarMensaje('error', 'se present� un problema al registrar.');
                                        }
                                    }, timeout: 20000
                                });
                            } else {
                                mostrarMensaje('error', 'ya existe un usuario con ese documento.');
                            }
                        }, timeout: 20000
                    });
                } else {
                    mostrarMensaje('error', 'ya existe un usuario con ese usuario.');
                }
            }, timeout: 20000
        });
    }

    function soloNumeros(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = "0,1,2,3,4,5,6,7,8,9,.";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }

    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " �����abcdefghijklmn�opqrstuvwxyz";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }

    function limpiarFormulario(form) {
        $('#' + form + ' input[type="text"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="number"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="email"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="date"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' select').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="password"]').each(function () {
            $(this).val("");
        });
        $("input:radio").prop("checked", false);
    }

</script>
