
<%-- 
    Document   : registrar-aseguradora
    Created on : 15/09/2017, 03:21:25 PM
    Author     : Ing. Hebert Medelo
--%>
<div class="grid-form" id="xcv">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Parametrizaci�n Tarifas</h3>
        <div class="col-md-12" id="alert"></div>
        <form role="form" id="ListarForm" action="return:false" autocomplete="off">            
            <div id="alert"></div> 
            <div class="row" id="divForm">
                <div class="col-md-12">                 
                    <div class="form-group col-md-6">                       
                        <select id="anio" name="vigencia" class="form-control" title="Ingrese a�o." required>
                            <option value="">Seleccione una a�o</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">                          
                        <button type="submit" class="btn btn-primary" id="btnListarTarifas" onclick="validar('ListarForm', 2)">Buscar</button>
                        <button type="button" class="btn btn-primary" id="btnVerFromEditar" onclick="verFormEditar()">Registrar</button>
                    </div>
                </div>                
            </div>
            <div class="table-responsive" id="tablaAseguradoras">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr id='addTh'>
                            <th>Edad</th>
                        </tr>
                    </thead>                    
                    <tbody id="listado">
                    </tbody>
                </table>          
            </div>
        </form>
        <div class="but_list" id="registerForm" hidden>
            <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active"><a href="#tabRegistrar" id="home-tab" role="tab" data-toggle="tab" aria-controls="tabRegistrar">Registrar Tarifa</a></li>
                    <li role="presentation"><a href="#cargarSXML" role="tab" id="profile-tab" data-toggle="tab" aria-controls="cargarSXML">Cargar Tarifas</a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="tabRegistrar" aria-labelledby="home-tab">
                        <form role="form" id="fromRegister" action="return:false" autocomplete="off">           
                            <div class="row" id="divForm">
                                <div class="col-md-12">                  
                                    <div class="form-group col-md-6">                        
                                        <label for="edad">Edad</label>
                                        <input max="79" min="18" type="number" class="form-control" id="edad" placeholder="Edad" title="Ingrese edad entre 18 y 79 a�os."  onkeypress="return soloNumeros(event);" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="vigencia">Vigencia</label>
                                        <select id="vigencia" name="vigencia" class="form-control" title="Ingrese a�o." required>
                                            <option value="">Seleccione una opci�n</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                        </select>
                                    </div>                                       
                                </div> 
                            </div> 
                            <div class="row">
                                <div class="col-md-12">                  
                                    <div class="form-group col-md-6">                        
                                        <label for="aseguradora">Aseguradora</label>
                                        <select id="aseguradora" name="aseguradora" class="form-control" title="Seleccione aseguradora." required></select>
                                    </div>
                                    <div class="form-group col-md-6">                        
                                        <label for="itp">%ITP Con fallecimiento y auxilio funerario</label>
                                        <input type="text" class="form-control" id="itp" name="itp" placeholder="ITP" title="Ingrese ITP" onkeypress="return soloNumeros(event);" required>
                                    </div>                                    
                                </div>                
                            </div>
                            <div class="col-sm-offset-8"> 
                                <button type="submit" class="btn btn-primary" id="btnRegistrar" onclick="validar('fromRegister', 1)">Registrar</button>                               
                                <button type="reset" class="btn btn-default" id="btnVolver" onclick="volver();">Volver</button>                                
                            </div>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="cargarSXML" aria-labelledby="profile-tab">
                        <form role="form" action="return:false" autocomplete="off" id="formfile">  
                            <div class="row">
                                <div class="col-md-12">                
                                    <div class="form-group col-md-6">                                      
                                        <input type="file" id="file" name="file" placeholder="Archivo XLSX" title="Seleccione un archivo." class="form-control" required>
                                        <p class="help-block">Solo permite archivos .xlsx</p>
                                    </div> 
                                    <div class="form-group col-md-6">                        
                                        <button type="submit" class="btn btn-primary" id="btnRegistrar" onclick="javascript:validar('formfile', 4)">Registrar</button>
                                        <button type="reset" class="btn btn-default" onclick="volver();">Volver</button>   
                                    </div>
                                </div>           
                            </div>
                            <div class="row" id="divTexError" hidden>
                                <div class="col-md-12">                
                                    <div class="form-group">
                                        <textarea id="textError" name="textError" class="form-control" rows="5" style="resize:vertical" readonly></textarea>      
                                    </div>
                                </div>           
                            </div>
                        </form>
                    </div>                    
                </div>
            </div>
        </div>
    </div>   
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" modal-lg>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                <h2 class="modal-title">Editar Tarifa</h2>
            </div>
            <form role="form" id="fromModal" action="return:false" autocomplete="off">          
                <div class="modal-body">
                    <div id="alertM"></div> 
                    <div class="row" id="divForm">
                        <div class="col-md-12">                  
                            <div class="form-group col-md-6">                        
                                <label for="edad">Edad</label>
                                <input max="79" min="18" type="number" class="form-control" id="edadE" placeholder="Edad" title="Ingrese edad entre 18 y 79 a�os."  onkeypress="return soloNumeros(event);" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="vigencia">Vigencia</label>
                                <select id="vigenciaE" name="vigencia" class="form-control" title="Ingrese a�o." required>
                                    <option value="">Seleccione una opci�n</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                </select>
                            </div>                                       
                        </div> 
                    </div> 
                    <div class="row">
                        <div class="col-md-12">                  
                            <div class="form-group col-md-6">                        
                                <label for="aseguradora">Aseguradora</label>
                                <select id="aseguradoraE" name="aseguradora" class="form-control" title="Seleccione aseguradora." required></select>
                            </div>
                            <div class="form-group col-md-6">                        
                                <label for="itp">%ITP</label>
                                <input type="text" class="form-control" id="itpE" name="itp" placeholder="ITP" title="Ingrese ITP" onkeypress="return soloNumeros(event);" required>
                            </div>                                    
                        </div>                
                    </div>                  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btnCerraModal" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary" onclick="validar('fromModal', 3)">Guardar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>



<script>
    var listadoTarifas = [];
    var idTarifa = null;
    jQuery(document).ready(function () {
        listarAseguradoras("aseguradora");
        listarAseguradoras("aseguradoraE");
        $("#tablaAseguradoras").hide();
    });

    function  montarXLSM() {
        if ($('#file')[0].files[0].type != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            notificacion('danger', 'Solo se permiten archivos <strong>.xlsx</strong>.', 'alert');
            return;
        }
        var data;
        data = new FormData();
        data.append('file', $('#file')[0].files[0]);
        jQuery.ajax({
            url: 'ServletArchivoTarifas',
            data: data,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (r) {
                if (r == "true") {
                     notificacion('success', 'Se realizo el registro con <strong>ex�to.</strong>.', 'alert');
                    $("#btnVolver").click();
                    $("#tablaAseguradoras").hide();
                } else {
                    $("#divTexError").show();
                    $("#textError").val(r);
                }
                console.log(r);
            }
        });
    }

    function verFormEditar() {
        $("#registerForm").show();
        $("#ListarForm").hide();
        $("#btnRegistrar").show();
        $("#divTexError").hide();
    }

    function volver() {
        $("#registerForm").hide();
        $("#ListarForm").show();
    }

    function consultarTarifa() {

        var datos = {
            idAseguradora: $('#aseguradora').val(),
            vigencia: $('#vigencia').val(),
            edad: $('#edad').val(),
            ipt: $('#itp').val()
        };
        ajaxTeca.validarTarifa(datos, {
            callback: function (data) {
                if (data == false) {
                    registrar(datos);
                } else {
                    notificacion('danger', 'Ya esta tarifa ex�ste en la aseguradora: <strong>' + $("#aseguradora option:selected").html() + '</strong>.', 'alert');
                }
            },
            timeout: 20000
        });
    }

    function notificacion(tipo, msj, id) {
        $(".alert").alert('close');
        $("#" + id).append('<div class="alert alert-' + tipo + '" role="alert">\n\
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n\
                                                <span aria-hidden="true">&times;</span>\n\
                                            </button>' + msj + '\n\
                                        </div>');
        setTimeout('$(".alert").alert("close");', '3000');
    }

    function registrar(datos) {
        ajaxTeca.registrarTarifas(datos, {
            callback: function (data) {
                if (data) {
                    notificacion('success', 'Se realizo el registro con <strong>ex�to.</strong>.', 'alert');
                    $("#btnVolver").click();
                    $("#tablaAseguradoras").hide();
                } else {
                    notificacion('danger', 'No se pudo realizar el <strong>registro.</strong>.', 'alert');
                }
            },
            timeout: 20000
        });
    }

    function listarAseguradoras(idCombo, valorSeleccionado) {
        ajaxTeca.listarAseguradoras({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            nombre: 'Seleccione Aseguradora '
                        }], 'id', 'nombre');
                    dwr.util.addOptions(idCombo, data, 'id', 'nombre');
                    $("#" + idCombo).val(valorSeleccionado).trigger("change");
                }
            },
            timeout: 20000
        });
    }

    function listarTarifas() {
        var anio = $("#anio").val();
        dwr.util.removeAllRows("listado");
        $("#addTh").text("");
        ajaxTeca.listarTarifas(anio, {
            callback: function (data) {
                if (data != null) {
                    listadoTarifas = data;
                    $("#tablaAseguradoras").show();
                    $("#addTh").append("<th><div class='text-justify'>Edad</th>");
                    for (var i = 0; i < data.length; i++) {
                        $("#addTh").append("<th><div class='text-justify'>ITP (COVERTURA DE INCAPACIDAD TOTAL Y PERMANENTE) + Fallecimiento y auxilio funerario <big>" + data[i].nombreAseguradora + "</big></th>");
                    }
                    for (var j = 18; j < 80; j++) {
                        var td = "<tr><td>" + j + "</td>";
                        for (var a = 0; a < data.length; a++) {
                            for (var x = 0; x < data[a].tarifasAseguradoras.length; x++) {
                                var edad = data[a].tarifasAseguradoras[x].edad;
                                if (j == parseInt(edad)) {
                                    var ipt = data[a].tarifasAseguradoras[x].ipt;
                                    td += "<td><button title='Editar tarifa' type='button' class='btn btn-xs btn-link' data-toggle='modal' data-target='#myModal' onclick='datosTarifa(" + a + ", " + x + ");'>" + ipt + "</button></td>";
                                    break;
                                } else {
                                    if (x == data[a].tarifasAseguradoras.length - 1) {
                                        td += "<td>0</td>";
                                    }
                                }
                            }
                        }
                        td += "</tr>";
                        $("#listado").append(td);
                    }
                } else {
                    notificacion('danger', 'No se encontraron tarifas con el <strong>a�o</strong> seleccionado.', 'alert');
                }
            },
            timeout: 20000
        });
    }

    function datosTarifa(a, b) {
        //$("#registerForm").show();
        //$("#ListarForm").hide();
        //$("#btnRegistrar").hide();        

        $('#aseguradoraE').val(listadoTarifas[a].tarifasAseguradoras[b].idAseguradora);
        $('#vigenciaE').val(listadoTarifas[a].tarifasAseguradoras[b].vigencia);
        $('#edadE').val(listadoTarifas[a].tarifasAseguradoras[b].edad);
        $('#itpE').val(listadoTarifas[a].tarifasAseguradoras[b].ipt);
        idTarifa = listadoTarifas[a].tarifasAseguradoras[b].id;
    }

    function validarTarifaEditar() {

        var datos = {
            idAseguradora: $('#aseguradoraE').val(),
            vigencia: $('#vigenciaE').val(),
            edad: $('#edadE').val(),
            ipt: $('#itpE').val(),
            id: idTarifa
        };
        ajaxTeca.validarTarifaEditar(datos, {
            callback: function (data) {
                if (data == false) {
                    editarTarifa(datos);
                } else {
                    notificacion('danger', 'Ya esta tarifa ex�ste en la aseguradora: <strong>' + $("#aseguradoraE option:selected").html() + '</strong>.', 'alertM');
                }
            },
            timeout: 20000
        });
    }

    function editarTarifa(datos) {
        ajaxTeca.editarTarifa(datos, {
            callback: function (data) {
                if (data) {
                    notificacion('success', 'Se actualizo el registro con <strong>ex�to.</strong>.', 'alert');
                    $("#btnVolver").click();
                    $("#btnCerraModal").click();
                    listarTarifas();
                } else {
                    notificacion('danger', 'No se pudo actualizar el <strong>registro.</strong>.', 'alertM');
                }
            },
            timeout: 20000
        });
    }

    var nextFunction = null;
    function validar(form, Id) {
        nextFunction = Id;
        $(".form-group").removeClass("has-error");
        $('.error').remove();
        $('input[type="text"]').each(function () {
            $(this).val($(this).val().trim());
        });
        $('textarea').each(function () {
            $(this).val($(this).val().trim());
        });
        $('#' + form).validate({
            highlight: function (label) {
                jQuery(label).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (label) {
                jQuery(label).closest('.form-group').removeClass('has-error');
                label.remove();
            }, errorPlacement: function (error, element) {
                var placement = element.closest('.input-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            },
            submitHandler: function () {
                ejecutarPostValidate();
            }
        });
    }

    function ejecutarPostValidate() {

        switch (nextFunction) {
            case 1:
                consultarTarifa();
                break;
            case 2:
                listarTarifas();
                break;
            case 3:
                validarTarifaEditar();
                break;
            case 4:
                montarXLSM();
                break;
        }
        nextFunction = null;
    }

    function soloNumeros(e) {
        tecla = (document.all) ? e.keyCode : e.which;
        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }
        // Patron de entrada, en este caso solo acepta numeros
        patron = /[0-9.]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }
</script>