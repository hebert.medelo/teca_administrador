<%-- 
    Document   : cargar_archivo_fasecolda
    Created on : 9/02/2018, 02:27:52 PM
    Author     : JHONATAN
--%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <style>

            .loader-backdrop {
                position: fixed;
                top: 0;
                left: 0;
                height: 100%;
                width: 100%;
                z-index: 999999;
                background-color: #fff;
            }
            .loader {
                position:absolute;  top:40%;  left:40%; right: 40%;
                width: 400px;
                height: 254px;
                position: absolute;
                background-image: url("images/load.gif");
                margin: -50px 0px 0px -100px;
            }
        </style>
    </head>

    <body>
        <div class="grid-form">
            <div class="grid-form1">
                <h3 id="forms-example" class="">Cargar archivo de fasecolda</h3>
                <div id="divMensajeOk" class="form-group has-success" style="display: none;">
                    <div id="mensajeOk" class="form-control1">-</div>
                </div>
                <div id="divMensajeError" class="form-group has-error" style="display: none;">
                    <div id="mensajeError" class="form-control1">-</div>
                </div>
                <form role="form" action="return:false" autocomplete="off" id="formfile">  
                    <div class="row">
                        <div class="col-md-12">                
                            <div class="form-group col-md-6">                                      
                                <input type="file" id="file" name="file" placeholder="Archivo XLSX" title="Seleccione un archivo." class="form-control" required>
                                <p class="help-block">Solo permite archivos .xlsx</p>
                            </div> 
                            <div class="form-group col-md-6">                        
                                <button type="submit" class="btn btn-primary" id="btnRegistrar" onclick="javascript:validar('formfile');">Registrar</button>
                            </div>
                        </div>           
                    </div>
                    <div class="row" id="divTexError" hidden>
                        <div class="col-md-12">                
                            <div class="form-group">
                                <textarea id="textError" name="textError" class="form-control" rows="5" style="resize:vertical" readonly></textarea>      
                            </div>
                        </div>           
                    </div>
                </form>
            </div>
        </div>
        <div class="loader-backdrop">           
            <center>
                <div class="loader" id="loader">           
                </div>
            </center>
        </div>

        <script>

            $(document).ready(function () {
                $(".loader-backdrop").fadeOut();
            });

            function montarExcel() {
                if ($('#file')[0].files[0].type != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                    mostrarMensaje('error', 'Solo se permiten archivos <strong>.xlsx</strong>.');
                    return;
                }
                $(".loader-backdrop").fadeIn();
                var excel;
                excel = new FormData();
                excel.append('file', $('#file')[0].files[0]);
                jQuery.ajax({
                    url: 'UploadExcel',
                    data: excel,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (data) {
                        $(".loader-backdrop").fadeOut();
                        if (data == "true") {
                            $("#file").val("");
                            mostrarMensaje('ok', 'Se registr� con exito.');
                            $("#divTexError").hide();
                        } else {
                            mostrarMensaje('error', 'No se pudo realizar el registro.');
                            $("#divTexError").show();
                            $("#textError").val(data);
                        }
                    }
                });
            }

            function validar(form) {
                $('#' + form).validate({
                    highlight: function (label) {
                        $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    success: function (label) {
                        $(label).closest('.form-group').removeClass('has-error');
                        label.remove();
                    },
                    errorPlacement: function (error, element) {
                        var placement = element.closest('.input-group');
                        if (!placement.get(0)) {
                            placement = element;
                        }
                        if (error.text() !== '') {
                            placement.after(error);
                        }
                    },
                    submitHandler: function () {
                        montarExcel();
                    }
                });
            }
        </script>
    </body>
</html>

