<%-- 
    Document   : registrar-referido
    Created on : 5/02/2018, 10:26:42 AM
    Author     : JHONATAN
--%>

<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Registro de referidos</h3>
        <div id="divMensajeOk" class="form-group has-success" style="display: none;">
            <div id="mensajeOk" class="form-control1">-</div>
        </div>
        <div id="divMensajeError" class="has-error" style="display: none;">
            <div id="mensajeError" class="form-control1">-</div>
        </div>

        <form role="form" id="busquedaForm" action="return:false" autocomplete="off">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group col-md-5">                        
                        <label for="cedulaAsesor">C�dula asesor</label>
                        <input type="text" class="form-control" id="cedulaAsesor" name="cedulaAsesor" title="Ingrese c�dula" placeholder="C�dula asesor" maxlength="11" onkeypress="return soloNumeros(event);" required >
                    </div>           
                    <div class="col-md-2">     
                        <button type="submit" class="btn btn-primary" id="" onclick="javascript:validar('busquedaForm', 1);">Buscar</button>
                    </div>
                </div>
            </div>
        </form>
        <table class="table table-hover table-striped" id="tablaDetalle" hidden>          
            <tbody>
                <tr>
                    <td style="text-align: right"><strong>Nombre: </strong></td>
                    <td id="nombreAsesor"></td>
                    <td style="text-align: right"><strong>C�dula </strong></td>
                    <td id="cedAsesor"></td>                    
                </tr>
            </tbody>
        </table>

        <form role="form" id="registrarForm" action="return:false" autocomplete="off" hidden>
            <div class="row">
                <div class="col-md-12">                  
                    <div class="form-group col-md-5">                        
                        <label for="cedulaReferido">C�dula referido</label>
                        <input type="text" class="form-control" id="cedulaReferido" name="cedulaReferido" title="Ingrese c�dula referido" placeholder="C�dula asesor" maxlength="11" onkeypress="return soloNumeros(event);" required>
                    </div>
                    <div class="form-group col-md-5">                        
                        <label for="porcentajeReferido">Porcentaje referido</label>
                        <input type="text" class="form-control" id="porcentajeReferido" name="porcentajeReferido" title="Ingrese porcentaje referido" placeholder="Porcentaje referido" maxlength="4" onkeypress="return soloNumeros(event);" required>
                    </div>
                    <div class="col-sm-offset-10">     
                        <button type="submit" class="btn btn-primary" id="" onclick="javascript:validar('registrarForm', 2);">Agregar</button>
                    </div>
                </div>
            </div>
        </form>

        <div id="listadoReferidos">
            <div class="row">
                <div class="table-responsive col-md-12" id="divListaUsuarios">
                    <table class="table" id="tablaUsuarios">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Documento</th>
                                <th>Nombre</th>
                                <th>Apellido</th>                           
                                <th>% referido</th>
                                <th>Desasociar</th>
                            </tr>
                        </thead>                    
                        <tbody id="listado">
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {
    });

    var listado = [];
    var mapa = [
        function (data) {
            return data.idAsesorReferido;
        },
        function (data) {
            return data.documento;
        },
        function (data) {
            return data.nombre;
        },
        function (data) {
            return data.apellido;
        },
        function (data) {
            return data.porcentajeReferido;
        },
        function (data) {
            return "<td><button class='label label-success' onclick='desasociarAsesor(" + data.id + ");'>Desasociar</button></td>";
        }
    ];

    var nextFunction = null;
    function validar(form, id) {
        nextFunction = id;
        console.log("form", form);
        console.log("id", id);
        $('#' + form).validate({
            highlight: function (label) {
                jQuery(label).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (label) {
                jQuery(label).closest('.form-group').removeClass('has-error');
                label.remove();
            }, errorPlacement: function (error, element) {
                var placement = element.closest('.input-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            },
            submitHandler: function () {
                ejecutarPostValidate();
            }
        });
    }

    function ejecutarPostValidate() {
        console.log("entra a postvalidate");
        switch (nextFunction) {
            case 1:
                buscarAsesor();
                break;
            case 2:
                registrar();
                break;
        }
        nextFunction = null;
    }


    function soloNumeros(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = "0,1,2,3,4,5,6,7,8,9,.";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }

    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " �����abcdefghijklmn�opqrstuvwxyz";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }

    function limpiarFormulario(form) {
        $('#' + form + ' input[type="text"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="number"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="email"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="date"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' select').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="password"]').each(function () {
            $(this).val("");
        });
        $("input:radio").prop("checked", false);
    }

    var idAsesor = null;
    var cedAsesor = null;

    function desasociarAsesor(id) {
        ajaxTeca.desasociarReferido(id, {
            callback: function (data) {
                if (data) {
                    mostrarMensaje('ok', 'se desasocio exitosamente.');
                    buscarAsesor2();
                } else {
                    mostrarMensaje('error', 'no su pudo desasociar.');
                }
            }, timeout: 20000
        });
    }

    function buscarAsesor() {
        console.log("entra a buscarasesor");
        limpiarFormulario("registrarForm");
        listado = [];
        dwr.util.removeAllRows("listado");
        var cedulaAsesor = $("#cedulaAsesor").val();
        ajaxTeca.buscarAsesorPorCedula(cedulaAsesor, {
            callback: function (data) {
                if (data != null) {
                    idAsesor = data.id;
                    console.log("data de asesor", data.id);
                    $("#tablaDetalle").show();
                    $("#registrarForm").show();
                    $("#cedAsesor").text(data.documento);
                    $("#nombreAsesor").text(data.nombre);
                    cedAsesor = data.documento;


                    ajaxTeca.buscarReferidosPorIdAsesor(data.id, {
                        callback: function (data2) {
                            if (data2 != null) {
                                mostrarMensaje('ok', 'tiene referidos.');
                                listado = data2;
                                dwr.util.addRows("listado", data2, mapa, {
                                    escapeHtml: false
                                });

                            } else {
                                mostrarMensaje('error', 'no se encontraron referidos.');
                            }
                        }, timeout: 20000
                    });
                } else {
                    $("#tablaDetalle").hide();
                    $("#registrarForm").hide();
                    $("#cedAsesor").text("");
                    $("#nombreAsesor").text("");
                    mostrarMensaje('error', 'no se encontr� asesor con la c�dula ingresada.');
                }
            }, timeout: 20000
        });
    }

    function buscarAsesor2() {
        var cedulaAsesor = $("#cedulaAsesor").val();
        ajaxTeca.buscarAsesorPorCedula(cedulaAsesor, {
            callback: function (data) {
                if (data != null) {
                    ajaxTeca.buscarReferidosPorIdAsesor(data.id, {
                        callback: function (data2) {
                            if (data2 != null) {
                                dwr.util.removeAllRows("listado");
                                listado = data2;
                                dwr.util.addRows("listado", data2, mapa, {
                                    escapeHtml: false
                                });
                            } else {
                                mostrarMensaje('error', 'no se encontraron referidos.');
                            }
                        }, timeout: 20000
                    });
                }
            }, timeout: 20000
        });
    }

    function registrar() {
        console.log("entra a registrar");
        var cedulaReferido = $("#cedulaReferido").val().trim();
        console.log("cedulaReferido", cedulaReferido);
        console.log("cedAsesor", cedAsesor);
        if (cedAsesor == cedulaReferido) {
            mostrarMensaje('error', 'El asesor no se puede referir a s� mismo.');
        } else {

            ajaxTeca.buscarAsesorPorCedula(cedulaReferido, {
                callback: function (data) {
                    if (data != null) {
                        var x = false;
                        if (listado != null) {
                            for (var i = 0; i < listado.length; i++) {
                                if (listado[i].idAsesorReferido == data.id) {
                                    mostrarMensaje('error', 'El referido ya existe para esta asesor.');
                                    x = true;
                                    break;
                                }
                            }
                        }
                        if (!x) {
                            console.log("idasesor validar", idAsesor);
                            console.log("idreferido validar", data.id);
                            ajaxTeca.validarReferido(idAsesor, data.id, {
                                callback: function (data2) {
                                    console.log("se ejecuta ajax de validar");
                                    if (!data2) {
                                        ajaxTeca.asesorExisteComoReferido(data.id, {
                                            callback: function (data2) {
                                                console.log("se ejecuta ajax de asesorExisteComoReferido");
                                                if (!data2) {
                                                    console.log("No existe como referido");
                                                    console.log("se env�a a registrar");
                                                    var datos = {
                                                        id: idAsesor,
                                                        idAsesorReferido: data.id,
                                                        porcentajeReferido: $("#porcentajeReferido").val()
                                                    };
                                                    ajaxTeca.registrarReferido(datos, {
                                                        callback: function (data2) {
                                                            console.log("se ejecuta ajax de registrar");
                                                            if (data2) {
                                                                console.log("se env�a a registrar");
                                                                mostrarMensaje('ok', 'Se registr� correctamente.');
                                                                buscarAsesor2();
                                                                limpiarFormulario("registrarForm");
                                                                limpiarFormulario("busquedaForm");
                                                                $("#registrarForm").hide();
                                                                $("#tablaDetalle").hide();
                                                                $("#cedAsesor").text("");
                                                                $("#nombreAsesor").text("");
                                                            } else {
                                                                mostrarMensaje('error', 'Problema al registrar.');
                                                            }
                                                        }, timeout: 20000
                                                    });
                                                } else {
                                                    mostrarMensaje('error', 'El asesor que se intenta referir ya est� referido.');
                                                }
                                            }, timeout: 20000
                                        });
                                    } else {
                                        mostrarMensaje('error', 'No se puede referir a quien lo refiri�.');
                                    }
                                }, timeout: 20000
                            });
                        }
                    } else {
                        mostrarMensaje('error', 'el asesor que se quiere referir no existe.');
                    }
                }, timeout: 20000
            });
        }
    }
</script>