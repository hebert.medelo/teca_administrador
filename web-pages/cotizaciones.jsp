<%-- 
    Document   : cotizaciones
    Created on : 15/09/2017, 03:21:25 PM
    Author     : Ing. Hebert Medelo
--%>
<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Gestionar Cotizaci�n</h3>
        <div id="divMensajeOk" class="form-group has-success" style="display: none;">
            <div id="mensajeOk" class="form-control1">-</div>
        </div>
        <div id="divMensajeError" class="form-group has-error" style="display: none;">
            <div id="mensajeError" class="form-control1">-</div>
        </div>
        <form role="form" class="form-horizontal" id="registerForm" action="return:false" autocomplete="off">
            <div class="form-group">
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">De</span>
                        <input type="date" class="form-control1" name="start" id="fechaInicio" placeholder="Fecha de inicio" title="Ingrese fecha inicial" data-date-end-date="0d" required onchange="javascript:validarFecha(this.value)">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">a</span>
                        <input type="date" class="form-control1" name="end" id="fechaFinal" placeholder="Fecha de final" title="Ingrese fecha final" data-date-end-date="0d" required min="">     
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <div class="input-group">
                        <label for="selectTipoSeguro">Tipo de Seguro</label>
                        <select class="form-control" title="Seleccione tipo de seguro" id="selectTipoSeguro" name="selectTipoSeguro" onchange="seleccionarTipoSeguro(this.value)" required></select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">                  
                    <div class="form-group col-md-3" id="mostrarPlaca">                        
                        <label for="placa">Placa</label>
                        <input type="text" class="form-control" id="placa" placeholder="Placa" maxlength="8" >
                    </div>
                    <div class="form-group col-md-3" id="mostrarCedula">
                        <label for="cedula">C�dula</label>
                        <input type="text" class="form-control" id="cedula" placeholder="C�dula" maxlength="10">
                    </div>
                    <div class="form-group col-md-3">                        
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" placeholder="Nombre" maxlength="50" onkeypress="return soloLetras(event);">
                    </div>
                    <div class="form-group col-md-3" id="mostrarApellido">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" id="apellido" placeholder="Apellido" maxlength="50" onkeypress="return soloLetras(event);">
                    </div>
                </div>           
            </div>
            <div class="col-sm-offset-9">     
                <button type="submit" class="btn btn-primary" onclick="javascript:validar('registerForm');">Buscar</button>
            </div>
        </form>

        <table class="table" id="tablaCotizaciones" hidden>
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Transacci�n</th>
                    <th>Fecha</th>
                    <th>Aseguradora</th>                           
                    <th>Detalle</th>
                </tr>
            </thead>                    
            <tbody id="listadoDetalle">
            </tbody>
        </table>

        <table class="table table-hover table-striped" id="tablaDetalle" hidden>          
            <tbody>
                <tr>
                    <td style="text-align: right"><strong>Nombre: </strong></td>
                    <td id="cliente"></td>
                    <td style="text-align: right"><strong>Apellido: </strong></td>
                    <td id="detalleApellido"></td>                    
                </tr>
                <tr>
                    <td style="text-align: right"><strong>Tipo documento: </strong></td>
                    <td id="tipoDocumento"></td>
                    <td style="text-align: right"><strong>Documento: </strong></td>
                    <td id="documento"></td> 
                </tr>
                <tr>
                    <td style="text-align: right"><strong>Fecha Nacimento: </strong></td>
                    <td id="fechaNacimento"></td>
                    <td style="text-align: right"><strong>Tel�fono: </strong></td>
                    <td id="telefono"></td> 
                </tr>
                <tr>
                    <td style="text-align: right"><strong>Correo: </strong></td>
                    <td id="correo"></td>                     
                    <td style="text-align: right"><strong>Ubicaci�n: </strong></td>
                    <td id="ubicacion"></td> 
                </tr>
                <tr>
                    <td style="text-align: right"><strong>Departamento: </strong></td>
                    <td id="departamnento"></td>
                    <td style="text-align: right"><strong>Ciudad Circulaci�n: </strong></td>
                    <td id="municipio"></td> 
                </tr>

                <tr>
                    <td style="text-align: right"><strong>Marca: </strong></td>
                    <td id="marca"></td>
                    <td style="text-align: right"><strong>Linea: </strong></td>
                    <td id="linea"></td> 
                </tr>

                <tr>
                    <td style="text-align: right"><strong>Modelo: </strong></td>
                    <td id="modelo"></td>
                    <td style="text-align: right"><strong>Placa: </strong></td>
                    <td id="placaDetalle"></td> 
                </tr>
                <tr>
                    <td style="text-align: right"><strong>Tipo Servicio: </strong></td>
                    <td id="tipoServicio"></td>
                    <td style="text-align: right"><strong>Duraci�n Seguro </strong></td>
                    <td id="duracion"></td>
                </tr>
                <tr>
                    <td style="text-align: right"><strong>Aseguradora </strong></td>
                    <td id="aseguradora"></td>
                    <td style="text-align: right"><strong>Tipo Seguro: </strong></td>
                    <td id="tipoSeguro"></td> 
                </tr>                
                <tr>                    
                    <td style="text-align: right"><strong>valor Asegurado: </strong></td>
                    <td id="valorAsegurado"></td> 
                    <td style="text-align: right"><strong>Precio Seguro: </strong></td>
                    <td id="precio"></td> 
                </tr>
                <tr>
                    <td id="labelObservacionAsesor" style="text-align: right"><strong>Observaci�n asesor: </strong></td>
                    <td id="observacionAsesor"></td> 
                    <td style="text-align: right"><strong>Observaci�n: </strong></td>
                    <td><textarea id="observacionAuto"></textarea></td>
                </tr> 
            </tbody>
        </table>

        <table class="table table-hover table-striped" id="tablaDetalleVida" hidden>          
            <tbody>
                <tr>
                    <td style="text-align: center"><strong>Nombre: </strong></td>
                    <td id="detalleNombre"></td>
                    <td style="text-align: center"><strong>Apellido: </strong></td>
                    <td id="detalleApellido2"></td> 
                </tr>
                <tr>

                    <td style="text-align: center"><strong>Valor asegurado: </strong></td>
                    <td id="detalleValorAsegurado"></td>
                    <td style="text-align: center"><strong>Tiempo de duraci�n: </strong></td>
                    <td id="detalleTiempoDuracion"></td>
                </tr>
                <tr>
                    <td style="text-align: center"><strong>Fecha de nacimiento: </strong></td>
                    <td id="detalleFechaNacimiento"></td>
                    <td style="text-align: center"><strong>Prima comercial anual: </strong></td>
                    <td id="detallePrimaComercial"></td>
                </tr>
                <tr>
                    <td style="text-align: center"><strong>Correo: </strong></td>
                    <td id="detalleCorreo"></td>
                    <td style="text-align: center"><strong>Tel�fono: </strong></td>
                    <td id="detalleTelefono"></td>

                </tr>
                <tr>
                    <td style="text-align: center"><strong>Ciudad Circulaci�n: </strong></td>
                    <td id="detalleCiudadCirculacion"></td>
                    <td style="text-align: center"><strong>Aseguradora: </strong></td>
                    <td id="detalleAseguradora"></td>
                </tr>
                <tr>
                    <td id="labelObservacionAsesorVida" style="text-align: right"><strong>Observaci�n asesor: </strong></td>
                    <td id="observacionAsesorVida"></td>
                    <td style="text-align: center"><strong>Observaci�n: </strong></td>
                    <td><textarea id="observacionVida"></textarea></td>
                </tr> 
            </tbody>
        </table>

        <div class="col-sm-offset-8"> 
            <button type="submit" class="btn btn-primary" id="btnRegistrar" onclick="javascript:registrar()">Registrar</button>
            <button type="button" class="btn btn-primary" id="btnVolver" onclick="javascript:volver()">Volver</button>
        </div>


    </div>   
</div>
<script>
    jQuery(document).ready(function () {
        $("#btnVolver").hide();
        $("#btnRegistrar").hide();
        listarTipoSeguro("selectTipoSeguro");
    });

    var idTipoSeguro = null;

    function registrar() {
        var observacion = null;
        if (idTipoSeguro == "1") {
            observacion = $("#observacionAuto").val();
        } else {
            observacion = $("#observacionVida").val();
        }
        if (observacion == "") {
            mostrarMensaje('error', 'El campo de observaci�n es obligatorio.');
            window.scrollTo(0, 0);
            return;
        }
        var datos = {
            idCotizacion: idCotizacion,
            observacion: observacion
        };
        ajaxTeca.registrarCotizacionUsuario(datos, {
            callback: function (data) {
                if (data) {
                    mostrarMensaje('ok', 'Se registro con �xito.');
                    $("#btnVolver").click();
                    limpiarCampos();
                    dwr.util.removeAllRows("listadoDetalle");
                    $("#tablaCotizaciones").hide();

                } else {
                    mostrarMensaje('error', 'Error al registrar.');
                    window.scrollTo(0, 0);

                }
            },
            timeout: 20000
        });
    }

    function volver() {
        $("#tablaDetalle").hide();
        $("#tablaDetalleVida").hide();
        $("#tablaCotizaciones").show();
        $("#registerForm").show();
        $("#btnVolver").hide();
        $("#btnRegistrar").hide();
    }

    var mapa = [
        function (data) {
            return data.concecutivo;
        },
        function (data) {
            return data.id;
        },
        function (data) {
            return data.fecha;
        },
        function (data) {
            return data.nombraAseguradora;
        },
        function (data) {
            return "<td><a class='label label-success' onclick='verDetalle(" + data.id + ");'>Datalle</a></td>";
        }
    ];

    function listarCotizaciones() {
        var datos = {
            fechaInicial: $("#fechaInicio").val(),
            fechaFinal: $("#fechaFinal").val(),
            placa: $("#placa").val(),
            cedula: $("#cedula").val(),
            nombre: $("#nombre").val(),
            apellido: $("#apellido").val(),
            idTipoSeguro: $("#selectTipoSeguro").val()
        };
        ajaxTeca.listarCotizaciones(datos, {
            callback: function (data) {
                if (data != null) {
                    $("#tablaCotizaciones").show();
                    dwr.util.removeAllRows("listadoDetalle");
                    dwr.util.addRows("listadoDetalle", data, mapa, {
                        escapeHtml: false
                    });

                } else {
                    $("#tablaCotizaciones").hide();
                    mostrarMensaje('error', 'No se encontraron resultados.');
                }
            },
            timeout: 20000
        });
    }

    function verDetalle(id) {
        idCotizacion = id;
        ajaxTeca.detalleCotizacion(id, {
            callback: function (data) {
                if (data != null) {
                    $("#tablaCotizaciones").hide();
                    $("#registerForm").hide();
                    $("#btnVolver").show();
                    $("#btnRegistrar").show();
                    if (data.idTipoSeguro == "1") {
                        $("#tablaDetalle").show();
                        $("#cliente").text(data.nombre);
                        $("#detalleApellido").text(data.apellido);
                        $("#tipoDocumento").text(data.tipoDocumento);
                        $("#documento").text(data.documento);
                        $("#fechaNacimento").text(data.fechaNacimiento);
                        $("#telefono").text(data.telefono);
                        $("#departamnento").text(data.departamentoNombre);
                        $("#municipio").text(data.municipioNombre);
                        $("#ubicacion").text(data.ubicacionVehiculo);
                        $("#correo").text(data.correo);
                        $("#marca").text(data.marca);
                        $("#linea").text(data.lineaVehiculo);
                        $("#modelo").text(data.modelo);
                        $("#placaDetalle").text(data.placa);
                        $("#aseguradora").text(data.aseguradora);
                        $("#tipoSeguro").text(data.tipoSeguro);
                        $("#duracion").text(data.tiempoDuracion);
                        $("#precio").text(data.valorTotalSeguro);
                        $("#valorAsegurado").text(data.valorAsegurado);
                        $("#tipoServicio").text(data.tipoServicio);
                        if (data.observacionAsesor == null) {
                            $("#labelObservacionAsesor").hide();
                            $("#observacionAsesor").hide();
                        } else {
                            $("#labelObservacionAsesor").show();
                            $("#observacionAsesor").show();
                            $("#observacionAsesor").text(data.observacionAsesor);
                        }
                    } else {
                        $("#tablaDetalleVida").show();
                        $("#detalleNombre").text(data.nombre);
                        $("#detalleApellido2").text(data.apellido);
                        $("#detalleCorreo").text(data.correo);
                        $("#detalleFechaNacimiento").text(data.fechaNacimiento);
                        $("#detalleValorAsegurado").text(data.valorAsegurado);
                        $("#detalleTelefono").text(data.telefono);
                        $("#detalleCiudadCirculacion").text(data.municipioNombre);
                        $("#detalleAseguradora").text(data.aseguradora);
                        $("#detallePrimaComercial").text(data.primaComercialAnual);
                        $("#detalleTiempoDuracion").text(data.tiempoDuracion);
                        if (data.observacionAsesor == null) {
                            $("#labelObservacionAsesorVida").hide();
                            $("#observacionAsesorVida").hide();
                        } else {
                            $("#labelObservacionAsesorVida").show();
                            $("#observacionAsesorVida").show();
                            $("#observacionAsesorVida").text(data.observacionAsesor);
                        }
                    }
                    idTipoSeguro = data.idTipoSeguro;
                } else {
                    mostrarMensaje('error', 'No se encontraron resultados.');
                }
            },
            timeout: 20000
        });
    }

    function listarTipoSeguro(idCombo, valorSeleccionando) {
        ajaxTeca.listarTipoSeguro({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            nombre: 'Seleccione tipo de seguro'
                        }], 'id', 'nombre');
                    dwr.util.addOptions(idCombo, data, 'id', 'nombre');
                    $('#' + idCombo).val(valorSeleccionando);
                }
            },
            timeout: 20000
        });
    }

    function seleccionarTipoSeguro(value) {
        if (value == "2") {
            $("#mostrarPlaca").hide();
            $("#mostrarCedula").hide();
        } else {
            $("#mostrarPlaca").show();
            $("#mostrarCedula").show();
        }
    }

    function limpiarCampos() {
        $("#cliente").text("");
        $("#detalleApellido").text("");
        $("#tipoDocumento").text("");
        $("#documento").text("");
        $("#fechaNacimento").text("");
        $("#telefono").text("");
        $("#departamnento").text("");
        $("#municipio").text("");
        $("#ubicacion").text("");
        $("#correo").text("");
        $("#marca").text("");
        $("#linea").text("");
        $("#modelo").text("");
        $("#placaDetalle").text("");
        $("#aseguradora").text("");
        $("#tipoSeguro").text("");
        $("#duracion").text("");
        $("#precio").text("");
        $("#valorAsegurado").text("");
        $("#tipoServicio").text("");
        $("#detalleNombre").text("");
        $("#detalleApellido2").text("");
        $("#detalleCorreo").text("");
        $("#detalleFechaNacimiento").text("");
        $("#detalleValorAsegurado").text("");
        $("#detalleTelefono").text("");
        $("#detalleCiudadCirculacion").text("");
        $("#detalleAseguradora").text("");
        $("#observacionVida").val("");
        $("#observacionAuto").val("");
        $("#observacionAsesor").text("");
        $("#observacionAsesorVida").text("");
    }

    function limpiarFormulario(form) {
        $('#' + form + ' input[type="text"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="number"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="email"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="date"]').each(function () {
            $(this).val("");
        });
        $('#' + form + ' select').each(function () {
            $(this).val("");
        });
        $('#' + form + ' input[type="password"]').each(function () {
            $(this).val("");
        });
        $("input:radio").prop("checked", false);
    }

    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " �����abcdefghijklmn�opqrstuvwxyz";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }

    function validar(form) {
        $('#' + form).validate({
            highlight: function (label) {
                $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (label) {
                $(label).closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                var placement = element.closest('.input-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            },
            submitHandler: function () {
                listarCotizaciones();
            }
        });
    }

    function validarFecha(valor) {
        $("#fechaFinal").val("");
        $("#fechaFinal").attr("min", valor);
    }

</script>