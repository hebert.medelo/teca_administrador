<%-- 
    Document   : cambiar-contrasenia
    Created on : 22/01/2018, 10:20:05 AM
    Author     : JHONATAN
--%>
<%@page import="co.mobiltech.teca.mvc.dto.DatosUsuarioDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="co.mobiltech.teca.mvc.dto.*"%>
<%@ page import="java.util.*,java.io.*"%>
<%@page session='true'%>

<%

    DatosUsuarioDTO datosUsuario = (DatosUsuarioDTO) session.getAttribute("datosUsuario");

%>

<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Cambiar contraseña</h3>
        <form role="form" id="formContrasenia" action="return:false" autocomplete="off">
            <div id="divMensajeOk" class="form-group has-success" style="display: none;">
                <div id="mensajeOk" class="form-control1">-</div>
            </div>
            <div id="divMensajeError" class="form-group has-error" style="display: none;">
                <div id="mensajeError" class="form-control1">-</div>
            </div>
            <div class="row">
                <div class="col-sm-12"> 
                    <div class="form-group col-sm-3"></div>
                    <div class="form-group col-sm-6">
                        <label for="contraseniaAnterior" class="control-label">Contraseña Actual</label>
                        <div class="input-group">                                
                            <span class="input-group-addon"><i class="icon-lock-open"></i></span>
                            <input minlength="6" maxlength="10" type="password" class="form-control" name="contraseniaAnterior" id="contraseniaAnterior" placeholder="Contraseña actual" required title="Ingrese contraseña, Mínimo 6 y máximo 10 caracteres">
                        </div>
                    </div>
                    <div class="form-group col-sm-3"></div>
                </div> 
            </div> 

            <div class="row">
                <div class="col-sm-12"> 
                    <div class="form-group col-sm-3"></div>
                    <div class="form-group col-sm-6">
                        <label for="nuevaContrasenia" class="control-label">Nueva Contraseña</label>
                        <div class="input-group">                                
                            <span class="input-group-addon"><i class="icon-lock-3"></i></span>
                            <input minlength="6" maxlength="10" type="password" class="form-control" name="nuevaContrasenia" id="nuevaContrasenia" placeholder="Nueva contraseña" required title="Ingrese contraseña, Mínimo 6 y máximo 10 caracteres">                                                       
                        </div>
                    </div>
                    <div class="form-group col-sm-3"></div>
                </div> 
            </div>

            <div class="row">
                <div class="col-sm-12"> 
                    <div class="form-group col-sm-3"></div>
                    <div class="form-group col-sm-6">
                        <label for="confNnuevaContrasenia" class="control-label">Comprobar Contraseña</label>
                        <div class="input-group">                                
                            <span class="input-group-addon"><i class="icon-lock-3"></i></span>
                            <input minlength="6" maxlength="10" type="password" class="form-control" equalTo="#nuevaContrasenia" name="confNnuevaContrasenia" id="confNnuevaContrasenia" placeholder="Comprobar contraseña" required title="Las contraseñas no coinciden">                                                       
                        </div>
                    </div>
                    <div class="form-group col-sm-3"></div>
                </div> 
            </div>

            <div class="form-group">
                <div class="col-sm-offset-6">
                    <button type="submit" class="btn btn-primary" onclick="javascript:validar('formContrasenia');">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>

    function validar(form) {
        $('#' + form).validate({
            highlight: function (label) {
                $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (label) {
                $(label).closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                var placement = element.closest('.input-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            },
            submitHandler: function () {
                cambiarContrasenia();
            }
        });
    }

    function cambiarContrasenia() {
        var datos = {
            clave: $('#contraseniaAnterior').val(),
            nuevaClave: $('#nuevaContrasenia').val()
        };
        ajaxTeca.validarContrasenia(datos, {
            callback: function (data) {
                if (data) {
                    ajaxTeca.cambiarContrasenia(datos, {
                        callback: function (data2) {
                            if (data2) {
                                $('#formContrasenia input[type="password"]').each(function () {
                                    $(this).val("");
                                });
                                mostrarMensaje('ok', 'La contraseña se actualizó con éxito.');
                            } else {
                                mostrarMensaje('error', 'No se pudo cambiar la contraseña.');
                            }
                        }, timeout: 20000
                    });
                } else {
                    mostrarMensaje('error', 'La clave anterior no coincide con la ingresada.');
                }
            }, timeout: 20000
        });
    }

    function validarUsuarios(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = "abcdefghijklmnopqrstuvwxyz,0,1,2,3,4,5,6,7,8,9";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }
</script>
