<%-- 
    Document   : reporte-referido
    Created on : 5/02/2018, 10:26:54 AM
    Author     : JHONATAN
--%>
<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Reporte de referidos</h3>
        <div id="divMensajeOk" class="form-group has-success" style="display: none;">
            <div id="mensajeOk" class="form-control1">-</div>
        </div>
        <div id="divMensajeError" class="has-error" style="display: none;">
            <div id="mensajeError" class="form-control1">-</div>
        </div>
        <div id="listadoReferidos">
            <div class="row">
                <div class="table-responsive col-md-12">
                    <table class="table" id="tablaReporteReferidos">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>C�dula asesor</th>
                                <th>Nombre asesor</th>
                                <th>Apellido asesor</th>                           
                                <th>% asesor auto</th>
                                <th>% asesor vida</th>
                                <th>C�dula referido</th>
                                <th>Nombre referido</th>
                                <th>Apellido referido</th>                           
                                <th>% referido</th>
                            </tr>
                        </thead>                    
                        <tbody id="listado">
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>
        <div class="col-sm-offset-10" id="btnDescargar">     
            <button type="button" class="btn btn-primary" onclick="javascript:generarExcel('tablaReporteReferidos');">Descargar</button>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {
        listarReferidos();
    });

    var listado = [];
    var mapa = [
        function (data) {
            return data.idAsesor;
        },
        function (data) {
            return data.documentoAsesor;
        },
        function (data) {
            return data.nombreAsesor;
        },
        function (data) {
            return data.apellidoAsesor;
        },
        function (data) {
            return data.porcentajeAuto;
        },
        function (data) {
            return data.porcentajeVida;
        },
        function (data) {
            return data.documentoAsesorReferido;
        },
        function (data) {
            return data.nombreAsesorReferido;
        },
        function (data) {
            return data.apellidoAsesorReferido;
        },
        function (data) {
            return data.porcentajeReferido;
        }
    ];

    function listarReferidos() {
        ajaxTeca.listarReferidos({
            callback: function (data) {
                if (data != null) {
                    listado = data;
                    dwr.util.removeAllRows("listado");
                    dwr.util.addRows("listado", data, mapa, {
                        escapeHtml: false
                    });
                    $("#listadoReferidos").show();
                    $("#btnDescargar").show();
                } else {
                    mostrarMensaje('error', 'no se encontraron resultados.');
                    $("#listadoReferidos").hide();
                    $("#btnDescargar").hide();
                }
            }, timeout: 20000
        });
    }

    function generarExcel(table) {
        $("#" + table).table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "Reporte referidos",
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    }
</script>
