<%-- 
    Document   : reporte-cotizaciones-asesor
    Created on : 2/01/2018, 02:09:25 PM
    Author     : JHONATAN
--%>
<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Reporte de Cotizaciones Asesor</h3>
        <form role="form" class="form-horizontal" id="registerForm" action="return:false" autocomplete="off">
            <div id="divMensajeOk" class="form-group has-success" style="display: none;">
                <div id="mensajeOk" class="form-control1">-</div>
            </div>
            <div id="divMensajeError" class="form-group has-error" style="display: none;">
                <div id="mensajeError" class="form-control1">-</div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">De</span>
                        <input type="date" class="form-control1" name="start" id="fechaInicio" placeholder="Fecha de inicio" title="Ingrese fecha inicial" data-date-end-date="0d" required onchange="javascript:validarFecha(this.value)">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">a</span>
                        <input type="date" class="form-control1" name="end" id="fechaFinal" placeholder="Fecha de final" title="Ingrese fecha final" data-date-end-date="0d" required min="">     
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <center><button type="submit" class="btn btn-primary" id="" onclick="javascript:validar('registerForm');">Buscar</button></center>
                </div>
            </div>
        </form>
        <div>
            <div class="table-responsive col-md-12" id="divReporte" hidden>
                <table class="table" id="tablaCotizaciones">
                    <thead>
                        <tr>
                            <th>No. Cot</th>
                            <th>T. Doc</th>
                            <th>No. Doc</th>
                            <th>Nombre</th>                           
                            <th>Apellido</th>
                            <th>Tel�fono</th>
                            <th>e-mail</th>
                            <th>Dep/to</th>
                            <th>C. Cir</th>                           
                            <th>U. Vei</th>
                            <th>Marca</th>
                            <th>Linea</th>
                            <th>Modelo</th>
                            <th>Placa</th>                           
                            <th>F. Cot</th>
                            <th>Aseguradora</th>                           
                            <th>V. Cot</th>
                            <th>F. Call</th>
                            <th>Doc. Asesor</th>
                            <th>Nombre Asesor</th>
                            <th>Apellido Asesor</th>
                            <th>% de auto</th>
                            <th>Valor % auto</th>
                            <th>% de vida</th>
                            <th>Valor % vida</th>
                        </tr>
                    </thead>                    
                    <tbody id="listado">
                    </tbody>
                </table>    
            </div>
        </div>
        <div class="col-sm-offset-9">             
            <button type="button" class="btn btn-primary" id="btnVolver" onclick="javascript:generarExcel('tablaCotizaciones')">Excel</button>
        </div>
    </div>   
</div>
<script>
    jQuery(document).ready(function () {
        $("#btnVolver").hide();
        $("#btnRegistrar").hide();
    });

    var mapa = [
        function (data) {
            return data.idCotizacion;
        },
        function (data) {
            return data.tipoDocumento;
        },
        function (data) {
            return data.documento;
        },
        function (data) {
            return data.nombre;
        },
        function (data) {
            return data.apellido;
        },
        function (data) {
            return data.telefono;
        },
        function (data) {
            return data.correo;
        },
        function (data) {
            return data.departamentoNombre;
        },
        function (data) {
            return data.municipioNombre;
        },
        function (data) {
            return data.ubicacionVehiculo;
        },
        function (data) {
            return data.marca;
        },
        function (data) {
            return data.lineaVehiculo;
        },
        function (data) {
            return data.modelo;
        },
        function (data) {
            return data.placa;
        },
        function (data) {
            return data.fechaCotizacion;
        },
        function (data) {
            return data.aseguradora;
        },
        function (data) {
            return data.valorAsegurado;
        },
        function (data) {
            return data.fechaCallCenter;
        },
        function (data) {
            return data.documentoAsesor;
        },
        function (data) {
            return data.nombreAsesor;
        },
        function (data) {
            return data.apellidoAsesor;
        },
        function (data) {
            return data.porcentajeAuto;
        },
        function (data) {
            return data.valorPorcentajeAuto;
        },
        function (data) {
            return data.porcentajeVida;
        },
        function (data) {
            return data.valorPorcentajeVida;
        }
    ];

    function listarCotizaciones() {
        idTipoSeguro = $("#selectTipoSeguro").val();
        var datos = {
            fechaInicial: $("#fechaInicio").val(),
            fechaFinal: $("#fechaFinal").val(),
        };

        ajaxTeca.reporteCotizacionesAsesor(datos, {
            callback: function (data) {
                if (data !== null) {
                    $("#divReporte").show();
                    $("#tablaCotizaciones").show();
                    $("#btnVolver").show();
                    dwr.util.removeAllRows("listado");
                    dwr.util.addRows("listado", data, mapa, {
                        escapeHtml: false
                    });
                } else {
                    $("#divReporte").hide();
                    $("#tablaCotizaciones").hide();
                    $("#btnVolver").hide();
                    mostrarMensaje('error', 'No se encontraron resultados.');
                }
            },
            timeout: 20000
        });
    }

    function generarExcel(table) {
        $("#" + table).table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "Cotizaciones Realizadas Asesor",
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    }
    function validar(form) {
        $('#' + form).validate({
            highlight: function (label) {
                $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (label) {
                $(label).closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                var placement = element.closest('.input-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            },
            submitHandler: function () {
                listarCotizaciones();
            }
        });
    }

    function validarFecha(valor) {
        $("#fechaFinal").val("");
        $("#fechaFinal").attr("min", valor);
    }


</script>