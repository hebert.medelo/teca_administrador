<%-- 
    Document   : cotizaciones
    Created on : 15/09/2017, 03:21:25 PM
    Author     : Ing. Hebert Medelo
--%>
<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Reporte de posibles cotizaciones</h3>
        <form role="form" class="form-horizontal" id="registerForm" action="return:false" autocomplete="off">   
            <div id="divMensajeOk" class="form-group has-success" style="display: none;">
                <div id="mensajeOk" class="form-control1">-</div>
            </div>
            <div id="divMensajeError" class="form-group has-error" style="display: none;">
                <div id="mensajeError" class="form-control1">-</div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">De</span>
                        <input type="date" class="form-control1" name="start" id="fechaInicio" placeholder="Fecha de inicio" title="Ingrese fecha inicial" data-date-end-date="0d" required onchange="javascript:validarFecha(this.value)">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">a</span>
                        <input type="date" class="form-control1" name="end" id="fechaFinal" placeholder="Fecha de final" title="Ingrese fecha final" data-date-end-date="0d" required min="">     
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <div class="input-group">
                        <label for="selectTipoSeguro">Tipo de Seguro</label>
                        <select class="form-control" title="Seleccione tipo de seguro" id="selectTipoSeguro" name="selectTipoSeguro" required></select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <center><button type="submit" class="btn btn-primary" id="" onclick="javascript:validar('registerForm');">Buscar</button></center>
                    </div>
                </div>
            </div>
        </form>
        <div>
            <div class="table-responsive col-md-12" id="divSeguroAuto" hidden>
                <table class="table" id="tablaCotizaciones">
                    <thead>
                        <tr>
                            <th>No. Cot</th>
                            <th>Tip. Doc</th>
                            <th>No. Doc</th>
                            <th>Nombre</th>                           
                            <th>Apellido</th>
                            <th>Tel�fono</th>
                            <th>e-mail</th>
                            <th>Fec. nacimiento</th>
                            <th>Ubi. Vei</th>
                            <th>Dep/to</th>
                            <th>Ciudad</th>  
                            <th>Placa</th>   
                            <th>Tipo servicio</th> 
                            <th>Modelo</th>
                            <th>Marca</th>
                            <th>Linea</th>                        
                            <th>Cod. fasecolda</th>
                        </tr>
                    </thead>                    
                    <tbody id="listado">
                    </tbody>
                </table>    
            </div>

            <div class="table-responsive col-md-12" id="divSeguroVida" hidden>
                <table class="table" id="tablaCotizacionesVida">
                    <thead>
                        <tr>
                            <th>No. Cot</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Celular</th>
                            <th>Correo</th>
                            <th>F. nacimiento</th>
                            <th>F. cotizaci�n</th>                                                    
                            <th>Monto Asegurado</th>
                        </tr>
                    </thead>                    
                    <tbody id="listado2">
                    </tbody>
                </table>    
            </div>
        </div>
        <div class="col-sm-offset-9">             
            <button type="button" class="btn btn-primary" id="btnVolver" onclick="javascript:generarExcel('tablaCotizaciones')">Excel</button>
        </div>
        <div class="col-sm-offset-9">             
            <button type="button" class="btn btn-primary" id="btnVolver2" onclick="javascript:generarExcel('tablaCotizacionesVida')">Excel</button>
        </div>
    </div>   
</div>
<script>
    jQuery(document).ready(function () {
        $("#btnVolver").hide();
        $("#btnVolver2").hide();
        $("#btnRegistrar").hide();
        listarTipoSeguro("selectTipoSeguro");

    });

    var mapa = [
        function (data) {
            return data.id;
        },
        function (data) {
            return data.tipoDocumento;
        },
        function (data) {
            return data.cedula;
        },
        function (data) {
            return data.nombre;
        },
        function (data) {
            return data.apellido;
        },
        function (data) {
            return data.telefono;
        },
        function (data) {
            return data.correo;
        },
        function (data) {
            return data.fechaNacimiento;
        },
        function (data) {
            return data.ubicacionVehiculo;
        },
        function (data) {
            return data.departamento;
        },
        function (data) {
            return data.ciudad;
        },
        function (data) {
            return data.placa;
        },
        function (data) {
            return data.tipoServicio;
        },
        function (data) {
            return data.modelo;
        },
        function (data) {
            return data.marca;
        },
        function (data) {
            return data.linea;
        },
        function (data) {
            return data.codigoFasecolda;
        }
    ];

    var mapa2 = [
        function (data) {
            return data.id;
        },
        function (data) {
            return data.nombre;
        },
        function (data) {
            return data.apellido;
        },
        function (data) {
            return data.telefono;
        },
        function (data) {
            return data.correo;
        },
        function (data) {
            return data.fechaNacimiento;
        },
        function (data) {
            return data.fecha;
        },
        function (data) {
            return data.valorAsegurado;
        }
    ];
    var idTipoSeguro = null;

    function listarCotizaciones() {
        idTipoSeguro = $("#selectTipoSeguro").val();
        var datos = {
            fechaInicial: $("#fechaInicio").val(),
            fechaFinal: $("#fechaFinal").val(),
            idTipoSeguro: idTipoSeguro
        };

        ajaxTeca.listarPosiblesCotizaciones(datos, {
            callback: function (data) {
                if (data != null) {

                    if (data[0].idTipoSeguro == "1") {
                        $("#divSeguroVida").hide();
                        $("#btnVolver2").hide();
                        $("#divSeguroAuto").show();
                        $("#tablaCotizaciones").show();
                        $("#btnVolver").show();
                        dwr.util.removeAllRows("listado");
                        dwr.util.addRows("listado", data, mapa, {
                            escapeHtml: false
                        });
                    } else {
                        $("#divSeguroAuto").hide();
                        $("#btnVolver").hide();
                        $("#divSeguroVida").show();
                        $("#tablaCotizacionesVida").show();
                        $("#btnVolver2").show();
                        dwr.util.removeAllRows("listado2");
                        dwr.util.addRows("listado2", data, mapa2, {
                            escapeHtml: false
                        });
                    }

                } else {
                    $("#divSeguro").hide();
                    $("#divSeguroAuto").hide();
                    $("#tablaCotizaciones").hide();
                    $("#tablaCotizacionesVida").hide();
                    $("#btnVolver").hide();
                    $("#btnVolver2").hide();
                    mostrarMensaje('error', 'No se encontraron resultados.');
                }
            },
            timeout: 20000
        });
    }

    function generarExcel(table) {

        $("#" + table).table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "Posibles Cotizaciones",
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    }

    function listarTipoSeguro(idCombo, valorSeleccionando) {
        ajaxTeca.listarTipoSeguro({
            callback: function (data) {
                if (data !== null) {
                    dwr.util.removeAllOptions(idCombo);
                    dwr.util.addOptions(idCombo, [{
                            id: '',
                            nombre: 'Seleccione tipo de seguro'
                        }], 'id', 'nombre');
                    dwr.util.addOptions(idCombo, data, 'id', 'nombre');
                    $('#' + idCombo).val(valorSeleccionando);
                }
            },
            timeout: 20000
        });
    }

    function validar(form) {
        $('#' + form).validate({
            highlight: function (label) {
                $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (label) {
                $(label).closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                var placement = element.closest('.input-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            },
            submitHandler: function () {
                listarCotizaciones();
            }
        });
    }

    function validarFecha(valor) {
        $("#fechaFinal").val("");
        $("#fechaFinal").attr("min", valor);
    }

</script>


