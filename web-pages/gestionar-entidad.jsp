<%-- 
    Document   : gestionar-entidad
    Created on : 21/12/2017, 02:41:26 PM
    Author     : JHONATAN
--%>

<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Gestionar Entidad</h3>
        <form role="form" id="registerForm" action="return:false" autocomplete="off">
            <div id="divMensajeOk" class="form-group has-success" style="display: none;">
                <div id="mensajeOk" class="form-control1">-</div>
            </div>
            <div id="divMensajeError" class="form-group has-error" style="display: none;">
                <div id="mensajeError" class="form-control1">-</div>
            </div>
            <div class="row" id="divForm">
                <div class="col-md-12">                  
                    <div class="form-group col-md-6">                        
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" placeholder="Nombre" >
                    </div>
                    <br>
                    <div class="col-sm-offset-9"> 
                        <button type="button" class="btn btn-primary" id="btnRegistrar" onclick="javascript:registrar()">Registrar</button>
                        <button type="button" class="btn btn-primary" id="btnEditar" onclick="javascript:editar()">Editar</button>
                    </div>
                </div>           
            </div>
            <br>
            <table class="table table-hover table-striped" id="tablaEntidades">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Fecha creaci�n</th>
                        <th> </th>
                    </tr>
                </thead>                    
                <tbody id="listado">
                </tbody>
            </table>


        </form>

    </div>   
</div>
<script>

    jQuery(document).ready(function () {
        listarEntidades();
        $("#btnEditar").hide();
    });

    function listarEntidades() {

        ajaxTeca.listarEntidades({
            callback: function (data) {
                if (data != null) {
                    listado = data;
                    dwr.util.removeAllRows("listado");
                    dwr.util.addRows("listado", data, mapa, {
                        escapeHtml: false
                    });
                } else {
                    mostrarMensaje('error', 'No se encontraron entidades registradas.');
                    $("#tablaEntidades").hide();
                }
            },
            timeout: 20000
        });
    }
    var idEntidad = null;

    var listado = [];
    var mapa = [
        function (data) {
            return data.nombre;
        },
        function (data) {
            return data.fechaCreacion;
        },
        function (data) {
            return "<td><button class='label label-success' onclick='editarEntidad(" + data.id + ");'>Modificar</button></td>";
        }
    ];

    function registrar() {
        if ($('#nombre').val() === '') {
            mostrarMensaje('error', 'Ingrese nombre.');
            $('#nombre').focus();
            return;
        }
        var datos = {
            nombre: $("#nombre").val()
        };
        ajaxTeca.registrarEntidad(datos, {
            callback: function (data) {
                if (data) {
                    mostrarMensaje('ok', 'Se registr� con exito.');
                    $('#nombre').val("");
                    listarEntidades();
                } else {
                    mostrarMensaje('error', 'No se pudo realizar el registro.');
                }
            },
            timeout: 20000
        });
    }

    function soloNumeros(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = "0,1,2,3,4,5,6,7,8,9";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }

    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " �����abcdefghijklmn�opqrstuvwxyz";
        especiales = "8-37-39-46";
        tecla_especial = false;
        for (var i in especiales) {
            if (key === especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }

    function editarEntidad(id) {
        idEntidad = id;
        $("#tablaEntidades").hide();
        $("#btnRegistrar").hide();
        $("#divForm").show();
        $("#btnEditar").show();
    }

    function editar() {
        var datos = {
            id: idEntidad,
            nombre: $("#nombre").val()
        };
        ajaxTeca.editarEntidad(datos, {
            callback: function (data) {
                if (data) {
                    $('#nombre').val("");
                    idEntidad = null;
                    listarEntidades();
                    $("#btnEditar").hide();
                    $("#tablaEntidades").show();
                    $("#divForm").show();
                    $("#btnRegistrar").show();
                    mostrarMensaje('ok', 'Se modific� con exito.');
                } else {
                    mostrarMensaje('error', 'No se pudo realizar la modificaci�n.');
                }
            },
            timeout: 20000
        });
    }
</script>
