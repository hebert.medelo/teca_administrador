<%-- 
    Document   : recuperar-contrasenia
    Created on : 20/01/2018, 10:50:52 AM
    Author     : JHONATAN
--%>
<%@ page import="javax.servlet.*"%>
<%@ page import="co.mobiltech.teca.common.util.*"%>
<%@ page import="java.util.*,java.io.*"  contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%

    session.invalidate();

    String mensajeError = request.getParameter("error");

    boolean mostrarError = false;

    if (!ValidaString.isNullOrEmptyString(mensajeError)) {
        mostrarError = true;
    }
%>

<!DOCTYPE HTML>
<html>
    <head>
        <title>Teca | Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <link href="css/font-awesome.css" rel="stylesheet"> 
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="login">
            <h1><a href="login.jsp">TECA</a></h1>
            <div class="login-bottom">
                <form role="form" method="post" autocomplete="off" class="form-horizontal" id="contactForm">                        
                    <div class="alert alert-success" id="alertCorrecto">   
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>Recuperación con Éxito!</h4>
                        <p>La contraseña fue enviada al correo <strong id="correo2"></strong></p> <br>                                                         
                    </div>
                    <div class="alert alert-danger alert-dismissable" id="alertError">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>  
                        <p id="msgError"> </p>
                    </div>


                    <div class="col-md-12 form-group">
                        <div class="col-sm-4 col-md-4 control-label hor-form">
                            <label class="control-label">Correo</label>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <input type="email" id="correo" name="correo" class="form-control text-input col-sm-12" placeholder="Correo" required title="Ingrese correo">
                        </div>
                    </div>
                    <br><br><br>
                    <div class="col-md-12 login-do">
                        <div class="col-md-6">
                            <label class="hvr-shutter-in-horizontal login-sub">
                                <input type="submit" id="dtnContinuar" value="Continuar" onclick="javascript:validar();"/>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <a href="login.jsp" class="hvr-shutter-in-horizontal">Volver</a>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </form>
            </div>
        </div>
        <!---->
        <div class="copy-right">
            <p> &copy; 2016 Minimal. All Rights Reserved | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>	    </div>  
        <!---->
        <!--scrolling js-->
        <script src="js/jquery.nicescroll.js"></script>
        <script src="js/scripts.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <!--//scrolling js-->
    </body>

    <script>
                                    $(document).ready(function () {
                                        $("#alertCorrecto").hide();
                                        $("#alertError").hide();
                                    });

                                    function validar() {
                                        $("#alertCorrecto").hide();
                                        $("#alertError").hide();
                                        $('#contactForm').validate({
                                            highlight: function (label) {
                                                jQuery(label).closest('.form-group').removeClass('has-success').addClass('has-error');
                                            },
                                            success: function (label) {
                                                jQuery(label).closest('.form-group').removeClass('has-error');
                                                label.remove();
                                            },
                                            errorPlacement: function (error, element) {
                                                var placement = element.closest('.input-group');
                                                if (!placement.get(0)) {
                                                    placement = element;
                                                }
                                                if (error.text() !== '') {
                                                    placement.after(error);
                                                }
                                            },
                                            submitHandler: function () {
                                                llamarServlet();
                                            }
                                        });
                                    }

                                    function llamarServlet() {
                                        $('#dtnContinuar').attr("disabled", true);
                                        $.ajax({
                                            url: 'servletRecuperarContrasenia',
                                            data: {
                                                correo: $("#correo").val()
                                            },
                                            success: function (data) {
                                                if (data != "incorrecto" && data != "problema") {
                                                    $("#alertCorrecto").show();
                                                    $('#correo2').text(data);
                                                }
                                                if (data === "problema") {
                                                    $("#alertError").show();
                                                    $('#msgError').text("Ocurrio un problema en la recuperación de la contraseña");
                                                }
                                                if (data === "incorrecto") {
                                                    $("#alertError").show();
                                                    $('#msgError').text("No existe un usuario con correo: " + $("#correo").val());
                                                }
                                                $('#dtnContinuar').attr("disabled", false);
                                            }
                                        });
                                    }

    </script>
</html>